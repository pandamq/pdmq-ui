import { createAction, props } from '@ngrx/store';
import { FetchResponseInterface } from 'src/app/interfaces/common/fetch-response.interface';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';
import { RoleInterface } from 'src/app/interfaces/schema/role.interface';


export const RoleActionTypes = {
  FETCH                 : "[ROLE] FETCH",
  FETCH_SUCCESS         : "[ROLE] FETCH SUCCESS",
  FETCH_SUMMARY         : "[ROLE] FETCH SUMMARY",
  FETCH_SUMMARY_SUCCESS : "[ROLE] FETCH SUMMARY SUCCESS",
  SAVE                  : "[ROLE] SAVE",
  SAVE_SUCCESS          : "[ROLE] SAVE SUCCESS",
  DELETE                : "[ROLE] DELETE",
  DELETE_SUCCESS        : "[ROLE] DELETE SUCCESS"
}

const FETCH = createAction(
  RoleActionTypes.FETCH,
  props<{
    pagination: PaginationInterface
  }>()
);

const FETCH_SUCCESS = createAction(
  RoleActionTypes.FETCH_SUCCESS,
  props<FetchResponseInterface<RoleInterface>>()
);

const SAVE = createAction(
  RoleActionTypes.SAVE,
  props<{ 
    payload: RoleInterface,
    cb?: Function
  }>()
);

const SAVE_SUCCESS = createAction(
  RoleActionTypes.SAVE_SUCCESS,
  props<{
    result: RoleInterface
  }>()
);

const DELETE = createAction(
  RoleActionTypes.DELETE,
  props<{ 
    id: string,
    cb?: Function
  }>()
);

const DELETE_SUCCESS = createAction(
  RoleActionTypes.DELETE_SUCCESS,
  props<{
    id: string
  }>()
);

const FETCH_SUMMARY = createAction(
  RoleActionTypes.FETCH_SUMMARY
);

const FETCH_SUMMARY_SUCCESS = createAction(
  RoleActionTypes.FETCH_SUMMARY_SUCCESS,
  props<FetchResponseInterface<RoleInterface>>()
);

export const RoleActions = {
  FETCH,
  FETCH_SUCCESS,
  FETCH_SUMMARY,
  FETCH_SUMMARY_SUCCESS,
  SAVE,
  SAVE_SUCCESS,
  DELETE,
  DELETE_SUCCESS
}

