import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { RoleActions } from './role.actions';
import { RoleInterface } from 'src/app/interfaces/schema/role.interface';

export interface RoleState extends EntityState<RoleInterface> {
  count: number
}

const adapter: EntityAdapter<RoleInterface> = createEntityAdapter<RoleInterface>({
  selectId: (item) => item._id
});

const initialState: RoleState = adapter.getInitialState({
  count: 0
});

const reducer = createReducer(
  initialState,

  on(RoleActions.FETCH_SUCCESS, (state, { results, count }) => {
    return adapter.setAll(results, {
      ...state,
      count
    });
  }),

  on(RoleActions.FETCH_SUMMARY_SUCCESS, (state, { results, count }) => {
    return adapter.setAll(results, {
      ...state,
      count
    });
  }),

  on(RoleActions.SAVE_SUCCESS, (state, { result }) => {
    return adapter.upsertOne(result, state);
  }),

  on(RoleActions.DELETE_SUCCESS, (state, { id }) => {
    return adapter.removeOne(id, state)
  }),
);

export function RoleReducer(state: RoleState | undefined, action: Action) {
  return reducer(state, action);
}
