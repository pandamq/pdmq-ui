import { map, mergeMap, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { RoleActions } from './role.actions';
import { RolesService } from 'src/app/services/store/permissions/roles.service';

@Injectable()
export class RoleEffects {

  constructor(
    private actions$: Actions,
    private _role: RolesService
  ) {}

  FETCH$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RoleActions.FETCH),
      mergeMap((action) => {
        return this._role.fetch(action.pagination)
      }),
      map(res => {
        return RoleActions.FETCH_SUCCESS(res)
      })
    )
  })

  FETCH_SUMMARY$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RoleActions.FETCH_SUMMARY),
      mergeMap(() => {
        return this._role.fetchSummary()
      }),
      map(res => {
        return RoleActions.FETCH_SUMMARY_SUCCESS(res)
      })
    )
  })

  SAVE$ = createEffect(() => {
    let cb: Function;
    return this.actions$.pipe(
      ofType(RoleActions.SAVE),
      mergeMap((action) => {
        cb = action.cb;
        return this._role.save(
          action.payload
        )
      }),
      tap((res) => {
        res.result && cb && cb(res.result);
      }),
      map(res => {
        return RoleActions.SAVE_SUCCESS(res)
      })
    )
  })

  DELETE$ = createEffect(() => {
    let cb: Function;
    return this.actions$.pipe(
      ofType(RoleActions.DELETE),
      mergeMap((action) => {
        cb = action.cb;
        return this._role.delete(
          action.id
        )
      }),
      tap((res) => {
        cb && cb(res);
      }),
      map(res => {
        return RoleActions.DELETE_SUCCESS({
          id: res
        })
      })
    )
  })
}
