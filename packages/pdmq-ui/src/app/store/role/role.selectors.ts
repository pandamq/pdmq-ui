import { createSelector } from "@ngrx/store";
import { LoginInterface } from "src/app/interfaces/schema/login.interface";
import { AppState } from "../store";

const listAll = (state: AppState) => Object.values(state.Role?.entities || {})
const count = (state: AppState) => state.Role.count;
const entities = (state: AppState) => state.Role?.entities || {}

export const RoleSelectors = {
  listAll: createSelector(
    listAll,
    (roles: LoginInterface[]) => {
      return roles
    }
  ),
  item: (id) => createSelector(
    entities,
    entities => {
      return entities[id]
    }
  ),
  count: createSelector(
    count,
    (count: number) => {
      return count
    }
  )
}
