import { createAction, props } from '@ngrx/store';
import { FetchResponseInterface } from 'src/app/interfaces/common/fetch-response.interface';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';
import { PDMQWorkflowExecutionLog } from 'pdmq/dist/interfaces/workflow-execution-log.interface';

export const PDMQWorkflowExecutionLogActionTypes = {
  FETCH_WORKFLOW_EXECUTION_LOG: "[PDMQ WORKFLOW EXECUTION LOG] FETCH WORKFLOW EXECUTION LOG",
  FETCH_WORKFLOW_EXECUTION_LOG_SUCCESS: "[PDMQ WORKFLOW EXECUTION LOG] FETCH WORKFLOW EXECUTION LOG SUCCESS"
}

const FETCH_WORKFLOW_EXECUTION_LOG = createAction(
  PDMQWorkflowExecutionLogActionTypes.FETCH_WORKFLOW_EXECUTION_LOG,
  props<{
    workflowId: string,
    nodeId: number,
    pagination?: PaginationInterface
  }>()
);

const FETCH_WORKFLOW_EXECUTION_LOG_SUCCESS = createAction(
  PDMQWorkflowExecutionLogActionTypes.FETCH_WORKFLOW_EXECUTION_LOG_SUCCESS,
  props<FetchResponseInterface<PDMQWorkflowExecutionLog>>()
);

export const PDMQWorkflowExecutionLogActions = {
  FETCH_WORKFLOW_EXECUTION_LOG,
  FETCH_WORKFLOW_EXECUTION_LOG_SUCCESS
}

