import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { PDMQWorkflowExecutionLogActions } from './pdmq-workflow-execution-log.actions';
import { PDMQWorkflowExecutionLog } from 'pdmq/dist/interfaces/workflow-execution-log.interface';

export interface PDMQWorkflowExecutionLogState extends EntityState<PDMQWorkflowExecutionLog> {
  count: number
}

const adapter: EntityAdapter<PDMQWorkflowExecutionLog> = createEntityAdapter<PDMQWorkflowExecutionLog>({
  selectId: (item) => item._id
});

const initialState: PDMQWorkflowExecutionLogState = adapter.getInitialState({
  count: 0
});

const reducer = createReducer(
  initialState,

  on(PDMQWorkflowExecutionLogActions.FETCH_WORKFLOW_EXECUTION_LOG_SUCCESS, (state, { results, count }) => {
    return adapter.setAll(results, {
      ...state,
      count
    });
  })
);

export function PDMQWorkflowExecutionLogReducer(state: PDMQWorkflowExecutionLogState | undefined, action: Action) {
  return reducer(state, action);
}
