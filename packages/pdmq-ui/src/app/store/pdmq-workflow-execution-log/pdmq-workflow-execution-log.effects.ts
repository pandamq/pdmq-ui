import { map, mergeMap, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { PDMQWorkflowExecutionLogActions } from './pdmq-workflow-execution-log.actions';
import { WorkflowExecutionLogsService } from 'src/app/services/pdmq/workflow-execution-logs.service';

@Injectable()
export class PDMQWorkflowExecutionLogEffects {

  constructor(
    private actions$: Actions,
    private _pdmq: WorkflowExecutionLogsService
  ) {}

  FETCH_WORKFLOW_EXECUTION_LOG$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PDMQWorkflowExecutionLogActions.FETCH_WORKFLOW_EXECUTION_LOG),
      mergeMap((action) => {
        return this._pdmq.fetch({
          workflowId: action.workflowId,
          nodeId: action.nodeId,
          pagination: action.pagination
        })
      }),
      map(res => {
        return PDMQWorkflowExecutionLogActions.FETCH_WORKFLOW_EXECUTION_LOG_SUCCESS(res)
      })
    )
  })

}
