import * as moment from "moment";
import { AppState } from "../store";

const listAll = (state: AppState) => Object.values(state.PDMQWorkflowExecutionLog?.entities || {}).sort((a, b) => {
  return moment(b.process_started_at).isAfter(moment(a.process_started_at)) ? -1 : 1;
})
const count = (state: AppState) => state.PDMQWorkflowExecutionLog.count;
export const PDMQWorkflowExecutionLogSelectors = {
  listAll,
  count
}
