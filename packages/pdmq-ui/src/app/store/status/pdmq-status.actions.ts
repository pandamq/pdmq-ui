import { StatusActionsTypes } from '../actions-types/status.action-types';
import { createAction, props } from '@ngrx/store';
import { PDMQTask } from 'pdmq/dist/interfaces/task.interface';
import { PDMQConsumer } from 'pdmq/dist/interfaces/consumer.interface';

const FETCH_CONSUMER_COUNT = createAction(
  StatusActionsTypes.FETCH_CONSUMER_COUNT
);

const FETCH_CONSUMER_COUNT_SUCCESS = createAction(
  StatusActionsTypes.FETCH_CONSUMER_COUNT_SUCCESS,
  props<{ count: number }>()
);

const FETCH_PROCESSING_TASKS = createAction(
  StatusActionsTypes.FETCH_PROCESSING_TASKS
);

const FETCH_PROCESSING_TASKS_SUCCESS = createAction(
  StatusActionsTypes.FETCH_PROCESSING_TASKS_SUCCESS,
  props<{ tasks: PDMQTask[] }>()
);

const FETCH_CONSUMERS = createAction(
  StatusActionsTypes.FETCH_CONSUMERS
);

const FETCH_CONSUMERS_SUCCESS = createAction(
  StatusActionsTypes.FETCH_CONSUMERS_SUCCESS,
  props<{ consumers: PDMQConsumer[] }>()
);


export const StatusActions = {
  FETCH_CONSUMER_COUNT,
  FETCH_CONSUMER_COUNT_SUCCESS,
  FETCH_PROCESSING_TASKS,
  FETCH_PROCESSING_TASKS_SUCCESS,
  FETCH_CONSUMERS,
  FETCH_CONSUMERS_SUCCESS
}

