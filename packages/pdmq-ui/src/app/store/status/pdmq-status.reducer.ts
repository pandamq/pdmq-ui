import { Action, createReducer, on } from '@ngrx/store';
import { PDMQConsumer } from 'pdmq/dist/interfaces/consumer.interface';
import { PDMQTask } from 'pdmq/dist/interfaces/task.interface';
import { StatusActions } from './pdmq-status.actions';


export interface PDMQStatusState {
  consumers: PDMQConsumer[],
  consumerCount: number
  currentProcessingTasks?: PDMQTask[]
}

const initialState: PDMQStatusState = {
  consumerCount: 0,
  consumers: [],
  currentProcessingTasks: []
}

const reducer = createReducer(
  initialState,
  on(StatusActions.FETCH_CONSUMER_COUNT_SUCCESS, (state, { count }) => {
    return {
      ...state,
      consumerCount: count
    }
  }),
  on(StatusActions.FETCH_PROCESSING_TASKS_SUCCESS, (state, { tasks }) => {
    return {
      ...state,
      currentProcessingTasks: tasks
    }
  }),
  on(StatusActions.FETCH_CONSUMERS_SUCCESS, (state, { consumers }) => {
    return {
      ...state,
      consumers: consumers
    }
  }),
);

export function PDMQStatusReducer(state: PDMQStatusState | undefined, action: Action) {
  return reducer(state, action);
}
