import { AppState } from "../store";

const consumerCount = (state: AppState) => state.PDMQStatus.consumerCount
const currentProcessingTasks = (state: AppState) => state.PDMQStatus.currentProcessingTasks
const consumers = (state: AppState) => state.PDMQStatus.consumers

export const PDMQStatusSelectors = {
  consumerCount,
  currentProcessingTasks,
  consumers
}
