import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { StatusActions } from './pdmq-status.actions';
import { StatusService } from 'src/app/services/pdmq/status.service';

@Injectable()
export class PDMQStatusEffects {

  constructor(
    private actions$: Actions,
    private _status: StatusService
  ) {}

  FETCH_CONSUMER_COUNT$ = createEffect(() => this.actions$.pipe(
    ofType(StatusActions.FETCH_CONSUMER_COUNT),
    tap(() => this._status.fetchConsumerCount())
  ), {dispatch: false})

  FETCH_CONSUMERS$ = createEffect(() => this.actions$.pipe(
    ofType(StatusActions.FETCH_CONSUMERS),
    tap(() => this._status.fetchConsumers())
  ), {dispatch: false})

  FETCH_PROCESSING_TASKS$ = createEffect(() => this.actions$.pipe(
    ofType(StatusActions.FETCH_PROCESSING_TASKS),
    tap(() => this._status.fetchProcessingTask())
  ), {dispatch: false})
}
