import { map, mergeMap, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TaskService } from 'src/app/services/pdmq/task.service';
import { PDMQTasksActions } from './pdmq-task.actions';
import { FetchResponseInterface } from 'src/app/interfaces/common/fetch-response.interface';
import { PDMQTask } from 'pdmq/dist/interfaces/task.interface'
import { TaskTriggerTypes } from 'src/app/enums/task-trigger-types.enum';

@Injectable()
export class TaskEffects {

  constructor(
    private actions$: Actions,
    private _task: TaskService
  ) {}

  FETCH_TASK$ = createEffect(() => this.actions$.pipe(
    ofType(PDMQTasksActions.FETCH_TASK),
    mergeMap((action) => {
      return this._task.fetchTasks({
        from: action.from, 
        to: action.to, 
        type: action.triggerType
      })
    }),
    map((res: FetchResponseInterface<PDMQTask>) => {
      return PDMQTasksActions.FETCH_TASK_SUCCESS(res)
    })
  ))

  FETCH_STORED_TASK$ = createEffect(() => this.actions$.pipe(
    ofType(PDMQTasksActions.FETCH_STORED_TASK),
    mergeMap(() => {
      return this._task.fetchTasks({
        type: TaskTriggerTypes.NEVER
      })
    }),
    map((res: FetchResponseInterface<PDMQTask>) => {
      return PDMQTasksActions.FETCH_STORED_TASK_SUCCESS(res)
    })
  ))

  DELETE_TASK$ = createEffect(() => this.actions$.pipe(
    ofType(PDMQTasksActions.DELETE_TASK),
    mergeMap((action) => {
      return this._task.deleteTask(action.task_id)
    }),
    map((res) => {
      return PDMQTasksActions.DELETE_TASK_SUCCESS({task_id: res})
    })
  ))

  SAVE_TASK$ = createEffect(() => this.actions$.pipe(
    ofType(PDMQTasksActions.SAVE_TASK),
    mergeMap((action) => this._task.saveTask(action.task)),
    map((res: {result: PDMQTask}) => {
      return PDMQTasksActions.SAVE_TASK_SUCCESS(res)
    })
  ))
}
