import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { PDMQTasksActions } from './pdmq-task.actions';
import { PDMQTask } from 'pdmq/dist/interfaces/task.interface';

export interface PDMQTaskState extends EntityState<PDMQTask> {
  storedTasks: PDMQTask[],
  count: number
}

const adapter: EntityAdapter<PDMQTask> = createEntityAdapter<PDMQTask>({
  selectId: (task) => task.task_id
});

const initialState: PDMQTaskState = adapter.getInitialState({
  storedTasks: [],
  count: 0
});

const reducer = createReducer(
  initialState,

  on(PDMQTasksActions.FETCH_TASK_SUCCESS, (state, { results, count }) => {
    return adapter.setAll((results), {
      ...state,
      count
    });
  }),

  on(PDMQTasksActions.FETCH_STORED_TASK_SUCCESS, (state, { results, count }) => {
    return {
      ...state,
      storedTasks: results
    }
  }),

  on(PDMQTasksActions.CLEAR_TASK_SUCCESS, (state) => {
    return adapter.setAll([], state);
  }),

  on(PDMQTasksActions.SAVE_TASK_SUCCESS, (state, {result}) => {
    return adapter.upsertOne(result, state);
  }),

  on(PDMQTasksActions.DELETE_TASK_SUCCESS, (state, {task_id}) => {
    return adapter.removeOne(task_id, state);
  }),
);

export function PDMQTaskReducer(state: PDMQTaskState | undefined, action: Action) {
  return reducer(state, action);
}
