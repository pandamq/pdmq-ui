import { PDMQActionTypes } from '../actions-types/pdmq.action-types';
import { createAction, props } from '@ngrx/store';
import { PDMQTask } from 'pdmq/dist/interfaces/task.interface';
import { TaskTriggerTypes } from 'src/app/enums/task-trigger-types.enum';
import { FetchResponseInterface } from 'src/app/interfaces/common/fetch-response.interface';

const FETCH_TASK = createAction(
  PDMQActionTypes.FETCH_TASK,
  props<{ from?: string; to?: string, triggerType?: TaskTriggerTypes }>()
);

const FETCH_TASK_SUCCESS = createAction(
  PDMQActionTypes.FETCH_TASK_SUCCESS,
  props<FetchResponseInterface<PDMQTask>>()
);

const SAVE_TASK = createAction(
  PDMQActionTypes.SAVE_TASK,
  props<{ task: PDMQTask }>()
);

const SAVE_TASK_SUCCESS = createAction(
  PDMQActionTypes.SAVE_TASK_SUCCESS,
  props<{ result: PDMQTask }>()
);

const CLEAR_TASK = createAction(
  PDMQActionTypes.CLEAR_TASK
);

const DELETE_TASK = createAction(
  PDMQActionTypes.DELETE_TASK,
  props<{ task_id: string }>()
);

const DELETE_TASK_SUCCESS = createAction(
  PDMQActionTypes.DELETE_TASK_SUCCESS,
  props<{ task_id: string }>()
);

const FETCH_STORED_TASK = createAction(
  PDMQActionTypes.FETCH_STORED_TASK
)

const FETCH_STORED_TASK_SUCCESS = createAction(
  PDMQActionTypes.FETCH_STORED_TASK_SUCCESS,
  props<FetchResponseInterface<PDMQTask>>()
)

const CLEAR_TASK_SUCCESS = createAction(
  PDMQActionTypes.CLEAR_TASK_SUCCESS
)

export const PDMQTasksActions = {
  FETCH_STORED_TASK,
  FETCH_STORED_TASK_SUCCESS,
  FETCH_TASK,
  FETCH_TASK_SUCCESS,
  SAVE_TASK,
  SAVE_TASK_SUCCESS,
  CLEAR_TASK,
  CLEAR_TASK_SUCCESS,
  DELETE_TASK,
  DELETE_TASK_SUCCESS,
}

