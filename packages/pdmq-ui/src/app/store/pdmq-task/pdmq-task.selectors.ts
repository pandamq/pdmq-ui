import { AppState } from "../store";

const listAll = (state: AppState) => Object.keys(state.PDMQTask.entities)
  .map(key => state.PDMQTask.entities[key])


const storedTasks = (state: AppState) => state.PDMQTask.storedTasks;

export const PDMQTaskSelectors = {
  listAll,
  storedTasks
}
