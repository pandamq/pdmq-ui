import { createSelector } from "@ngrx/store";
import { LoginInterface } from "src/app/interfaces/schema/login.interface";
import { AppState } from "../store";

const listAll = (state: AppState) => Object.values(state.User?.entities || {})
const count = (state: AppState) => state.User.count;

export const UserSelectors = {
  listAll: createSelector(
    listAll,
    (users: LoginInterface[]) => {
      return users
    }
  ),
  count: createSelector(
    count,
    (count: number) => {
      return count
    }
  )
}
