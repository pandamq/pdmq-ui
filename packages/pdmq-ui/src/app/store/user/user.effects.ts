import { filter, map, mergeMap, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { UserActions } from './user.actions';
import { UsersService } from 'src/app/services/store/permissions/users.service';

@Injectable()
export class UserEffects {

  constructor(
    private actions$: Actions,
    private _settings: UsersService
  ) {}

  FETCH$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.FETCH),
      mergeMap((action) => {
        return this._settings.fetch(action.pagination)
      }),
      map(res => {
        return UserActions.FETCH_SUCCESS(res)
      })
    )
  })

  SAVE$ = createEffect(() => {
    let cb: Function;
    return this.actions$.pipe(
      ofType(UserActions.SAVE),
      mergeMap((action) => {
        cb = action.cb;
        return this._settings.save(
          action.payload
        )
      }),
      tap((res) => {
        res.result && cb && cb(res.result);
      }),
      map(res => {
        return UserActions.SAVE_SUCCESS(res)
      })
    )
  })

  CHANGE_PASSWORD$ = createEffect(() => {
    let cb: Function;
    return this.actions$.pipe(
      ofType(UserActions.CHANGE_PASSWORD),
      mergeMap((action) => {
        cb = action.cb;
        return this._settings.changePassword(
          action.password
        )
      }),
      tap((res) => {
        res.result && cb && cb(res.result);
      }),
      map(res => {
        return UserActions.CHANGE_PASSWORD_SUCCESS(res)
      })
    )
  })

}
