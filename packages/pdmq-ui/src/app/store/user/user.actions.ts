import { createAction, props } from '@ngrx/store';
import { FetchResponseInterface } from 'src/app/interfaces/common/fetch-response.interface';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';
import { LoginInterface } from 'src/app/interfaces/schema/login.interface';


export const UserActionTypes = {
  FETCH                   : "[USER] FETCH",
  FETCH_SUCCESS           : "[USER] FETCH SUCCESS",
  SAVE                    : "[USER] SAVE",
  SAVE_SUCCESS            : "[USER] SAVE SUCCESS",
  CHANGE_PASSWORD         : "[USER] CHANGE PASSWORD",
  CHANGE_PASSWORD_SUCCESS : "[USER] CHANGE PASSWORD SUCCESS",
}

const FETCH = createAction(
  UserActionTypes.FETCH,
  props<{
    pagination: PaginationInterface
  }>()
);

const FETCH_SUCCESS = createAction(
  UserActionTypes.FETCH_SUCCESS,
  props<FetchResponseInterface<LoginInterface>>()
);

const SAVE = createAction(
  UserActionTypes.SAVE,
  props<{ 
    payload: LoginInterface,
    cb?: Function
   }>()
);

const SAVE_SUCCESS = createAction(
  UserActionTypes.SAVE_SUCCESS,
  props<{
    result: LoginInterface
  }>()
);

const CHANGE_PASSWORD = createAction(
  UserActionTypes.CHANGE_PASSWORD,
  props<{ 
    password: string,
    cb?: Function
   }>()
);

const CHANGE_PASSWORD_SUCCESS = createAction(
  UserActionTypes.CHANGE_PASSWORD_SUCCESS,
  props<{
    result: LoginInterface
  }>()
);

export const UserActions = {
  FETCH,
  FETCH_SUCCESS,
  SAVE,
  SAVE_SUCCESS,
  CHANGE_PASSWORD,
  CHANGE_PASSWORD_SUCCESS
}

