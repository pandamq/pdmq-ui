import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { LoginInterface } from 'src/app/interfaces/schema/login.interface';
import { UserActions } from './user.actions';

export interface UserState extends EntityState<LoginInterface> {
  count: number
}

const adapter: EntityAdapter<LoginInterface> = createEntityAdapter<LoginInterface>({
  selectId: (item) => item._id
});

const initialState: UserState = adapter.getInitialState({
  count: 0
});

const reducer = createReducer(
  initialState,

  on(UserActions.FETCH_SUCCESS, (state, { results, count }) => {
    return adapter.setAll(results, {
      ...state,
      count
    });
  }),

  on(UserActions.SAVE_SUCCESS, (state, { result }) => {
    return adapter.upsertOne(result, state);
  }),
);

export function UserReducer(state: UserState | undefined, action: Action) {
  return reducer(state, action);
}
