import { map, mergeMap, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { PDMQVariableActions } from './pdmq-variable.actions';
import { VariableService } from 'src/app/services/pdmq/variable.service';

@Injectable()
export class PDMQVariableEffects {

  constructor(
    private actions$: Actions,
    private _pdmq: VariableService
  ) {}

  FETCH_VARIABLE$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PDMQVariableActions.FETCH_VARIABLE),
      mergeMap((action) => {
        return this._pdmq.fetch(
          action.pagination
        )
      }),
      map(res => {
        return PDMQVariableActions.FETCH_VARIABLE_SUCCESS(res)
      })
    )
  })

  FETCH_NAMES$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PDMQVariableActions.FETCH_NAMES),
      mergeMap(() => {
        return this._pdmq.fetchNames()
      }),
      map(res => {
        return PDMQVariableActions.FETCH_NAMES_SUCCESS(res)
      })
    )
  })

  FETCH_VARIABLES_EXAMPLE$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PDMQVariableActions.FETCH_VARIABLES_EXAMPLE),
      mergeMap(() => {
        return this._pdmq.fetchExamples()
      }),
      map(res => {
        return PDMQVariableActions.FETCH_VARIABLES_EXAMPLE_SUCCESS(res)
      })
    )
  })

  SAVE_VARIABLE$ = createEffect(() => {
    let cb: Function;
    return this.actions$.pipe(
      ofType(PDMQVariableActions.SAVE_VARIABLE),
      mergeMap((action) => {
        cb = action.cb;
        return this._pdmq.save(
          action.payload
        )
      }),
      tap((res) => {
        res.result && cb && cb(res.result);
      }),
      map(res => {
        return PDMQVariableActions.SAVE_VARIABLE_SUCCESS(res)
      })
    )
  })

  DELETE_VARIABLE$ = createEffect(() => {
    let cb: Function;
    return this.actions$.pipe(
      ofType(PDMQVariableActions.DELETE_VARIABLE),
      mergeMap((action) => {
        cb = action.cb;
        return this._pdmq.delete(
          action.variable_id
        )
      }),
      tap((res) => {
        res && cb && cb(res);
      }),
      map(res => {
        return PDMQVariableActions.DELETE_VARIABLE_SUCCESS({
          variable_id: res
        })
      })
    )
  })
}
