import { createSelector } from "@ngrx/store";
import { AppState } from "../store";
import { PDMQVariableState } from "./pdmq-variable.reducer";

const listAll = (state: AppState) => Object.values(state.PDMQVariable?.entities || {})
const count = (state: AppState) => state.PDMQVariable.count;
const state = (state: AppState) => state.PDMQVariable;

export const PDMQVariableSelectors = {
  listAll,
  count,
  examples: createSelector(
    state,
    (state: PDMQVariableState) => state.examples
  )
}
