import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { PDMQVariableActions } from './pdmq-variable.actions';
import { PDMQVariable } from 'pdmq/dist/interfaces/variable.interface';

export interface PDMQVariableState extends EntityState<PDMQVariable> {
  count: number,
  examples: {text: string, value: string}[]
}

const adapter: EntityAdapter<PDMQVariable> = createEntityAdapter<PDMQVariable>({
  selectId: (variable) => variable._id
});

const initialState: PDMQVariableState = adapter.getInitialState({
  count: 0,
  examples: []
});

const reducer = createReducer(
  initialState,

  on(PDMQVariableActions.FETCH_VARIABLE_SUCCESS, (state, { results, count }) => {
    return adapter.setAll(results, {
      ...state,
      count
    });
  }),

  on(PDMQVariableActions.FETCH_VARIABLES_EXAMPLE_SUCCESS, (state, { results }) => {
    return {
      ...state,
      examples: results
    }
  }),

  on(PDMQVariableActions.SAVE_VARIABLE_SUCCESS, (state, { result }) => {
    return adapter.upsertOne(result, state);
  }),

  on(PDMQVariableActions.DELETE_VARIABLE_SUCCESS, (state, {variable_id}) => {
    return adapter.removeOne(variable_id, state);
  }),
);

export function PDMQVariableReducer(state: PDMQVariableState | undefined, action: Action) {
  return reducer(state, action);
}
