import { PDMQActionTypes } from '../actions-types/pdmq.action-types';
import { createAction, props } from '@ngrx/store';
import { PDMQVariable } from 'pdmq/dist/interfaces/variable.interface';
import { FetchResponseInterface } from 'src/app/interfaces/common/fetch-response.interface';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';

const FETCH_VARIABLE = createAction(
  PDMQActionTypes.FETCH_VARIABLE,
  props<{pagination: PaginationInterface}>()
);

const FETCH_VARIABLE_SUCCESS = createAction(
  PDMQActionTypes.FETCH_VARIABLE_SUCCESS,
  props<FetchResponseInterface<PDMQVariable>>()
);

const SAVE_VARIABLE = createAction(
  PDMQActionTypes.SAVE_VARIABLE,
  props<{ 
    payload: PDMQVariable,
    cb?: Function
   }>()
);

const SAVE_VARIABLE_SUCCESS = createAction(
  PDMQActionTypes.SAVE_VARIABLE_SUCCESS,
  props<{
    result: PDMQVariable
  }>()
);

const DELETE_VARIABLE = createAction(
  PDMQActionTypes.DELETE_VARIABLE,
  props<{
    variable_id: string,
    cb?: Function
  }>()
);

const DELETE_VARIABLE_SUCCESS = createAction(
  PDMQActionTypes.DELETE_VARIABLE_SUCCESS,
  props<{ variable_id: string }>()
);

const FETCH_NAMES = createAction(
  PDMQActionTypes.FETCH_NAMES
);

const FETCH_NAMES_SUCCESS = createAction(
  PDMQActionTypes.FETCH_NAMES_SUCCESS,
  props<{ results: string[] }>()
);

const FETCH_VARIABLES_EXAMPLE = createAction(
  PDMQActionTypes.FETCH_VARIABLES_EXAMPLE
);

const FETCH_VARIABLES_EXAMPLE_SUCCESS = createAction(
  PDMQActionTypes.FETCH_VARIABLES_EXAMPLE_SUCCESS,
  props<{ results: {text: string, value: string}[] }>()
);


export const PDMQVariableActions = {
  FETCH_VARIABLE,
  FETCH_VARIABLE_SUCCESS,
  FETCH_NAMES,
  FETCH_NAMES_SUCCESS,
  SAVE_VARIABLE,
  SAVE_VARIABLE_SUCCESS,
  DELETE_VARIABLE,
  DELETE_VARIABLE_SUCCESS,
  FETCH_VARIABLES_EXAMPLE,
  FETCH_VARIABLES_EXAMPLE_SUCCESS
}

