import { filter, map, mergeMap, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SystemSettingActions } from './system-settings.actions';
import { SystemSettingsService } from 'src/app/services/store/system-settings.service';

@Injectable()
export class SystemSettingEffects {

  constructor(
    private actions$: Actions,
    private _settings: SystemSettingsService
  ) {}

  FETCH$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SystemSettingActions.FETCH),
      mergeMap((action) => {
        return this._settings.fetch()
      }),
      map(res => {
        return SystemSettingActions.FETCH_SUCCESS(res)
      })
    )
  })

  FETCH_BY_NAME$ = createEffect(() => {
    let cb: Function
    return this.actions$.pipe(
      ofType(SystemSettingActions.FETCH_BY_NAME),
      tap((action) => cb = action.cb),
      mergeMap((action) => {
        return this._settings.fetchByName(
          action.name
        )
      }),
      tap((res) => cb && cb(res?.result || {})),
      filter(res => res?.result),
      map(res => {
        return SystemSettingActions.FETCH_BY_NAME_SUCCESS(res)
      })
    )
  })

  SAVE$ = createEffect(() => {
    let cb: Function;
    return this.actions$.pipe(
      ofType(SystemSettingActions.SAVE),
      mergeMap((action) => {
        cb = action.cb;
        return this._settings.save(
          action.payload
        )
      }),
      tap((res) => {
        res.result && cb && cb(res.result);
      }),
      map(res => {
        return SystemSettingActions.SAVE_SUCCESS(res)
      })
    )
  })

}
