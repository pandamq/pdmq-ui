import { createSelector } from "@ngrx/store";
import { SystemSettings } from "src/app/enums/system-settings.enum";
import { SystemSettingInterface } from "src/app/interfaces/schema/system-setting.interface";
import { AppState } from "../store";

const listAll = (state: AppState) => Object.values(state.SystemSetting?.entities || {})
const count = (state: AppState) => state.SystemSetting.count;

export const SystemSettingSelectors = {
  listAll,
  count,
  item: (name: SystemSettings) => {
    return createSelector(
      listAll,
      (entities: SystemSettingInterface[]) => {
        return entities.find(item => item.system_setting_name == name)
      }
    )
  } 
}
