import { createAction, props } from '@ngrx/store';
import { SystemSettings } from 'src/app/enums/system-settings.enum';
import { FetchResponseInterface } from 'src/app/interfaces/common/fetch-response.interface';
import { SystemSettingInterface } from 'src/app/interfaces/schema/system-setting.interface';


export const SystemSettingActionTypes = {
  FETCH: "[SYSTEM SETTINGS] FETCH",
  FETCH_SUCCESS: "[SYSTEM SETTINGS] FETCH SUCCESS",
  SAVE: "[SYSTEM SETTINGS] SAVE",
  SAVE_SUCCESS: "[SYSTEM SETTINGS] SAVE SUCCESS",
  FETCH_BY_NAME: "[SYSTEM SETTINGS] FETCH BY NAME",
  FETCH_BY_NAME_SUCCESS: "[SYSTEM SETTINGS] FETCH BY NAME SUCCESS"
}

const FETCH = createAction(
  SystemSettingActionTypes.FETCH
);

const FETCH_SUCCESS = createAction(
  SystemSettingActionTypes.FETCH_SUCCESS,
  props<FetchResponseInterface<SystemSettingInterface>>()
);

const FETCH_BY_NAME = createAction(
  SystemSettingActionTypes.FETCH_BY_NAME,
  props<{
    name: SystemSettings,
    cb?: Function
  }>()
);

const FETCH_BY_NAME_SUCCESS = createAction(
  SystemSettingActionTypes.FETCH_BY_NAME_SUCCESS,
  props<{result: SystemSettingInterface}>()
);


const SAVE = createAction(
  SystemSettingActionTypes.SAVE,
  props<{ 
    payload: SystemSettingInterface,
    cb?: Function
   }>()
);

const SAVE_SUCCESS = createAction(
  SystemSettingActionTypes.SAVE_SUCCESS,
  props<{
    result: SystemSettingInterface
  }>()
);

export const SystemSettingActions = {
  FETCH,
  FETCH_SUCCESS,
  FETCH_BY_NAME,
  FETCH_BY_NAME_SUCCESS,
  SAVE,
  SAVE_SUCCESS,
}

