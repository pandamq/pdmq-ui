import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { SystemSettingInterface } from 'src/app/interfaces/schema/system-setting.interface';
import { SystemSettingActions } from './system-settings.actions';

export interface SystemSettingState extends EntityState<SystemSettingInterface> {
  count: number
}

const adapter: EntityAdapter<SystemSettingInterface> = createEntityAdapter<SystemSettingInterface>({
  selectId: (item) => item._id
});

const initialState: SystemSettingState = adapter.getInitialState({
  count: 0
});

const reducer = createReducer(
  initialState,

  on(SystemSettingActions.FETCH_SUCCESS, (state, { results, count }) => {
    return adapter.setAll(results, {
      ...state,
      count
    });
  }),

  on(SystemSettingActions.FETCH_BY_NAME_SUCCESS, (state, { result }) => {
    return adapter.upsertOne(result, state);
  }),

  on(SystemSettingActions.SAVE_SUCCESS, (state, { result }) => {
    return adapter.upsertOne(result, state);
  }),
);

export function SystemSettingReducer(state: SystemSettingState | undefined, action: Action) {
  return reducer(state, action);
}
