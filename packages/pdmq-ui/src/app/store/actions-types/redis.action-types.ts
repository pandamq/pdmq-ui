export const RedisActionTypes = {
  CREATE_CONNECTION: "[Redis] CREATE CONNECTION",
  CREATE_CONNECTION_SUCCESS: "[Redis] CREATE CONNECTION SUCCESS",
  CREATE_CONNECTION_FAILED: "[Redis] CREATE CONNECTION FAILED",
  DISCONNECT: "[Redis] CLOSE CONNECTION",
  DISCONNECT_SUCCESS: "[Redis] CLOSE CONNECTION SUCCESS",
}
