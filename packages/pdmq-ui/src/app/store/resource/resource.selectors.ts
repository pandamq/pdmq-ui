import { createSelector } from "@ngrx/store";
import { ResourceInterface } from "src/app/interfaces/schema/resource.interface";
import { AppState } from "../store";

const entities = (state: AppState) => state.Resource?.entities || {}
const listAll = (state: AppState) => Object.values(state.Resource?.entities || {})
const count = (state: AppState) => state.Resource.count;

export const ResourceSelectors = {
  listAll: createSelector(
    listAll,
    (resources: ResourceInterface[]) => {
      return resources
    }
  ),
  item: (id) => createSelector(
    entities,
    entities => {
      return entities[id]
    }
  ),
  count: createSelector(
    count,
    (count: number) => {
      return count
    }
  )
}
