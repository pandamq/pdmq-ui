import { filter, map, mergeMap, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ResourceActions } from './resource.actions';
import { ResourcesService } from 'src/app/services/store/permissions/resources.service';

@Injectable()
export class ResourceEffects {

  constructor(
    private actions$: Actions,
    private _resource: ResourcesService
  ) {}

  FETCH$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ResourceActions.FETCH),
      mergeMap((action) => {
        return this._resource.fetch(action.pagination)
      }),
      map(res => {
        return ResourceActions.FETCH_SUCCESS(res)
      })
    )
  })

  FETCH_SUMMARY$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ResourceActions.FETCH_SUMMARY),
      mergeMap(() => {
        return this._resource.fetchSummary()
      }),
      map(res => {
        return ResourceActions.FETCH_SUMMARY_SUCCESS(res)
      })
    )
  })

  SAVE$ = createEffect(() => {
    let cb: Function;
    return this.actions$.pipe(
      ofType(ResourceActions.SAVE),
      mergeMap((action) => {
        cb = action.cb;
        return this._resource.save(
          action.payload
        )
      }),
      tap((res) => {
        res.result && cb && cb(res.result);
      }),
      map(res => {
        return ResourceActions.SAVE_SUCCESS(res)
      })
    )
  })

  DELETE$ = createEffect(() => {
    let cb: Function;
    return this.actions$.pipe(
      ofType(ResourceActions.DELETE),
      mergeMap((action) => {
        cb = action.cb;
        return this._resource.delete(
          action.id
        )
      }),
      tap((res) => {
        cb && cb(res);
      }),
      map(res => {
        return ResourceActions.DELETE_SUCCESS({
          id: res
        })
      })
    )
  })

}
