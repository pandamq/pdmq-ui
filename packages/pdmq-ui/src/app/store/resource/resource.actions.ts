import { createAction, props } from '@ngrx/store';
import { FetchResponseInterface } from 'src/app/interfaces/common/fetch-response.interface';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';
import { ResourceInterface } from 'src/app/interfaces/schema/resource.interface';


export const ResourceActionTypes = {
  FETCH                 : "[RESOURCE] FETCH",
  FETCH_SUCCESS         : "[RESOURCE] FETCH SUCCESS",
  FETCH_SUMMARY         : "[RESOURCE] FETCH SUMMARY",
  FETCH_SUMMARY_SUCCESS : "[RESOURCE] FETCH SUMMARY SUCCESS",
  SAVE                  : "[RESOURCE] SAVE",
  SAVE_SUCCESS          : "[RESOURCE] SAVE SUCCESS",
  DELETE                : "[RESOURCE] DELETE",
  DELETE_SUCCESS        : "[RESOURCE] DELETE SUCCESS"
}

const FETCH = createAction(
  ResourceActionTypes.FETCH,
  props<{
    pagination: PaginationInterface
  }>()
);

const FETCH_SUCCESS = createAction(
  ResourceActionTypes.FETCH_SUCCESS,
  props<FetchResponseInterface<ResourceInterface>>()
);

const FETCH_SUMMARY = createAction(
  ResourceActionTypes.FETCH_SUMMARY
);

const FETCH_SUMMARY_SUCCESS = createAction(
  ResourceActionTypes.FETCH_SUMMARY_SUCCESS,
  props<FetchResponseInterface<ResourceInterface>>()
);


const SAVE = createAction(
  ResourceActionTypes.SAVE,
  props<{ 
    payload: ResourceInterface,
    cb?: Function
   }>()
);

const SAVE_SUCCESS = createAction(
  ResourceActionTypes.SAVE_SUCCESS,
  props<{
    result: ResourceInterface
  }>()
);

const DELETE = createAction(
  ResourceActionTypes.DELETE,
  props<{ 
    id: string,
    cb?: Function
  }>()
);

const DELETE_SUCCESS = createAction(
  ResourceActionTypes.DELETE_SUCCESS,
  props<{
    id: string
  }>()
);

export const ResourceActions = {
  FETCH,
  FETCH_SUCCESS,
  FETCH_SUMMARY,
  FETCH_SUMMARY_SUCCESS,
  SAVE,
  SAVE_SUCCESS,
  DELETE,
  DELETE_SUCCESS
}

