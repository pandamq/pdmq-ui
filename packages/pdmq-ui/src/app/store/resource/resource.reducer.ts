import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { ResourceInterface } from 'src/app/interfaces/schema/resource.interface';
import { ResourceActions } from './resource.actions';

export interface ResourceState extends EntityState<ResourceInterface> {
  count: number
}

const adapter: EntityAdapter<ResourceInterface> = createEntityAdapter<ResourceInterface>({
  selectId: (item) => item._id
});

const initialState: ResourceState = adapter.getInitialState({
  count: 0
});

const reducer = createReducer(
  initialState,

  on(ResourceActions.FETCH_SUCCESS, (state, { results, count }) => {
    return adapter.setAll(results, {
      ...state,
      count
    });
  }),

  on(ResourceActions.SAVE_SUCCESS, (state, { result }) => {
    return adapter.upsertOne(result, state);
  }),

  on(ResourceActions.DELETE_SUCCESS, (state, { id }) => {
    return adapter.removeOne(id, state)
  }),
);

export function ResourceReducer(state: ResourceState | undefined, action: Action) {
  return reducer(state, action);
}
