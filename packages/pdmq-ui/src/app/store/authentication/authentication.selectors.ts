import { createSelector } from "@ngrx/store";
import { AppState } from "../store";
import { AuthenticationState } from "./authentication.reducer";

const state = (state: AppState) => state.Authentication

const isLoggedIn = createSelector(
  state,
  (state: AuthenticationState) => {
    return !!state.login?._id
  }
)

const user = createSelector(
  state,
  (state: AuthenticationState) => {
    return state.login
  }
)

const accessKey = createSelector(
  state,
  (state: AuthenticationState) => {
    return state.accessKey
  }
)

export const AuthenticationSelector = {
  isLoggedIn,
  accessKey,
  user
}
