import { catchError, combineAll, map, mergeMap, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AuthenticationActions } from './authentication.actions';
import { AuthenticationService } from 'src/app/services/store/authentication.service';
import { StorageMap } from '@ngx-pwa/local-storage';
import { RequestService } from 'src/app/services/common/request.service';

@Injectable()
export class AuthenticationEffects {

  private doAppLoadCallback: Function;

  constructor(
    private _actions: Actions,
    private _http: RequestService,
    private _storage: StorageMap,
    private _authentication: AuthenticationService
  ) {}

  LOGIN$ = createEffect(() => {
    return this._actions.pipe(
      ofType(AuthenticationActions.LOGIN),
      mergeMap((action) => this._authentication.login({
        email: action.email,
        password: action.password
      })),
      map(res => {
        return AuthenticationActions.LOGIN_SUCCESS(res);
      })
    )
  })

  DO_APP_LOAD$ = createEffect(() => {
    let token: string, refreshToken: string;
    return this._actions.pipe(
      ofType(AuthenticationActions.DO_APP_LOAD),
      mergeMap((action) => {
        if (action.cb)
          this.doAppLoadCallback = action.cb;
        return this._storage.get('token')
      }),
      tap((data: string) => {
        token = data;
      }),
      mergeMap(() => {
        return this._storage.get('refreshToken')
      }),
      tap((data: string) => {
        refreshToken = data;
      }),
      map(data => {
        return data ? AuthenticationActions.VERIFY_TOKEN({
          token,
          refreshToken
        }) : AuthenticationActions.DO_APP_LOAD_COMPLETED();
      })
    )
  })

  VERIFY_TOKEN$ = createEffect(() => {
    return this._actions.pipe(
      ofType(AuthenticationActions.VERIFY_TOKEN),
      mergeMap((action) => {
        return this._authentication.verifyToken(action.token, action.refreshToken).pipe(
          catchError(error => {
            return this._storage.clear()
          })
        );
      }),
      map((res) => {
        return AuthenticationActions.LOGIN_SUCCESS(res);
      })
    )
  })

  LOGIN_SUCCESS$ = createEffect(() => {
    return this._actions.pipe(
      ofType(AuthenticationActions.LOGIN_SUCCESS),
      tap(action => {
        this._http.setTokens(action.tokens);
      }),
      map(() => {
        return AuthenticationActions.DO_APP_LOAD_COMPLETED();
      })
    )
  })

  DO_APP_LOAD_COMPLETED$ = createEffect(() => {
    return this._actions.pipe(
      ofType(AuthenticationActions.DO_APP_LOAD_COMPLETED),
      tap(() => {
        if (this.doAppLoadCallback) {
          try {
            this.doAppLoadCallback();
          } catch (error) {
            console.error(AuthenticationActions.DO_APP_LOAD_COMPLETED, error);
          }
        }
      })
    )
  }, {dispatch: false})

  LOGOUT$ = createEffect(() => {
    return this._actions.pipe(
      ofType(AuthenticationActions.LOGOUT),
      mergeMap(action => {
        return this._storage.clear();
      }),
      map(() => AuthenticationActions.LOGOUT_SUCCESS())
    )
  })

  GENERATE_ACCESS_KEY$ = createEffect(() => {
    return this._actions.pipe(
      ofType(AuthenticationActions.GENERATE_ACCESS_KEY),
      mergeMap(action => {
        return this._authentication.generateAccessKey();
      }),
      map((res) => AuthenticationActions.GENERATE_ACCESS_KEY_SUCCESS(res))
    )
  })

  FETCH_ACCESS_KEY$ = createEffect(() => {
    return this._actions.pipe(
      ofType(AuthenticationActions.FETCH_ACCESS_KEY),
      mergeMap(action => {
        return this._authentication.fetchAccessKey();
      }),
      map((res) => AuthenticationActions.FETCH_ACCESS_KEY_SUCCESS(res))
    )
  })
}
