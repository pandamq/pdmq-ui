import { createAction, props } from '@ngrx/store';
import { LoginInterface } from 'src/app/interfaces/schema/login.interface';
import { ResourceInterface } from 'src/app/interfaces/schema/resource.interface';
import { RoleInterface } from 'src/app/interfaces/schema/role.interface';
import { AuthenticationActionTypes } from '../actions-types/authentication.action-types';

const LOGIN = createAction(
  AuthenticationActionTypes.LOGIN,
  props<{ 
    email: string,
    password: string
  }>()
);

const LOGIN_SUCCESS = createAction(
  AuthenticationActionTypes.LOGIN_SUCCESS,
  props<{ 
    login     : LoginInterface, 
    resources : ResourceInterface[],
    roles     : RoleInterface[],
    tokens    : {
      token: string,
      refreshToken: string
    } 
  }>()
);

const DO_APP_LOAD = createAction(
  AuthenticationActionTypes.DO_APP_LOAD,
  props<{
    cb?: Function
  }>()
);

const DO_APP_LOAD_COMPLETED = createAction(
  AuthenticationActionTypes.DO_APP_LOAD_COMPLETED
);

const LOGOUT = createAction(
  AuthenticationActionTypes.LOGOUT
);

const LOGOUT_SUCCESS = createAction(
  AuthenticationActionTypes.LOGOUT_SUCCESS
);

const VERIFY_TOKEN = createAction(
  AuthenticationActionTypes.VERIFY_TOKEN,
  props<{ 
    token: string,
    refreshToken: string
  }>()
);

const GENERATE_ACCESS_KEY = createAction(
  AuthenticationActionTypes.GENERATE_ACCESS_KEY
);

const GENERATE_ACCESS_KEY_SUCCESS = createAction(
  AuthenticationActionTypes.GENERATE_ACCESS_KEY_SUCCESS,
  props<{ 
    key: string,
    path: string,
    method: string,
    lastAccessedAt: string
  }>()
);

const FETCH_ACCESS_KEY = createAction(
  AuthenticationActionTypes.FETCH_ACCESS_KEY
);

const FETCH_ACCESS_KEY_SUCCESS = createAction(
  AuthenticationActionTypes.FETCH_ACCESS_KEY_SUCCESS,
  props<{ 
    key: string,
    path: string,
    method: string,
    lastAccessedAt: string
  }>()
);

export const AuthenticationActions = {
  LOGIN,
  LOGIN_SUCCESS,
  LOGOUT,
  LOGOUT_SUCCESS,
  DO_APP_LOAD,
  DO_APP_LOAD_COMPLETED,
  VERIFY_TOKEN,
  GENERATE_ACCESS_KEY,
  GENERATE_ACCESS_KEY_SUCCESS,
  FETCH_ACCESS_KEY,
  FETCH_ACCESS_KEY_SUCCESS
}

