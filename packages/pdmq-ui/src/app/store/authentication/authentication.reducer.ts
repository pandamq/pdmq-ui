import { Action, createReducer, on } from '@ngrx/store';
import { LoginInterface } from 'src/app/interfaces/schema/login.interface';
import { ResourceInterface } from 'src/app/interfaces/schema/resource.interface';
import { RoleInterface } from 'src/app/interfaces/schema/role.interface';
import { AuthenticationActions } from './authentication.actions';


export interface AuthenticationState {
  login     : LoginInterface,
  roles     : RoleInterface[],
  resources : ResourceInterface[],
  accessKey : {
    key?: string,
    path?: string,
    method?: string,
    lastAccessedAt?: string
  },
  tokens    : {
    token         ?: string,
    refreshToken  ?: string
  }
}

const initialState: AuthenticationState = {
  login: {},
  roles: [],
  resources: [],
  tokens: {},
  accessKey: {
    key: '',
    path: '',
    method: '',
    lastAccessedAt: ''
  }
}

const reducer = createReducer(
  initialState,
  
  on(AuthenticationActions.LOGOUT_SUCCESS, (state) => {
    return initialState;
  }),
  
  on(AuthenticationActions.GENERATE_ACCESS_KEY_SUCCESS, (state, action) => {
    return {
      ...state,
      accessKey: {
        key: action.key || '',
        path: action.path || '',
        method: action.method || '',
        lastAccessedAt: action.lastAccessedAt || ''
      }
    };
  }),
  
  on(AuthenticationActions.FETCH_ACCESS_KEY_SUCCESS, (state, action) => {
    return {
      ...state,
      accessKey: {
        key: action.key || '',
        path: action.path || '',
        method: action.method || '',
        lastAccessedAt: action.lastAccessedAt || ''
      }
    };
  }),

  on(AuthenticationActions.LOGIN_SUCCESS, (state, { login, resources, roles, tokens }) => {
    return {
      ...state,
      login, 
      resources, 
      roles, 
      tokens
    }
  }),
);

export function AuthenticationReducer(state: AuthenticationState, action: Action) {
  return reducer(state, action);
}
