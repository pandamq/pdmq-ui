import { AuthenticationState } from "./authentication/authentication.reducer";
import { PDMQTaskState } from "./pdmq-task/pdmq-task.reducer";
import { PDMQVariableState } from "./pdmq-variable/pdmq-variable.reducer";
import { PDMQWorkflowExecutionLogState } from "./pdmq-workflow-execution-log/pdmq-workflow-execution-log.reducer";
import { PDMQWorkflowState } from "./pdmq-workflow/pdmq-workflow.reducer";
import { ResourceState } from "./resource/resource.reducer";
import { RoleState } from "./role/role.reducer";
import { PDMQStatusState } from "./status/pdmq-status.reducer";
import { SystemSettingState } from "./system-settings/system-settings.reducer";
import { UserState } from "./user/user.reducer";

export interface AppState {
  User          : UserState,
  Role          : RoleState,
  Resource      : ResourceState,

  Authentication: AuthenticationState,
  SystemSetting : SystemSettingState,
  PDMQTask      : PDMQTaskState,
  PDMQStatus    : PDMQStatusState,
  PDMQVariable  : PDMQVariableState,
  PDMQWorkflow  : PDMQWorkflowState,
  PDMQWorkflowExecutionLog: PDMQWorkflowExecutionLogState,
}
