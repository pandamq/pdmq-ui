import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { WorkflowActions } from './pdmq-workflow.actions';
import { PDMQWorkflow } from 'pdmq/dist/interfaces/workflow.interface';
import { WorkflowCronInterface } from 'src/app/interfaces/workflow-cron.interface';
import { cloneDeep } from 'lodash';

export interface PDMQWorkflowState extends EntityState<PDMQWorkflow> {
  count: number,
  cronList: WorkflowCronInterface[]
}

const adapter: EntityAdapter<PDMQWorkflow> = createEntityAdapter<PDMQWorkflow>({
  selectId: (workflow) => workflow._id
});

const initialState: PDMQWorkflowState = adapter.getInitialState({
  count: 0,
  cronList: []
});

const reducer = createReducer(
  initialState,

  on(WorkflowActions.FETCH_WORKFLOW_SUCCESS, (state, { results, count }) => {
    return adapter.setAll(results, {
      ...state,
      count
    });
  }),

  on(WorkflowActions.FETCH_WORKFLOW_CRON_LIST_SUCCESS, (state, { results, count }) => {
    return {
      ...state,
      cronList: results
    }
  }),

  on(WorkflowActions.SAVE_WORKFLOW_SUCCESS, (state, { result }) => {
    return adapter.upsertOne(result, state);
  }),

  on(WorkflowActions.DELETE_WORKFLOW_SUCCESS, (state, {id}) => {
    return adapter.removeOne(id, state);
  }),

  on(WorkflowActions.FETCH_WORKFLOW_ITEM_SUCCESS, (state, { result }) => {
    return adapter.upsertOne(result, state);
  }),

  on(WorkflowActions.TOGGLE_WORKFLOW_CRON_STATUS_SUCCESS, (state, { result }) => {
    const list = cloneDeep(state.cronList);
    const cronListIndex = list.findIndex(job => job.workflow._id == result.workflow._id);
    if (cronListIndex == -1) {
      list.push(result)
    }
    else {
      list[cronListIndex] = result
    }
    return {
      ...state,
      cronList: list
    }
  })
);

export function PDMQWorkflowReducer(state: PDMQWorkflowState | undefined, action: Action) {
  return reducer(state, action);
}
