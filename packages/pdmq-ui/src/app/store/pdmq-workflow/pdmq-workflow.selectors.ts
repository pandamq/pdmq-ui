import { PDMQWorkflowState } from './pdmq-workflow.reducer';
import { createSelector } from "@ngrx/store";
import { AppState } from "../store";

const state   = (state: AppState) => state.PDMQWorkflow;
const listAll = (state: AppState) => Object.values(state.PDMQWorkflow?.entities || {});
const count   = (state: AppState) => state.PDMQWorkflow.count;

const cronList = createSelector(
  state,
  (state: PDMQWorkflowState) => {
    return state.cronList
  }
)

export const PDMQWorkflowSelectors = {
  listAll,
  cronList,
  count
}
