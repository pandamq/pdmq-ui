import { PDMQActionTypes } from '../actions-types/pdmq.action-types';
import { createAction, props } from '@ngrx/store';
import { PDMQWorkflow } from 'pdmq/dist/interfaces/workflow.interface';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';
import { FetchResponseInterface } from 'src/app/interfaces/common/fetch-response.interface';
import { WorkflowCronInterface } from 'src/app/interfaces/workflow-cron.interface';

const FETCH_WORKFLOW = createAction(
  PDMQActionTypes.FETCH_WORKFLOW,
  props<{ pagination: PaginationInterface }>()
);

const FETCH_WORKFLOW_SUCCESS = createAction(
  PDMQActionTypes.FETCH_WORKFLOW_SUCCESS,
  props<FetchResponseInterface<PDMQWorkflow>>()
);

const FETCH_WORKFLOW_CRON_LIST = createAction(
  PDMQActionTypes.FETCH_WORKFLOW_CRON_LIST
);

const FETCH_WORKFLOW_CRON_LIST_SUCCESS = createAction(
  PDMQActionTypes.FETCH_WORKFLOW_CRON_LIST_SUCCESS,
  props<FetchResponseInterface<WorkflowCronInterface>>()
);


const SAVE_WORKFLOW = createAction(
  PDMQActionTypes.SAVE_WORKFLOW,
  props<{ 
    payload: PDMQWorkflow,
    cb?: Function
  }>()
);

const SAVE_WORKFLOW_SUCCESS = createAction(
  PDMQActionTypes.SAVE_WORKFLOW_SUCCESS,
  props<{ result: PDMQWorkflow }>()
);

const DELETE_WORKFLOW = createAction(
  PDMQActionTypes.DELETE_WORKFLOW,
  props<{ 
    id: string,
    cb?: Function
  }>()
);

const DELETE_WORKFLOW_SUCCESS = createAction(
  PDMQActionTypes.DELETE_WORKFLOW_SUCCESS,
  props<{ id: string }>()
);

const FETCH_WORKFLOW_ITEM = createAction(
  PDMQActionTypes.FETCH_WORKFLOW_ITEM,
  props<{ 
    id: string,
    cb?: Function
  }>()
);

const FETCH_WORKFLOW_ITEM_SUCCESS = createAction(
  PDMQActionTypes.FETCH_WORKFLOW_ITEM_SUCCESS,
  props<{ result: PDMQWorkflow  }>()
);

const RUN_WORKFLOW = createAction(
  PDMQActionTypes.RUN_WORKFLOW,
  props<{
    workflowId: string,
    cb?: Function
  }>()
)

const STOP_WORKFLOW = createAction(
  PDMQActionTypes.STOP_WORKFLOW,
  props<{
    workflowId: string,
    cb?: Function
  }>()
)

const TOGGLE_WORKFLOW_CRON_STATUS = createAction(
  PDMQActionTypes.TOGGLE_WORKFLOW_CRON_STATUS,
  props<{ 
    workflowId: string,
    cb?: Function
  }>()
);

const TOGGLE_WORKFLOW_CRON_STATUS_SUCCESS = createAction(
  PDMQActionTypes.TOGGLE_WORKFLOW_CRON_STATUS_SUCCESS,
  props<{ result: WorkflowCronInterface  }>()
);

export const WorkflowActions = {
  RUN_WORKFLOW,
  STOP_WORKFLOW,
  FETCH_WORKFLOW,
  FETCH_WORKFLOW_SUCCESS,
  FETCH_WORKFLOW_ITEM,
  FETCH_WORKFLOW_ITEM_SUCCESS,
  SAVE_WORKFLOW,
  SAVE_WORKFLOW_SUCCESS,
  DELETE_WORKFLOW,
  DELETE_WORKFLOW_SUCCESS,
  FETCH_WORKFLOW_CRON_LIST,
  FETCH_WORKFLOW_CRON_LIST_SUCCESS,
  TOGGLE_WORKFLOW_CRON_STATUS,
  TOGGLE_WORKFLOW_CRON_STATUS_SUCCESS
}

