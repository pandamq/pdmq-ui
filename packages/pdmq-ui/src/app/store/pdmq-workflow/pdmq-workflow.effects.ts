import { map, mergeMap, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { WorkflowActions } from './pdmq-workflow.actions';
import { WorkflowService } from 'src/app/services/pdmq/workflow.service';

@Injectable()
export class PDMQWorkflowEffects {

  constructor(
    private actions$: Actions,
    private _pdmq: WorkflowService
  ) {}

  RUN_WORKFLOW$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowActions.RUN_WORKFLOW),
    tap(async (action) => {
      await this._pdmq.run(action.workflowId).toPromise();
      action.cb && action.cb();
    }),
  ), {
    dispatch: false
  })

  STOP_WORKFLOW$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowActions.STOP_WORKFLOW),
    tap(async (action) => {
      await this._pdmq.stop(action.workflowId).toPromise();
      action.cb && action.cb();
    }),
  ), {
    dispatch: false
  })

  FETCH_WORKFLOW$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowActions.FETCH_WORKFLOW),
    mergeMap((action) => this._pdmq.fetchWorkflows(action.pagination)),
    map(res => {
      return WorkflowActions.FETCH_WORKFLOW_SUCCESS(res)
    })
  ))

  FETCH_WORKFLOW_CRON_LIST$ = createEffect(() => this.actions$.pipe(
    ofType(WorkflowActions.FETCH_WORKFLOW_CRON_LIST),
    mergeMap(() => this._pdmq.fetchWorkflowCronList()),
    map(res => {
      return WorkflowActions.FETCH_WORKFLOW_CRON_LIST_SUCCESS(res)
    })
  ))

  SAVE_WORKFLOW$ = createEffect(() => {
    let cb: Function;
    return this.actions$.pipe(
      ofType(WorkflowActions.SAVE_WORKFLOW),
      mergeMap((action) => {
        cb = action.cb;
        return this._pdmq.saveWorkflow(action.payload)
      }),
      tap((res) => {
        res?.result && cb && cb(res.result)
      }),
      map(res => {
        return WorkflowActions.SAVE_WORKFLOW_SUCCESS(res)
      })
    )
  })

  DELETE_WORKFLOW$ = createEffect(() => {
    let cb: Function;
    return this.actions$.pipe(
      ofType(WorkflowActions.DELETE_WORKFLOW),
      tap((action) => {
        cb = action.cb
      }),
      mergeMap((action) => this._pdmq.deleteWorkflow(action.id)),
      tap(() => {
        cb && cb()
      }),
      map(res => {
        return WorkflowActions.DELETE_WORKFLOW_SUCCESS({
          id: res
        })
      })
    )
  })

  FETCH_WORKFLOW_ITEM$ = createEffect(() => {
    let cb: Function;
    return this.actions$.pipe(
      ofType(WorkflowActions.FETCH_WORKFLOW_ITEM),
      mergeMap((action) => {
        cb = action.cb;
        return this._pdmq.fetchItem(action.id);
      }),
      tap((res) => {
        res?.result && cb && cb(res.result);
      }),
      map(res => {
        return WorkflowActions.FETCH_WORKFLOW_ITEM_SUCCESS(res)
      })
    )
  })

  TOGGLE_WORKFLOW_CRON_STATUS$ = createEffect(() => {
    let cb: Function;
    return this.actions$.pipe(
      ofType(WorkflowActions.TOGGLE_WORKFLOW_CRON_STATUS),
      mergeMap((action) => {
        cb = action.cb;
        return this._pdmq.toggleWorkflowCronStatus(action.workflowId);
      }),
      tap((res) => {
        res?.result && cb && cb(res.result);
      }),
      map(res => {
        return WorkflowActions.TOGGLE_WORKFLOW_CRON_STATUS_SUCCESS(res)
      })
    )
  })
}
