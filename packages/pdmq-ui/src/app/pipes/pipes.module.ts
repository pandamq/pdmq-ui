import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StringifyPipe } from './stringify.pipe';



@NgModule({
  declarations: [
    StringifyPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    StringifyPipe
  ]
})
export class PipesModule { }
