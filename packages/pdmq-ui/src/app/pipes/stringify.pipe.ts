import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringify'
})
export class StringifyPipe implements PipeTransform {

  transform(value: any): string {
    if (value === null) return "";
    else if (typeof value == 'string') return value;
    else if (typeof value == 'number') return value.toString();
    else if (typeof value == 'object') return JSON.stringify(value);
    return value?.toString() || "";
  }

}
