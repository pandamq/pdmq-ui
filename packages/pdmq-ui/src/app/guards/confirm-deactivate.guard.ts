import { Injectable } from '@angular/core';
import { CanActivate, CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { EditWorkflowDetailComponent } from '../dashboard/workflows/edit-workflow-detail/edit-workflow-detail.component';
import { CommunicatorService } from '../services/common/communicator.service';

@Injectable({
  providedIn: 'root'
})
export class ConfirmDeactivateGuard implements CanDeactivate<EditWorkflowDetailComponent> {
  
  constructor(
    private _comm: CommunicatorService
  ) {}

  async canDeactivate(
    component: EditWorkflowDetailComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Promise<boolean> 
  {
    if (!component.hasChange) return true;
    return this._comm.confirm("Are you sure you want to discard all changes?");
  }
  
}
