import { FieldTypeOptions } from "pdmq/dist/constants/field-type-options.const";
import { WorkflowNodeCategories } from "pdmq/dist/enums/workflow-node-categories.enum";
import { WorkflowNodeTypes } from "pdmq/dist/enums/workflow-node-types.enum";
import { WorkflowNodeInterface } from "../interfaces/workflow-node.interface";

export const WorkflowNodes: WorkflowNodeInterface[] = [
  {
    title: '<b>SEND</b> Http Request',
    input: 1,
    output: 1,
    type: WorkflowNodeTypes.SEND_HTTP_REQUEST,
    category: WorkflowNodeCategories.ACTION,
    fields: [
      {
        value: 'endpoint',
        text: 'Request Endpoint',
        type: 'string',
        required: true
      },
      {
        value: 'method',
        text: 'Method',
        type: 'string',
        required: true,
        options: [
          {
            value: "get",
            text: "Get"
          },
          {
            value: "post",
            text: "Post"
          }
        ]
      },
      {
        value: 'headers',
        text: 'Headers',
        type: 'json'
      },
      {
        value: 'body',
        text: 'Body',
        type: 'json'
      },
      {
        value: 'retry',
        text: 'Retry',
        type: 'number'
      },
      {
        value: 'retry_delay',
        text: 'Retry Delay (seconds)',
        type: 'number'
      },
      {
        value: 'result_mapping',
        text: 'Result Mapping',
        type: 'string',
        hint: "https://goessner.net/articles/JsonPath/"
      },
      {
        value: 'disabled',
        text: 'Disabled',
        type: 'boolean'
      },
    ]
  },
  {
    title: '<b>SEND</b> Email',
    input: 1,
    output: 1,
    type: WorkflowNodeTypes.SEND_EMAIL,
    category: WorkflowNodeCategories.ACTION,
    fields: [
      {
        required: true,
        value: 'recipients',
        text: 'Recipients (Split by comma)',
        type: 'string'
      },
      {
        required: true,
        value: 'subject',
        text: 'Subject',
        type: 'string'
      },
      {
        required: true,
        value: 'message',
        text: 'Message',
        type: 'textarea',
        displaySystemVariableExamples: true 
      },
      {
        value: 'disabled',
        text: 'Disabled',
        type: 'boolean'
      },
    ],
  },
  {
    title: '<b>SET</b> Variable',
    input: 1,
    output: 1,
    type: WorkflowNodeTypes.SET_VARIABLE,
    category: WorkflowNodeCategories.ACTION,
    fields: [
      {
        required: true,
        value: 'key',
        text: 'Key Name',
        type: 'string'
      },
      {
        value: 'path',
        text: 'Data Path',
        type: 'string',
        hint: "https://goessner.net/articles/JsonPath/"
      },
      {
        value: 'type',
        text: 'Variable Type',
        type: 'string',
        options: FieldTypeOptions
      },
      {
        value: 'disabled',
        text: 'Disabled',
        type: 'boolean'
      },
    ],
  },
  {
    title: '<b>START</b> Workflow',
    input: 1,
    output: 1,
    type: WorkflowNodeTypes.START_WORKFLOW,
    category: WorkflowNodeCategories.ACTION,
    fields: [
      {
        required: true,
        value: 'workflow',
        text: 'Workflow Name',
        type: 'string'
      },
      {
        value: 'disabled',
        text: 'Disabled',
        type: 'boolean'
      },
    ],
  },
  {
    title: '<b>DELAY</b> Progress',
    input: 1,
    output: 1,
    type: WorkflowNodeTypes.DELAY_PROGRESS,
    category: WorkflowNodeCategories.ACTION,
    fields: [
      {
        required: true,
        value: 'seconds',
        text: 'Seconds to Delay',
        type: 'number'
      },
      {
        value: 'disabled',
        text: 'Disabled',
        type: 'boolean'
      },
    ],
  },
  {
    title: '<b>IF</b> Succeed',
    input: 1,
    output: 1,
    type: WorkflowNodeTypes.IF_SUCCEED,
    category: WorkflowNodeCategories.CONDITION,
  },
  {
    title: '<b>IF</b> Failed',
    input: 1,
    output: 1,
    type: WorkflowNodeTypes.IF_FAILED,
    category: WorkflowNodeCategories.CONDITION,
  },
  {
    title: '<b>IF</b> Equals to',
    input: 1,
    output: 1,
    type: WorkflowNodeTypes.IF_VARIABLE_EQUALS,
    category: WorkflowNodeCategories.CONDITION,
    fields: [
      {
        value: 'variable_name',
        text: 'Variable Name',
        type: 'string'
      },
      {
        value: 'value',
        text: 'Value',
        type: 'string'
      }
    ],
  },
  {
    title: '<b>IF NOT</b> Equals to',
    input: 1,
    output: 1,
    type: WorkflowNodeTypes.IF_NOT_EQUALS,
    category: WorkflowNodeCategories.CONDITION,
    fields: [
      {
        value: 'variable_name',
        text: 'Variable Name',
        type: 'string'
      },
      {
        value: 'value',
        text: 'Value',
        type: 'string'
      }
    ],
  }
]