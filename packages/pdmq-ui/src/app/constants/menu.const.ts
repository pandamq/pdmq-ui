import { environment } from "src/environments/environment";

export const MENU = [
  {
    panelClass: ["border-top"],
    text: "Dashboard",
    url: '/dashboard',
    icon: 'dashboard'
  },
  {
    panelClass: [],
    text: "Variables",
    url: '/dashboard/variables',
    icon: 'account_tree'
  },
  {
    panelClass: [],
    text: "Workflows",
    url: '/dashboard/workflows',
    icon: 'all_inclusive'
  },
  {
    panelClass: [],
    text: "Cron List",
    url: '/dashboard/cron-list',
    icon: 'timer'
  },
  {
    panelClass: [],
    text: "Upcoming Tasks",
    url: '/dashboard/timeline',
    icon: 'timeline'
  },
  {
    panelClass: [],
    text: "Tasks",
    url: '',
    icon: 'keyboard_arrow_down'
  },
  {
    panelClass: ["pl-3 text-sm"],
    text: "Stored Tasks",
    url: '/dashboard/tasks/never',
  },
  {
    panelClass: ["pl-3 text-sm"],
    text: "Instant Tasks",
    url: '/dashboard/tasks/instant',
  },
  {
    panelClass: ["pl-3 text-sm"],
    text: "Time-based Tasks",
    url: '/dashboard/tasks/time',
  },
  {
    panelClass: ["pl-3 text-sm"],
    text: "All Tasks",
    url: '/dashboard/tasks/all',
  },
  {
    panelClass: ["border-top"],
    text: "Settings",
    icon: 'keyboard_arrow_down',
    url: ''
  },
  {
    panelClass: ["pl-3 text-sm"],
    text: "My Profile",
    url: '/dashboard/settings/my-profile',
  },
  {
    panelClass: ["pl-3 text-sm"],
    text: "Permissions",
    url: '/dashboard/settings/permissions',
  },
  {
    panelClass: ["pl-3 text-sm"],
    text: "Configs",
    url: '/dashboard/settings/configs',
  },
  {
    panelClass: ["border-top"],
    text: "API Endpoints",
    icon: 'api',
    external: true,
    url: environment.API_URL + '/docs',
  }
]
