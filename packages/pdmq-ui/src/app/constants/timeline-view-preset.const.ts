export const TIMELINE_VIEW_PRESET = [
  {
    text: "Next Minute",
    value: 1
  },
  {
    text: "Next 10 Minutes",
    value: 10
  },
  {
    text: "Next Hour",
    value: 10 * 6
  }
]
