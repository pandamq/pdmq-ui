import { PDMQWorkflowSelectors } from 'src/app/store/pdmq-workflow/pdmq-workflow.selectors';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { WorkflowActions } from 'src/app/store/pdmq-workflow/pdmq-workflow.actions';
import { WorkflowCronInterface } from 'src/app/interfaces/workflow-cron.interface';
import { CommunicatorService } from 'src/app/services/common/communicator.service';

@Component({
  selector: 'app-cron-list',
  templateUrl: './cron-list.component.html',
  styleUrls: ['./cron-list.component.css']
})
export class CronListComponent implements OnInit {

  public cronList$ = this._store.select(PDMQWorkflowSelectors.cronList);

  constructor(
    private _comm: CommunicatorService,
    private _store: Store
  ) { }

  ngOnInit(): void {
    this._store.dispatch(WorkflowActions.FETCH_WORKFLOW_CRON_LIST());
  }

  /**
   * Toggle Play / Stop
   * 
   * @param cron 
   */
  async toggleCronJob(cron: WorkflowCronInterface) {
    const con = await this._comm.confirm(`Are you sure you want to ${cron.isRunning ? 'DISABLE' : 'ENABLE'} this job?`)
    if (!con) return;
    
    this._comm.showLoading();
    this._store.dispatch(WorkflowActions.TOGGLE_WORKFLOW_CRON_STATUS({
      workflowId: cron.workflow._id,
      cb: () => {
        this._comm.hideLoading();
      }
    }));
  }

}
