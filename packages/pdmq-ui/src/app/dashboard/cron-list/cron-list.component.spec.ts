import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CronListComponent } from './cron-list.component';

describe('CronListComponent', () => {
  let component: CronListComponent;
  let fixture: ComponentFixture<CronListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CronListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CronListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
