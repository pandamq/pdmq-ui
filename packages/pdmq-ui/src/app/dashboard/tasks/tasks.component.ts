import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { PDMQTasksActions } from 'src/app/store/pdmq-task/pdmq-task.actions';
import * as moment from "moment";
import { MatDialog } from '@angular/material/dialog';
import { EditTaskDetailComponent } from 'src/app/components/modal/edit-task-detail/edit-task-detail.component';
import { Subscription } from 'rxjs';
import { CommunicatorService } from 'src/app/services/common/communicator.service';
import { Actions, ofType } from '@ngrx/effects';

import { PDMQTask } from 'pdmq/dist/interfaces/task.interface';
import { filter, mergeMap } from 'rxjs/operators';
import { PDMQTaskSelectors } from 'src/app/store/pdmq-task/pdmq-task.selectors';
import { TaskTriggerTypes } from 'src/app/enums/task-trigger-types.enum';
import { TaskTriggerTypes as TaskTriggerTypesEnum } from 'pdmq/dist/enums/task-trigger-types.enum';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  public options = {
    from: moment().startOf('day'),
    to: moment().startOf('day'),
    type: '' as TaskTriggerTypes
  };

  public subs: Subscription[] = [];
  public tasks$ = this._store.select(PDMQTaskSelectors.listAll)

  constructor(
    private _store: Store,
    private _comm: CommunicatorService,
    private _dialog: MatDialog,
    private _activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.subs.push(
      this._activatedRoute.params.subscribe(params => {
        this.options.type = params.type || TaskTriggerTypes.ALL;
        this.search();
      })
    )
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subs.forEach(sub => sub.unsubscribe());
  }


  get isTimeBased() {
    return this.options.type === TaskTriggerTypes.TIME;
  }

  search() {
    this._comm.showLoading();
    this._store.dispatch(PDMQTasksActions.FETCH_TASK({
      from: this.options.from.startOf('day').toISOString(),
      to: this.options.to.endOf('day').toISOString(),
      triggerType: this.options.type
    }))
  }

  formatDate(date: string) {
    return moment(date).format("DD MMM YYYY HH:mm:ss")
  }

  /**
   * Create a task
   */
  addTask() {
    let task_trigger_type = TaskTriggerTypesEnum.INSTANT;
    if (this.isTimeBased) task_trigger_type = TaskTriggerTypesEnum.DAILY;
    else if (this.options.type === TaskTriggerTypes.NEVER) task_trigger_type = TaskTriggerTypesEnum.NEVER;

    this._dialog.open(EditTaskDetailComponent, {
      panelClass: ["dialog", "dialog-md"],
      disableClose: true,
      data: {
        item: {
          task_trigger_type
        }
      }
    })
  }

  /**
   * Edit task
   * 
   * @param task 
   */
  editTask(task:PDMQTask) {
    this._dialog.open(EditTaskDetailComponent, {
      panelClass: ["dialog", "dialog-md"],
      data: {
        item: task
      }
    })
  }

  async delete(task:PDMQTask) {
    const con = await this._comm.confirm(`Are you sure you want to delete ${task.task_name}`, "Confirmation");
    if (!con) return;

    this._store.dispatch(PDMQTasksActions.DELETE_TASK({task_id: task.task_id}));
  }

}
