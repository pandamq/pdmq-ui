import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { PDMQVariableSelectors } from 'src/app/store/pdmq-variable/pdmq-variable.selectors';
import { PDMQVariable } from 'pdmq/dist/interfaces/variable.interface';
import { PageEvent } from '@angular/material/paginator';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';
import { PageSizeOptions } from 'src/app/constants/page-size-options.const';
import { CommunicatorService } from 'src/app/services/common/communicator.service';
import { EditVariableDetailComponent } from 'src/app/components/modal/edit-variable-detail/edit-variable-detail.component';
import { PDMQVariableActions } from 'src/app/store/pdmq-variable/pdmq-variable.actions';
import { cloneDeep } from 'lodash';

@Component({
  selector: 'app-variables',
  templateUrl: './variables.component.html',
  styleUrls: ['./variables.component.css']
})
export class VariablesComponent implements OnInit {

  public pagination: PaginationInterface = {
    index: 0,
    size: 20,
    searchKey: ''
  }

  public readonly pageSizeOptions = PageSizeOptions;

  public count$     = this._store.select(PDMQVariableSelectors.count);
  public variables$ = this._store.select(PDMQVariableSelectors.listAll);

  constructor(
    private _store: Store,
    private _comm: CommunicatorService,
    private _dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.init();
  }

  /**
   * Fetch variables
   */
  init() {
    this._store.dispatch(PDMQVariableActions.FETCH_VARIABLE({
      pagination: cloneDeep(this.pagination)
    }))
  }

  /**
   * Update pagination
   * 
   * @param evt 
   */
  fetch(evt: PageEvent) {
    this.pagination.index = evt.pageIndex;
    this.pagination.size = evt.pageSize;
    this.init();
  }

  /**
   * Edit Variable
   */
  edit(variable?: PDMQVariable) {
    this._dialog.open(
      EditVariableDetailComponent,
      {
        disableClose: true,
        panelClass: [
          "dialog",
          "dialog-md"
        ],
        data: {
          item: variable,
        }
      }
    )
  }

  /**
   * Delete variable
   * 
   * @param variableId 
   */
  async delete(variableId) {
    const con = await this._comm.confirm("Are you sure you want to delete this variable?");
    if (!con) return;
    
    this._store.dispatch(PDMQVariableActions.DELETE_VARIABLE({
      variable_id: variableId,
      cb: () => {
        this._comm.showToast("Deleted");
      }
    }))
  }
  
}
