import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { SystemSettingActions } from 'src/app/store/system-settings/system-settings.actions';
import { cloneDeep } from 'lodash';
import { SystemSettings } from 'src/app/enums/system-settings.enum';
import { CommunicatorService } from 'src/app/services/common/communicator.service';
import { SystemSettingInterface } from 'src/app/interfaces/schema/system-setting.interface';

@Component({
  selector: 'app-smtp-config',
  templateUrl: './smtp-config.component.html',
  styleUrls: ['./smtp-config.component.css']
})
export class SmtpConfigComponent implements OnInit {

  public editingItem: {
    host?: string,
    port?: number,
    username?: string,
    password?: string
  } = {};

  constructor(
    private _store: Store,
    private _comm: CommunicatorService,
  ) { }

  ngOnInit(): void {
    this._store.dispatch(SystemSettingActions.FETCH_BY_NAME({
      name: SystemSettings.SMTP,
      cb: (res: SystemSettingInterface) => {
        this.editingItem = cloneDeep(res.system_setting_data || {});
      }
    }))
  }

  get canSave() {
    return this.editingItem.host && this.editingItem.port && this.editingItem.username && this.editingItem.password
  }

  /**
   * Save Config
   */
  save() {
    this._store.dispatch(SystemSettingActions.SAVE({
      payload: cloneDeep({
        system_setting_name: SystemSettings.SMTP,
        system_setting_data: this.editingItem
      }),
      cb: () => {
        this._comm.showToast("Saved");
      }
    }))
  }

}
