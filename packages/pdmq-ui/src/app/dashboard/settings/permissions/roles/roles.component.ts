import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Store } from '@ngrx/store';
import { PageSizeOptions } from 'src/app/constants/page-size-options.const';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';
import { RoleInterface } from 'src/app/interfaces/schema/role.interface';
import { RoleActions } from 'src/app/store/role/role.actions';
import { RoleSelectors } from 'src/app/store/role/role.selectors';
import { cloneDeep } from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { EditRoleDetailComponent } from 'src/app/components/modal/edit-role-detail/edit-role-detail.component';
import { CommunicatorService } from 'src/app/services/common/communicator.service';
import { ResourceActions } from 'src/app/store/resource/resource.actions';
import { ResourceSelectors } from 'src/app/store/resource/resource.selectors';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  public pagination: PaginationInterface = {
    index: 0,
    size: 0,
    searchKey: ''
  }

  public roles$ = this._store.select(RoleSelectors.listAll);
  public count$ = this._store.select(RoleSelectors.count);
  public readonly pageSizeOptions = PageSizeOptions;

  constructor(
    private _store: Store,
    private _dialog: MatDialog,
    private _comm: CommunicatorService
  ) { }

  ngOnInit(): void {
    this._store.dispatch(ResourceActions.FETCH_SUMMARY())
    this.init();
  }

  /**
   * Fetch role list
   */
  init() {
    this._store.dispatch(RoleActions.FETCH({
      pagination: cloneDeep(this.pagination)
    }))
  }

  /**
   * Update pagination
   */
  fetch(evt: PageEvent) {
    this.pagination.size = evt.pageSize;
    this.pagination.index = evt.pageIndex;
    this.init()
  }

  /**
   * Get Resource Name by ID
   * 
   * @param id 
   */
  resourceItemName$(id: string) {
    return this._store.select(ResourceSelectors.item(id)).pipe(
      map(item => item?.resource_name || "")
    )
  }

  /**
   * Update role info
   * 
   * @param item 
   */
  async delete(item?: RoleInterface) {
    const con = await this._comm.confirm(`Are you sure you want to delete ${item.role_name}?`);
    if (!con) return;

    this._store.dispatch(RoleActions.DELETE({ id: item._id }))
  }

  /**
   * Update role info
   * 
   * @param item 
   */
  edit(item?: RoleInterface) {
    this._dialog.open(EditRoleDetailComponent, {
      panelClass: ["dialog", "dialog-md"],
      data: {
        item
      }
    })
  }

}
