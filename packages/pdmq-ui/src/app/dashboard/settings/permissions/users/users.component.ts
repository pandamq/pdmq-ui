import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { UserActions } from 'src/app/store/user/user.actions';
import { cloneDeep } from 'lodash';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';
import { PageEvent } from '@angular/material/paginator';
import { LoginInterface } from 'src/app/interfaces/schema/login.interface';
import { UserSelectors } from 'src/app/store/user/user.selectors';
import { PageSizeOptions } from 'src/app/constants/page-size-options.const';
import { MatDialog } from '@angular/material/dialog';
import { EditUserDetailComponent } from 'src/app/components/modal/edit-user-detail/edit-user-detail.component';
import { ChangePasswordDialogComponent } from 'src/app/components/modal/change-password/change-password-dialog.component';
import { RoleActions } from 'src/app/store/role/role.actions';
import { map } from 'rxjs/operators';
import { RoleSelectors } from 'src/app/store/role/role.selectors';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  public pagination: PaginationInterface = {
    index: 0,
    size: 0,
    searchKey: ''
  }

  public users$ = this._store.select(UserSelectors.listAll);
  public count$ = this._store.select(UserSelectors.count);
  public readonly pageSizeOptions = PageSizeOptions;

  constructor(
    private _store: Store,
    private _dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.init();
    this._store.dispatch(RoleActions.FETCH_SUMMARY())
  }

  /**
   * Fetch user list
   */
  init() {
    this._store.dispatch(UserActions.FETCH({
      pagination: cloneDeep(this.pagination)
    }))
  }

  /**
   * Update pagination
   */
  fetch(evt: PageEvent) {
    this.pagination.size = evt.pageSize;
    this.pagination.index = evt.pageIndex;
    this.init()
  }

  /**
   * Update user info
   * 
   * @param item 
   */
  edit(item?: LoginInterface) {
    this._dialog.open(EditUserDetailComponent, {
      panelClass: [
        "dialog",
        "dialog-md"
      ],
      data: {
        item
      }
    })
  }

  /**
   * Get Role Name by ID
   * 
   * @param id 
   */
  roleItemName$(id: string) {
    return this._store.select(RoleSelectors.item(id)).pipe(
      map(item => item?.role_name || "")
    )
  }

  changePassword(item: LoginInterface) {
    this._dialog.open(ChangePasswordDialogComponent, {
      panelClass: [
        "dialog",
        "dialog-sm"
      ],
      data: {
        item
      }
    })
  }

}
