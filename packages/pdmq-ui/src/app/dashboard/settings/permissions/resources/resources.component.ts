import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Store } from '@ngrx/store';
import { PageSizeOptions } from 'src/app/constants/page-size-options.const';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';
import { ResourceInterface } from 'src/app/interfaces/schema/resource.interface';
import { ResourceActions } from 'src/app/store/resource/resource.actions';
import { ResourceSelectors } from 'src/app/store/resource/resource.selectors';
import { cloneDeep } from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { EditResourceDetailComponent } from 'src/app/components/modal/edit-resource-detail/edit-resource-detail.component';
import { CommunicatorService } from 'src/app/services/common/communicator.service';

@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.css']
})
export class ResourcesComponent implements OnInit {

  public pagination: PaginationInterface = {
    index: 0,
    size: 0,
    searchKey: ''
  }

  public resources$ = this._store.select(ResourceSelectors.listAll);
  public count$ = this._store.select(ResourceSelectors.count);
  public readonly pageSizeOptions = PageSizeOptions;

  constructor(
    private _store: Store,
    private _dialog: MatDialog,
    private _comm: CommunicatorService
  ) { }

  ngOnInit(): void {
    this.init();
  }

  /**
   * Fetch resource list
   */
  init() {
    this._store.dispatch(ResourceActions.FETCH({
      pagination: cloneDeep(this.pagination)
    }))
  }

  /**
   * Update pagination
   */
  fetch(evt: PageEvent) {
    this.pagination.size = evt.pageSize;
    this.pagination.index = evt.pageIndex;
    this.init()
  }

  /**
   * Update resource info
   * 
   * @param item 
   */
  edit(item?: ResourceInterface) {
    this._dialog.open(EditResourceDetailComponent, {
      panelClass: ["dialog", "dialog-md"],
      data: {
        item
      }
    })
  }

  /**
   * Update role info
   * 
   * @param item 
   */
   async delete(item?: ResourceInterface) {
    const con = await this._comm.confirm(`Are you sure you want to delete ${item.resource_name}?`);
    if (!con) return;

    this._store.dispatch(ResourceActions.DELETE({ id: item._id }))
  }

}
