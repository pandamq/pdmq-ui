import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { CommunicatorService } from 'src/app/services/common/communicator.service';
import { AuthenticationActions } from 'src/app/store/authentication/authentication.actions';
import { AuthenticationSelector } from 'src/app/store/authentication/authentication.selectors';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {

  public user$ = this._store.select(AuthenticationSelector.user);

  public accessKey$ = this._store.select(AuthenticationSelector.accessKey);

  constructor(
    private _comm: CommunicatorService,
    private _store: Store
  ) { }

  ngOnInit(): void {
    this._store.dispatch(AuthenticationActions.FETCH_ACCESS_KEY());
  }

  /**
   * Generate Api Key
   */
  async generate(key?: string) {
    if (key) {
      const con = await this._comm.confirm("Are you sure you want to recreate your API key?");
      if (!con) return;
    }
    this._store.dispatch(AuthenticationActions.GENERATE_ACCESS_KEY())
  }

  /**
   * Copy API Keys
   */
  copy(str: string) {
    this._comm.copy(str);
  }

  /**
   * Navigate to api endpoints page
   */
  navigateToApiDoc() {
    window.open(environment.API_URL + '/docs');
  }

}
