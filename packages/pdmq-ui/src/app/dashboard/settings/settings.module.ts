import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PermissionsComponent } from './permissions/permissions.component';
import { ConfigsComponent } from './configs/configs.component';
import { RouterModule } from '@angular/router';
import { AppMaterialModule } from 'src/app/plugins/material/material.module';
import { SmtpConfigComponent } from './configs/smtp-config/smtp-config.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { UsersComponent } from './permissions/users/users.component';
import { RolesComponent } from './permissions/roles/roles.component';
import { ResourcesComponent } from './permissions/resources/resources.component';
import { DirectivesModule } from 'src/app/directives/directives.module';
import { ComponentsModule } from 'src/app/components/components.module';
import { AppFormModule } from 'src/app/components/form/form.module';

@NgModule({
  declarations: [
    PermissionsComponent, 
    ConfigsComponent, 
    SmtpConfigComponent, 
    MyProfileComponent, 
    UsersComponent, 
    RolesComponent, 
    ResourcesComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    AppFormModule,
    AppMaterialModule,
    DirectivesModule,
    RouterModule.forChild([
      {
        path: 'permissions',
        component: PermissionsComponent
      }, {
        path: 'configs',
        component: ConfigsComponent
      }, {
        path: 'my-profile',
        component: MyProfileComponent
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class SettingsModule { }
