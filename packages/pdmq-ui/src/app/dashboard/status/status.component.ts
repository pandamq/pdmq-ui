import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { CommunicatorService } from 'src/app/services/common/communicator.service';
import { ServerStatusService } from 'src/app/services/sockets/server-status/server-status.service';
import { WorkflowActions } from 'src/app/store/pdmq-workflow/pdmq-workflow.actions';
import { PDMQStatusSelectors } from 'src/app/store/status/pdmq-status.selectors';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css']
})
export class StatusComponent implements OnInit {

  private timeInterval;
  public subscriptionsCount$ = this._store.select(PDMQStatusSelectors.consumerCount);
  public currentProcessingTasks$ = this._store.select(PDMQStatusSelectors.currentProcessingTasks);
  public subs: Subscription[] = [];
  public serverStatus$ = this._serverStatus.getStatus();

  constructor(
    private _store: Store,
    private _comm: CommunicatorService,
    private _router: Router,
    private _serverStatus: ServerStatusService
  ) {
    
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    if (this.timeInterval) clearInterval(this.timeInterval);
    this.subs.forEach(sub => sub.unsubscribe());
  }

  ngOnInit(): void {
  }

  viewInTimeline() {
    this._router.navigateByUrl('/dashboard/timeline')
  }

  /**
   * Stop Workflow
   * 
   * @param workflowId 
   */
  async stopWorkflow(workflowId: string) {
    const con = await this._comm.confirm("Are you sure you want to stop the execution?");
    if (!con) return;
    
    this._store.dispatch(WorkflowActions.STOP_WORKFLOW({
      workflowId,
      cb: () => {
        this._comm.showToast("Execution Stopped");
      }
    }))
  }

}
