import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { VERSION } from 'src/environments/version';
import { MENU } from '../constants/menu.const';
import { CommunicatorService } from '../services/common/communicator.service';
import { AuthenticationActions } from '../store/authentication/authentication.actions';
import { AuthenticationSelector } from '../store/authentication/authentication.selectors';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public user$ = this._store.select(AuthenticationSelector.user);
  public readonly version = VERSION;

  constructor(
    private _router: Router,
    private _store: Store,
    private _comm: CommunicatorService
  ){
  }

  ngOnInit():void {
  }

  ngOnDestroy(): void {
  }

  /**
   * Refresh Page Data
   */
  refresh() {
    this._comm.communicate("refresh");
  }

  /**
   * Logout
   */
  logout() {
    this._store.dispatch(AuthenticationActions.LOGOUT());
  }

  get menu () {
    return MENU
  }

  get pathname() {
    return this._router.url;
  }

  /**
   * Navigate pages
   * 
   * @param item 
   * @returns 
   */
  navigate(item) {
    if (!item.url) return;
    if (item.external)
      window.open(item.url, '_blank')
    else
      this._router.navigateByUrl(item.url)
  }
}
