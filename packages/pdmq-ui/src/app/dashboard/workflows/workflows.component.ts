import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { PDMQWorkflowSelectors } from 'src/app/store/pdmq-workflow/pdmq-workflow.selectors';
import { PDMQWorkflow } from 'pdmq/dist/interfaces/workflow.interface';
import { Router } from '@angular/router';
import { WorkflowActions } from 'src/app/store/pdmq-workflow/pdmq-workflow.actions';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';
import { PageSizeOptions } from 'src/app/constants/page-size-options.const';
import { PageEvent } from '@angular/material/paginator';
import { cloneDeep } from 'lodash';
import { CommunicatorService } from 'src/app/services/common/communicator.service';

@Component({
  selector: 'app-workflows',
  templateUrl: './workflows.component.html',
  styleUrls: ['./workflows.component.css']
})
export class WorkflowsComponent implements OnInit {

  public pagination: PaginationInterface = {
    index: 0,
    size: 20,
    searchKey: ''
  }
  public pageSizeOptions = PageSizeOptions
  public workflows$ = this._store.select(PDMQWorkflowSelectors.listAll);
  public count$ = this._store.select(PDMQWorkflowSelectors.count);

  constructor(
    private _store: Store,
    private _comm: CommunicatorService,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this.init();
  }

  /**
   * Fetch Workflows
   */
  init() {
    this._store.dispatch(WorkflowActions.FETCH_WORKFLOW({
      pagination: cloneDeep(this.pagination)
    }));
  }

  /**
   * Update pagination
   * 
   * @param evt 
   */
  fetch(evt: PageEvent) {
    this.pagination.size = evt.pageSize;
    this.pagination.index = evt.pageIndex;
    this.init();
  }

  /**
   * Create or edit workflow
   * 
   * @param workflow 
   */
  editWorkflow(workflow?: PDMQWorkflow) {
    if (!workflow)
      this._router.navigateByUrl('/dashboard/workflows/add')
    else
      this._router.navigateByUrl(`/dashboard/workflows/${workflow._id}/edit`)
  }

  /**
   * Run workflow
   * 
   * @param workflow 
   */
  async run(workflow: PDMQWorkflow) {
    const con = await this._comm.confirm("Are you sure you want to run this workflow?");
    if (!con) return ;

    this._store.dispatch(WorkflowActions.RUN_WORKFLOW({
      workflowId: workflow._id,
      cb: () => {
        this._comm.showToast("Workflow Started...")
      }
    }));
  }
}
