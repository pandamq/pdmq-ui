import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import Drawflow from 'drawflow'
import { PDMQWorkflow } from 'pdmq/dist/interfaces/workflow.interface'
import { WorkflowNodes } from 'src/app/constants/workflow-nodes.const';
import { cloneDeep } from 'lodash';
import { WorkflowNodeInterface } from 'src/app/interfaces/workflow-node.interface';
import { v1 } from 'uuid';
import { EditWorkflowNodeDetailComponent } from 'src/app/components/modal/edit-workflow-node-detail/edit-workflow-node-detail.component';
import { filter, map, take } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { WorkflowActions } from 'src/app/store/pdmq-workflow/pdmq-workflow.actions';
import { PDMQTaskSelectors } from 'src/app/store/pdmq-task/pdmq-task.selectors';
import { TaskTriggerTypes } from 'src/app/enums/task-trigger-types.enum';
import { EditTaskDetailComponent } from 'src/app/components/modal/edit-task-detail/edit-task-detail.component';
import { PDMQTasksActions } from 'src/app/store/pdmq-task/pdmq-task.actions';
import { ActivatedRoute, Router } from '@angular/router';
import { CommunicatorService } from 'src/app/services/common/communicator.service';
import { WorkflowNodeCategories } from 'pdmq/dist/enums/workflow-node-categories.enum';

import { WorkflowNodeTypes } from "pdmq/dist/enums/workflow-node-types.enum";
import { WorkflowExecutionStatus } from "pdmq/dist/enums/workflow-execution-status.enum"
import { WorkflowNode } from "pdmq/dist/interfaces/workflow-node.interface";
import { Location } from '@angular/common';
import { WorkflowStatusService } from 'src/app/services/sockets/workflow-status/workflow-status.service';
import { Subscription } from 'rxjs';
import { WorkflowExecutionInterface } from 'src/app/interfaces/schema/workflow-execution.interface';
import { WorkflowExecutionLogInterface } from 'src/app/interfaces/schema/workflow-execution-log.interface';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { NodeExecutionLogsComponent } from 'src/app/components/modal/node-execution-logs/node-execution-logs.component';
import * as download from "downloadjs";
import * as moment from 'moment';
import {isValidCron} from 'cron-validator';

@Component({
  selector: 'app-edit-workflow-detail',
  templateUrl: './edit-workflow-detail.component.html',
  styleUrls: ['./edit-workflow-detail.component.css']
})
export class EditWorkflowDetailComponent implements OnInit {

  public  editingItem     : PDMQWorkflow = {}
  public  subs            : Subscription[] = [];
  public  execution      ?: WorkflowExecutionInterface;
  public  executionStatus?: WorkflowExecutionStatus;
  private elementOnDrag   : WorkflowNodeInterface;
  private dragflowEditor  : Drawflow;
  public  hasChange       : boolean;
  private askForLeave     : Function = async (e) => {
    if (this.hasChange) {
      const con = confirm("Are you sure you want to leave this page?");
      if (!con) {
        e.preventDefault();
        e.returnValue = '';
      }
    }
  }

  public storedTaskCount$ = this._store.select(PDMQTaskSelectors.storedTasks).pipe(
    map(storedTasks => {
      return storedTasks.length;
    })
  )
  public storedTaskNodes$ = this._store.select(PDMQTaskSelectors.storedTasks).pipe(
    map(storedTasks => {
      return storedTasks.map(storedTask => ({
        title: "<b>TASK</b> " + storedTask.task_name,
        input: 1,
        output: 1,
        category: WorkflowNodeCategories.TASK,
        type: storedTask.task_id
      })) as WorkflowNodeInterface[]
    })
  );

  public readonly workflowNodes = WorkflowNodes;

  constructor(
    private _dialog: MatDialog,
    private _activatedRoute: ActivatedRoute,
    private _workflowStatus: WorkflowStatusService,
    private _store: Store,
    private _router: Router,
    private _location: Location,
    private _comm: CommunicatorService
  ) { }

  ngOnInit(): void {
    /** Fetch stored tasks */
    this._store.dispatch(PDMQTasksActions.FETCH_STORED_TASK());
    const workflowId = this._activatedRoute.snapshot.params.id;

    /** Connect websocket */
    workflowId && this.subs.push(
      this._workflowStatus.getStatus(workflowId).subscribe((data: {
        status     : WorkflowExecutionStatus,
        execution ?: WorkflowExecutionInterface,
        logs      ?: WorkflowExecutionLogInterface[]
      }) => {
        this.executionStatus = data.status;
        
        if (data.execution) {
          this.execution = data.execution;
        }

        if (data.logs?.length) {
          for (const log of data.logs) {
            const success = !!data.execution.workflow_execution_logs.find(l => l.log_id == log._id)?.success;
            const el = document.getElementById(`node-${log.node_id}`);
            el?.classList.remove("succeed", "failed", "running");
            log.process_completed_at && el?.classList.add(success ? 'succeed' : 'failed');
            !log.process_completed_at && el?.classList.add('running')
          }
        }
      })
    )

    /** Fetch workflow detail */
    workflowId && this._store.dispatch(WorkflowActions.FETCH_WORKFLOW_ITEM({
      id: workflowId,
      cb: (workflow) => {
        this.editingItem = cloneDeep(workflow);
        this.render(this.editingItem.workflow_body);
      }
    }))


    window.addEventListener('beforeunload', this.askForLeave as any);
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    setTimeout(() => {
      !this._activatedRoute.snapshot.params.id && this.render()
    }, 0);
  }

  /**
   * Clean up
   */
  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe());
    window.removeEventListener("beforeunload", this.askForLeave as any);
  }

  get canSave() {
    return !!this.editingItem.workflow_name;
  }

  get actionNodes() {
    return this.workflowNodes.filter(node => node.category == WorkflowNodeCategories.ACTION)
  }

  get conditionNodes() {
    return this.workflowNodes.filter(node => node.category == WorkflowNodeCategories.CONDITION)
  }

  get isNotRunning() {
    return this.executionStatus != WorkflowExecutionStatus.RUNNING;
  }

  get isRunning() {
    return !this.isNotRunning;
  }

  /**
   * Open API Key page in new tab
   */
  getApiKey() {
    window.open('/dashboard/settings/my-profile', '_blank')
  }

  /**
   * Export Workflow Configuration
   */
  export() {
    const exportObject: PDMQWorkflow = {
      workflow_name: this.editingItem.workflow_name,
      workflow_description: this.editingItem.workflow_description,
      workflow_body: this.editingItem.workflow_body,
      workflow_ignore_failure: this.editingItem.workflow_ignore_failure
    };
    download(JSON.stringify(exportObject, null, 4), `${this.editingItem.workflow_name}_export_${moment().format("MMM_DD")}.json`, "text/plain");
  }

  /**
   * Import Workflow Configuration
   */
  async import(evt: InputEvent) {
    if (!evt.target['files']?.length) return;
    const file = evt.target['files'][0] as File;
    const content = await file.text();
    try {
      const config = JSON.parse(content) as PDMQWorkflow;
      this.editingItem = {
        ...this.editingItem,
        ...config
      }
      this.render(this.editingItem.workflow_body);
      this._comm.showToast("Loaded");
    } catch (error) {
      this._comm.showErrors(error);
    }
  }

  /**
   * Add Stored Task
   */
  addStoredTask() {
    this._dialog.open(EditTaskDetailComponent, {
      panelClass: ["dialog", "dialog-md"],
      disableClose: true,
      data: {
        item: {
          task_trigger_type: TaskTriggerTypes.NEVER
        }
      }
    })
  }

  /**
   * Set Editor Mode
   * 
   * @param mode 
   */
  setEditorMode(mode: 'fixed' | 'edit') {
    this.dragflowEditor && (this.dragflowEditor.editor_mode = mode);
    if (mode == 'fixed') {
      let i = 0;
      while(i < document.getElementsByClassName("node-edit-btn").length) {
        const el = document.getElementsByClassName("node-edit-btn")[i];
        el.classList.add("d-none")
        i++;
      }
    }
    else if (mode == 'edit') {
      let i = 0;
      while(i < document.getElementsByClassName("node-edit-btn").length) {
        const el = document.getElementsByClassName("node-edit-btn")[i];
        el.classList.remove("d-none")
        i++;
      }
    }
  }

  /**
   * Lab changed
   * 
   * @param $evt 
   */
  selectedTabChange(evt: MatTabChangeEvent) {
    if (evt.tab.textLabel == 'Edit') this.setEditorMode('edit');
    else this.setEditorMode('fixed');
  }

  /**
   * Render DrawFlow Canvas
   */
  render(data?: any) {
    /** Clear Editor */
    this.dragflowEditor?.clear();

    /** Start Editor */
    const el = document.getElementById("drawflow");
    this.dragflowEditor = new Drawflow(el);
    this.dragflowEditor.start();

    /** Mark has change */
    this.dragflowEditor.on("connectionCreated", () => this.hasChange = true);
    this.dragflowEditor.on("connectionRemoved", () => this.hasChange = true);
    this.dragflowEditor.on("connectionCancel", () => this.hasChange = true);
    this.dragflowEditor.on("nodeRemoved", () => this.hasChange = true);
    this.dragflowEditor.on("nodeMoved", () => this.hasChange = true);
    this.dragflowEditor.on("nodeMoved", () => this.hasChange = true);

    setTimeout(() => {    
      this.setEditorMode('fixed');
    }, 100);

    /** Add Entry Point */
    if (!data) {
      this.dragflowEditor.addNode("ENTRY_POINT", 0, 1, 100, 100, "drawflow-card-entry-point", {}, `
        <div class="d-flex">
          <b>Entry Point</b>
          <span id="drawflow-card-entry-point" class="text-primary pointer node-edit-btn ml-auto material-icons">timer</span>
        </div>
        <div id="workflow_entry_point_cron" class="text-muted">

        </div>
      `, false)
    }

    /** Import Data */
    else {
      /** Lint Data */
      const nodeIds = Object.keys(data);
      nodeIds.forEach(id => {
        if (!data[id]?.inputs)
          data[id].inputs = {};
        if (!data[id]?.outputs)
          data[id].outputs = {};
        if (!data[id]?.data)
          data[id].data = {};
      })
      /** Start Import */
      this.dragflowEditor.import({
        drawflow: {
          Home: {
            data
          }
        }
      });
      
      nodeIds.forEach(id => {
        const node = data[id] as WorkflowNode;
        const nodeType = this.workflowNodes.find(workflowNode => {
          return workflowNode.type == node.data.type
        });
        
        this.addEditButtonListener({
          nodeName: node.name,
          nodeId  : node.id,
          nodeData: node.data,
          nodeType
        })
      });

      /** Show Cron Expression */
      if (this.editingItem.workflow_entry_point_cron && document.getElementById("workflow_entry_point_cron"))
        setTimeout(() => {
          document.getElementById("workflow_entry_point_cron").innerHTML = `
            <small>Cron Exp: ${this.editingItem.workflow_entry_point_cron}</small>
          `;
        }, 500);
    }

    /** Add Listener to Entry Point Button */
    setTimeout(() => {
      document.getElementById("drawflow-card-entry-point")?.addEventListener("click", () => {

        /** Open edit dialog */
        const ref = this._dialog.open(
          EditWorkflowNodeDetailComponent, {
            disableClose: true,
            panelClass: [
              "dialog",
              "dialog-md"
            ],
            data: {
              node: {
                fields: [{
                  value: 'workflow_entry_point_cron',
                  text: 'Cron Expression',
                  type: 'string'
                }]
              },
              data: {
                workflow_entry_point_cron: {
                  value: this.editingItem.workflow_entry_point_cron
                }
              }
            }
          }
        );

        ref.afterClosed().pipe(
          take(1),
          filter(o => o)
        ).subscribe((data) => {
          if (data.workflow_entry_point_cron.value && !isValidCron(data.workflow_entry_point_cron.value, {seconds: true})) {
            this._comm.showErrors("Please enter a valid cron expression.")
          } else {
            this.editingItem.workflow_entry_point_cron = data.workflow_entry_point_cron.value;
            document.getElementById("workflow_entry_point_cron").innerHTML = `
              <small>Cron Exp: ${data.workflow_entry_point_cron.value}</small>
            `;
          }
        })
        
      })
    }, 500);
  }

  /**
   * Drop Actions / Tasks / Conditions
   * 
   * @param evt 
   */
  drop(evt: Event) {
    evt.preventDefault()
    if (!this.elementOnDrag) return;
    this.hasChange  = true;
    const nodeType  = cloneDeep(this.elementOnDrag) as WorkflowNodeInterface;
    const category  = this.elementOnDrag.category;
    const nodeClass = "drawflow-card-" + category;
    const nodeName  = v1();
    const nodeData  = {
      type        : this.elementOnDrag.type,
      title       : this.elementOnDrag.title,
      category    : category,
      fields      : {}
    };
    const nodeId    = this.dragflowEditor.addNode(
      nodeName, 
      nodeType.input, 
      nodeType.output, 
      evt['layerX'], 
      evt['layerY'], 
      nodeClass, 
      nodeData,
      this.elementOnDrag.title + `<div style="font-size: 12px;" id="fields-${nodeName}">
        ${(nodeType.fields || []).map(field => {
          return `<span class="mr-2 ${field.required && 'text-danger'}">${field.text}: </span>`
        }).join("<br />")}
      </div>
      <hr />
      <u id="edit-${nodeName}-logs" class="text-primary pointer node-logs-btn">Logs</u>
      ${nodeType.fields?.length ? `<u id="edit-${nodeName}" class="ml-2 text-primary pointer node-edit-btn">Edit</u>` : ""}
      `, 
      false
    );
    

    /** Listen to edit event */
    this.addEditButtonListener({
      nodeName,
      nodeId,
      nodeType,
      nodeData
    })
  }

  addEditButtonListener(options: {
    nodeName  : string,
    nodeId    : number,
    nodeType ?: WorkflowNodeInterface,
    nodeData  : any
  }) {
    setTimeout(() => {
      const {
        nodeName,
        nodeId,
        nodeType,
        nodeData
      } = options;
      if (nodeType && nodeData && document.getElementById(`fields-${nodeName}`))
        document.getElementById(`fields-${nodeName}`).innerHTML = nodeData.fields && nodeType.fields?.length ? nodeType.fields.map(field => {
          return `<span class="mr-2 ${field.required && !nodeData.fields[field.value]?.value && 'text-danger'}">${field.text}: </span> <i class="text-muted">${nodeData?.fields[field.value]?.value || ""}</i>`
        }).join("<br />") : "";

      /** Append Disabled Class */
      if (nodeData.fields?.disabled?.value) {
        document.getElementById(`node-${nodeId}`)?.classList.add("disabled");
      }

      /** Add Listener for Logs Button */
      document.getElementById(`edit-${nodeName}-logs`)?.addEventListener('click', () => {
        if (!this.editingItem._id)
          return this._comm.showErrors("Please save and execute the workflow.")

        /** Open logs dialog */
        this._dialog.open(
          NodeExecutionLogsComponent, {
            panelClass: [
              "dialog",
              "dialog-lg"
            ],
            data: {
              nodeId,
              workflowId: this.editingItem._id
            }
          }
        )
      })

      /** Add Listener for Edit Button */
      nodeType && document.getElementById(`edit-${nodeName}`)?.addEventListener('click', () => {
        const item = this.dragflowEditor.getNodeFromId(nodeId);

        /** Open edit dialog */
        const ref = this._dialog.open(
          EditWorkflowNodeDetailComponent, {
            disableClose: true,
            panelClass: [
              "dialog",
              "dialog-md"
            ],
            data: {
              node: nodeType,
              data: item.data.fields
            }
          }
        )
          
        /** Subscribe and set node data */
        ref.afterClosed().pipe(
          take(1),
          filter(fields => fields)
        ).subscribe(fields => {
          /** Mark as changed */
          this.hasChange = true;

          /** Update displaying fields */
          document.getElementById(`fields-${nodeName}`).innerHTML = nodeType.fields.map(field => {
            return `<span class="mr-2">${field.text}: </span> <i class="text-muted">${fields[field.value]?.value || ""}</i>`
          }).join("<br />")
          this.dragflowEditor.updateNodeDataFromId(nodeId, {
            ...nodeData,
            fields
          });

          /** Update class if disable status toggled */
          const el = document.getElementById(`node-${nodeId}`);
          if (fields.disabled?.value) {
            el?.classList.add("disabled")
          } else {
            el?.classList.remove("disabled");
          }
        })
      })
    }, 500);
  }
  
  /**
   * Update Drop Zone CSS
   */
  allowDrop(evt: Event) {
    evt.preventDefault();
  }

  /**
   * drag Actions / Tasks / Conditions
   * 
   * @param evt 
   */
  drag(evt: WorkflowNodeInterface) {
    this.elementOnDrag = evt;
  }

  /**
   * Save Workflow
   */
  save() {
    const data = this.dragflowEditor.export().drawflow.Home.data;
    this.editingItem.workflow_body = data;
    this._store.dispatch(WorkflowActions.SAVE_WORKFLOW({
      payload: cloneDeep(this.editingItem),
      cb: (res: PDMQWorkflow) => {
        this.hasChange = false;
        this._comm.showToast("Saved");
        if (!this._activatedRoute.snapshot.params.id) {
          this._router.navigateByUrl(`/dashboard/workflows/${res._id}/edit`);
          this.editingItem = cloneDeep(res);
        }
      }
    }));
  }

  /**
   * Back
   */
  back() {
    this._location.back()
  }

  /**
   * Stop Execution
   */
  async stop() {
    const con = await this._comm.confirm("Are you sure you want to stop this execution?");
    if (!con) return ;

    this._store.dispatch(WorkflowActions.STOP_WORKFLOW({
      workflowId: this.editingItem._id,
      cb: () => {
        this._comm.showToast("Workflow Started...")
      }
    }));
  }

  /**
   * Delete workflow
   * 
   * @param workflowId 
   */
   async delete() {
    const con = await this._comm.confirm("Are you sure you want to delete this workflow?");
    if (!con) return;
    this._store.dispatch(WorkflowActions.DELETE_WORKFLOW({
      id: this.editingItem._id,
      cb: () => {
        this._comm.showToast("Deleted");
        this.back();
      }
    }))
  }

  /**
   * Run workflow
   * 
   * @param workflow 
   */
  async run() {
    const con = await this._comm.confirm("Are you sure you want to start the execution?");
    if (!con) return ;

    const nodeIds = Object.keys(this.editingItem.workflow_body);

    nodeIds.forEach(nodeId => {
      document.getElementById(`node-${nodeId}`)?.classList.remove("succeed", "failed", "running")
    })

    this._store.dispatch(WorkflowActions.RUN_WORKFLOW({
      workflowId: this.editingItem._id,
      cb: () => {
        this._comm.showToast("Workflow Started...")
      }
    }));
  }
}
