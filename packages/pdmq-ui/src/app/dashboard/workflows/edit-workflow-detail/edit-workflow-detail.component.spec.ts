import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditWorkflowDetailComponent } from './edit-workflow-detail.component';

describe('EditWorkflowDetailComponent', () => {
  let component: EditWorkflowDetailComponent;
  let fixture: ComponentFixture<EditWorkflowDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditWorkflowDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditWorkflowDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
