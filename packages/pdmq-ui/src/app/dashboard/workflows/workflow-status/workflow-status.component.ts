import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { PDMQWorkflow } from 'pdmq/dist/interfaces/workflow.interface';
import { CommunicatorService } from 'src/app/services/common/communicator.service';

@Component({
  selector: 'app-workflow-status',
  templateUrl: './workflow-status.component.html',
  styleUrls: ['./workflow-status.component.css']
})
export class WorkflowStatusComponent implements OnInit {

  @Input() workflow: PDMQWorkflow;

  constructor(
  ) { }

  ngOnInit(): void {
  }

  get workflowNodes() {
    return this.workflow?.last_execution?.workflow_execution_logs || [];
  }

}
