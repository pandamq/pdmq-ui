import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { AppMaterialModule } from '../plugins/material/material.module';
import { Routes, RouterModule } from '@angular/router';
import { StatusComponent } from './status/status.component';
import { TimelineComponent } from './timeline/timeline.component';
import { TasksComponent } from './tasks/tasks.component';
import { DirectivesModule } from '../directives/directives.module';
import { WorkflowsComponent } from './workflows/workflows.component';
import { EditWorkflowDetailComponent } from './workflows/edit-workflow-detail/edit-workflow-detail.component';
import { VariablesComponent } from './variables/variables.component';
import { WorkflowStatusComponent } from './workflows/workflow-status/workflow-status.component';
import { ServicesModule } from '../services/services.module';
import { PipesModule } from '../pipes/pipes.module';
import { ComponentsModule } from '../components/components.module';
import { CronListComponent } from './cron-list/cron-list.component';
import { ConfirmDeactivateGuard } from '../guards/confirm-deactivate.guard';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: '',
        component: StatusComponent
      },
      {
        path: 'workflows',
        component: WorkflowsComponent
      },
      {
        path: 'workflows/:id/edit',
        component: EditWorkflowDetailComponent,
        canDeactivate: [
          ConfirmDeactivateGuard
        ]
      },
      {
        path: 'workflows/add',
        component: EditWorkflowDetailComponent,
        canDeactivate: [
          ConfirmDeactivateGuard
        ]
      },
      {
        path: 'variables',
        component: VariablesComponent
      },
      {
        path: 'timeline',
        component: TimelineComponent
      },
      {
        path: 'tasks/:type',
        component: TasksComponent
      },
      {
        path: 'cron-list',
        component: CronListComponent
      },
      {
        path: 'settings',
        loadChildren: () => import('./settings/settings.module').then(m => m.SettingsModule)
      }
    ]
  }
];

@NgModule({
  declarations: [
    DashboardComponent,
    StatusComponent,
    CronListComponent,
    TimelineComponent,
    TasksComponent,
    WorkflowsComponent,
    EditWorkflowDetailComponent,
    VariablesComponent,
    WorkflowStatusComponent
  ],
  imports: [
    CommonModule,
    PipesModule,
    ComponentsModule,
    AppMaterialModule,
    DirectivesModule,
    ServicesModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    ConfirmDeactivateGuard
  ],
  exports: [
    RouterModule
  ]
})
export class DashboardModule { }
