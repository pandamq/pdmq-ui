import { TaskTriggerTypeColors } from './../../enums/task-trigger-type-colors.enum';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as Chart from 'chart.js';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';
import { EditTaskDetailComponent } from 'src/app/components/modal/edit-task-detail/edit-task-detail.component';
import { TIMELINE_VIEW_PRESET } from 'src/app/constants/timeline-view-preset.const';
import { PDMQTasksActions } from 'src/app/store/pdmq-task/pdmq-task.actions';
import { PDMQTaskSelectors } from 'src/app/store/pdmq-task/pdmq-task.selectors';
import { PDMQTask } from 'pdmq/dist/interfaces/task.interface';
import { CommunicatorService } from 'src/app/services/common/communicator.service';
import { TaskTriggerTypes as TaskTriggerTypesEnum } from 'pdmq/dist/enums/task-trigger-types.enum';
import { TaskTriggerTypes } from 'src/app/enums/task-trigger-types.enum';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements OnInit {

  private chart: Chart;
  public readonly TIMELINE_VIEW_PRESET = TIMELINE_VIEW_PRESET;
  public selectedPresetView = 0;
  private subs: Subscription[] = [];
  public timeInterval;
  public tasks$ = this._store.select(PDMQTaskSelectors.listAll).pipe(map(tasks => {
    return tasks.filter(task => {
      return task.trigger_next_at &&
      moment(task.trigger_next_at, moment.ISO_8601)
        .isBetween(
          moment(),
          moment().add(this.TIMELINE_VIEW_PRESET[this.selectedPresetView].value, 'minute'), 'second'
        );
    })
  }));

  constructor(
    private _comm: CommunicatorService,
    private _store: Store,
    private _dialog: MatDialog,
    private _actions: Actions
  ) {
    this.subs.push(
      this._actions.pipe(
        ofType(
          PDMQTasksActions.FETCH_TASK_SUCCESS,
          PDMQTasksActions.SAVE_TASK_SUCCESS,
          PDMQTasksActions.DELETE_TASK_SUCCESS,
          PDMQTasksActions.CLEAR_TASK_SUCCESS
        )
      ).subscribe(() => {
        this.renderChart();
      })
    )

    this.subs.push(
      this._comm.getCommunicate().pipe(filter(payload => payload.action === 'refresh')).subscribe(() => {
        this.init();
      })
    )
  }

  ngOnInit(): void {
    this.init();
    this.timeInterval = setInterval(() => {
      this._store.dispatch(PDMQTasksActions.FETCH_TASK({
        from: moment().add(TIMELINE_VIEW_PRESET[this.selectedPresetView].value, 'minute').toISOString(),
        to: moment().add(TIMELINE_VIEW_PRESET[this.selectedPresetView].value, 'minute').add(5, 's').toISOString()
      }))
    }, 5 * 1000)
  }


  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe());
    if (this.timeInterval) clearInterval(this.timeInterval);
  }

  addTask() {
    this._dialog.open(EditTaskDetailComponent, {
      panelClass: ["dialog", "dialog-md"],
      data: {
        item: {
          task_trigger_type: TaskTriggerTypesEnum.DAILY
        }
      }
    })
  }

  editTask(task) {
    this._dialog.open(EditTaskDetailComponent, {
      panelClass: ["dialog", "dialog-md"],
      data: {
        item: JSON.parse(JSON.stringify(task))
      }
    })
  }

  formatDate(date: string) {
    return moment(date).format("DD MMM YYYY HH:mm:ss")
  }

  init() {
    const selectedPresetView = TIMELINE_VIEW_PRESET[this.selectedPresetView];
    const from = moment();
    const to = moment().add(selectedPresetView.value, 'minutes');
    this._store.dispatch(PDMQTasksActions.FETCH_TASK({
      from: from.toISOString(), 
      to: to.toISOString(), 
      triggerType: TaskTriggerTypes.TIME
    }))
  }

  updatePresetView(index: number) {
    this.selectedPresetView = index;
    this.init();
  }

  async delete(task:PDMQTask) {
    const con = await this._comm.confirm(`Are you sure you want to delete ${task.task_name}`, "Confirmation");
    if (!con) return;

    this._store.dispatch(PDMQTasksActions.DELETE_TASK({task_id: task.task_id}));
  }

  renderChart() {
    this._store.select(PDMQTaskSelectors.listAll).pipe(take(1)).subscribe(tasks => {
      const once_dataset = {
        label: 'Non-repeat Tasks',
        borderColor: TaskTriggerTypeColors.ONCE,
        backgroundColor: 'transparent',
        data: []
      }
      const daily_dataset = {
        label: 'Daily Tasks',
        borderColor: TaskTriggerTypeColors.DAILY,
        backgroundColor: 'transparent',
        data: []
      }
      const weekly_dataset = {
        label: 'Weekly Tasks',
        borderColor: TaskTriggerTypeColors.WEEKLY,
        backgroundColor: 'transparent',
        data: []
      }
      const monthly_dataset = {
        label: 'Monthly Tasks',
        borderColor: TaskTriggerTypeColors.MONTHLY,
        backgroundColor: 'transparent',
        data: []
      }
      const annually_dataset = {
        label: 'Annually Tasks',
        borderColor: TaskTriggerTypeColors.ANNUALLY,
        backgroundColor: 'transparent',
        data: []
      }

      const selectedPresetView = TIMELINE_VIEW_PRESET[this.selectedPresetView];
      const from = moment();
      const to = moment().add(selectedPresetView.value, 'minutes');
      const data = [];
      let seconds = selectedPresetView.value * 60;
      while (seconds >= 0) {
        const m = moment().add(seconds, 'seconds');
        const currentTasks = tasks.filter(task => moment(task.trigger_next_at).isSame(m, 's'));
        once_dataset.data.push({
          x: m,
          y: currentTasks.filter(task => task.task_trigger_type == 'once').length
        })
        daily_dataset.data.push({
          x: m,
          y: currentTasks.filter(task => task.task_trigger_type == 'daily').length
        })
        weekly_dataset.data.push({
          x: m,
          y: currentTasks.filter(task => task.task_trigger_type == 'weekly').length
        })
        monthly_dataset.data.push({
          x: m,
          y: currentTasks.filter(task => task.task_trigger_type == 'monthly').length
        })
        annually_dataset.data.push({
          x: m,
          y: currentTasks.filter(task => task.task_trigger_type == 'annually').length
        })

        seconds--;
      };
      data.reverse();


      const canvas: any = document.getElementById("timeline");
      const ctx = canvas.getContext('2d');
      if (this.chart) this.chart.destroy();
      this.chart = new Chart(ctx, {
        type: 'line',
        data: {
          datasets: [
            once_dataset,
            daily_dataset,
            weekly_dataset,
            monthly_dataset,
            annually_dataset
          ]
        },
        options: {
          animation: {
            duration: 0
          },
          scales: {
            yAxes: [{
              ticks: {
                callback: function(label, index, labels) {
                  if (Number(label) % 1 == 0) return label;
                  return '';
              },
                beginAtZero: true
              }
            }],
            xAxes: [{
              ticks: {
                min: from,
                max: to,
                autoSkip: true
              },
              type: 'time',
              time: {
                unitStepSize: 5,
                unit: 'second'
              }
            }]
          }
        }
      });

    })
  }

}
