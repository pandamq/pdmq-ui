import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InitialConnectionComponent } from './initial-connection.component';

describe('InitialConnectionComponent', () => {
  let component: InitialConnectionComponent;
  let fixture: ComponentFixture<InitialConnectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InitialConnectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InitialConnectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
