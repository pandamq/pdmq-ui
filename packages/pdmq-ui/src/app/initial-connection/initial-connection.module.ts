import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InitialConnectionComponent } from './initial-connection.component';
import { RouterModule, Routes } from '@angular/router';
import { AppMaterialModule } from '../plugins/material/material.module';

const routes: Routes = [
  {
    path: '',
    component: InitialConnectionComponent
  }
];


@NgModule({
  declarations: [
    InitialConnectionComponent
  ],
  imports: [
    CommonModule,
    AppMaterialModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class InitialConnectionModule { }
