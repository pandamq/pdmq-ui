import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthenticationActions } from '../store/authentication/authentication.actions';


@Component({
  selector: 'app-initial-connection',
  templateUrl: './initial-connection.component.html',
  styleUrls: ['./initial-connection.component.scss']
})
export class InitialConnectionComponent implements OnInit {

  public editingItem = {
    email: "",
    password: ""
  }

  constructor(
    private _store: Store
  ) {
  }

  ngOnInit(): void {
  }

  get canSubmit() {
    return !!this.editingItem.email && !!this.editingItem.password;
  }

  /**
   * Login
   */
  submit() {
    this._store.dispatch(AuthenticationActions.LOGIN({
      email: this.editingItem.email,
      password: this.editingItem.password
    }))
  }
}
