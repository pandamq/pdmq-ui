import { NgModule } from '@angular/core';
import { MatMomentDateModule } from "@angular/material-moment-adapter";
import {MatSortModule} from '@angular/material/sort';
import { MatRadioModule } from '@angular/material/radio';
import {MatTableModule} from '@angular/material/table';
import {MatRippleModule, MAT_DATE_FORMATS} from '@angular/material/core';
import { MatBadgeModule } from '@angular/material/badge';
import {MatPaginatorIntl, MatPaginatorModule} from '@angular/material/paginator';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import {MatTabsModule} from '@angular/material/tabs';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatChipsModule} from '@angular/material/chips';
import {MatSelectModule} from '@angular/material/select';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatMenuModule} from '@angular/material/menu';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatDialogModule} from '@angular/material/dialog';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {MatStepperModule} from '@angular/material/stepper';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMatDateAdapter, NgxMatDatetimePickerModule, NGX_MAT_DATE_FORMATS } from '@angular-material-components/datetime-picker';
import { NgxMatMomentModule, NGX_MAT_MOMENT_FORMATS } from '@angular-material-components/moment-adapter';
import { AceModule, ACE_CONFIG } from 'ngx-ace-wrapper';

const pagination = () => {
  const options = new MatPaginatorIntl();
  options.itemsPerPageLabel = 'Rows';
  options.getRangeLabel = (page: number, pageSize: number, length: number): string => {
    if (length === 0 || pageSize === 0) {
      return `0 / ${length}`;
    }

    length = Math.max(length, 0);

    const startIndex: number = page * pageSize;
    const endIndex: number = startIndex < length
      ? Math.min(startIndex + pageSize, length)
      : startIndex + pageSize;

    return `Displaying ${startIndex + 1}-${endIndex} (of ${length} rows)`;
  };

  return options
}

const modules = [
  AceModule,
  MatIconModule,
  MatCardModule,
  MatListModule,
  MatTabsModule,
  MatMenuModule,
  MatRadioModule,
  MatBadgeModule,
  MatInputModule,
  MatChipsModule,
  MatDialogModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatRippleModule,
  MatButtonModule,
  MatDividerModule,
  MatTooltipModule,
  MatToolbarModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatCheckboxModule,
  MatPaginatorModule,
  MatFormFieldModule,
  MatExpansionModule,
  MatDatepickerModule,
  MatMomentDateModule,
  MatProgressBarModule,
  MatSlideToggleModule,
  MatAutocompleteModule,
  MatProgressSpinnerModule,
  DragDropModule,
  ScrollingModule,
  MatStepperModule,
  FormsModule,
  ReactiveFormsModule,
  NgxMatDatetimePickerModule,
  NgxMatMomentModule
];

// configures NgModule imports and exports
@NgModule({
  imports: [
    ...modules
  ],
  providers: [
    {
      provide: ACE_CONFIG,
      useValue: {
        
      }
    },

    {
      provide: MatPaginatorIntl,
      useValue: pagination()
    },
    {
      provide: NGX_MAT_DATE_FORMATS,
      useValue: {
        display: {
          dateInput: 'YYYY-MM-DD HH:mm:ss'
        },
      },
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        display: {
          dateInput: 'DD MMM YYYY',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
      },
    },
  ],
  exports: [
    ...modules
  ]
})
export class AppMaterialModule {}
