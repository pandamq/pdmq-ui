export interface PaginationInterface {
  index?: number

  size?: number

  searchKey?: string

  startAt?: string

  endAt?: string

  sortBy?: string

  direction?: string

  from?: string
  
  to?: string
}