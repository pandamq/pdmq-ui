export interface FetchResponseInterface<T> {
  results: T[],
  count: number
}