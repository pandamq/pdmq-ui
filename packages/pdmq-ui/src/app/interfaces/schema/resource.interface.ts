import { BaseInterface } from "./base.interface";

export interface ResourceInterface extends BaseInterface {
  has_delete    ?: boolean,
  has_edit      ?: boolean,
  has_create    ?: boolean,
  has_view      ?: boolean,
  resource_name ?: string,
  can_edit      ?: boolean,
  can_view      ?: boolean,
  can_create    ?: boolean,
  can_delete    ?: boolean
}