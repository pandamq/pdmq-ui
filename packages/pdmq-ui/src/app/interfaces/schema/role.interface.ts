import { BaseInterface } from "./base.interface";

export interface RoleInterface extends BaseInterface {
  resources?: {
    resource: string
    can_view: boolean
    can_edit: boolean
    can_create: boolean
    can_delete: boolean
  }[],
  role_icon?: string,
  role_name?: string,
}