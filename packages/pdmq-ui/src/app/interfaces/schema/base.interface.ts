export interface BaseInterface {
  _id?: string,
  created_at?: string,
  created_by?: string,
  updated_at?: string,
  updated_by?: string,
  is_system?: boolean,
  is_active?: boolean
}