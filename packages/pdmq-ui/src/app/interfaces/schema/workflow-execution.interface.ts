import { BaseInterface } from "./base.interface";
import { PDMQWorkflow } from "pdmq/dist/interfaces/workflow.interface";
import { WorkflowExecutionLogInterface } from "./workflow-execution-log.interface";

export interface WorkflowExecutionInterface extends BaseInterface {

  workflow?: string | PDMQWorkflow
  
  workflow_execution_completed_at?: Date
  
  workflow_execution_started_at?: Date

  workflow_execution_logs?: {
    log_id?: string | WorkflowExecutionLogInterface,
    success?: boolean
  }[]
}