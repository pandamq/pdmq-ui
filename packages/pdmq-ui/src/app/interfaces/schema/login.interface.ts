import { BaseInterface } from "./base.interface";

export interface LoginInterface extends BaseInterface {
  api_restricted        ?: boolean,
  dashboard_restricted  ?: boolean,
  roles                 ?: any[],
  email                 ?: string,
  resources             ?: {
    resource: string,
    can_view: boolean,
    can_edit: boolean,
    can_delete: boolean,
    can_create: boolean
  }[]
}