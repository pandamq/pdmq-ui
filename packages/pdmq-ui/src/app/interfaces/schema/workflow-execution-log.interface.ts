import { BaseInterface } from "./base.interface";

export interface WorkflowExecutionLogInterface extends BaseInterface {
  process_completed_at?: Date

  process_started_at?: string

  workflow_execution?: string

  node_execution_result?: any

  node_execution_context?: any

  node_id?: number

  node_type?: string
}