import { BaseInterface } from "./base.interface";

export interface SystemSettingInterface extends BaseInterface {
  system_setting_name?: string,

  system_setting_data?: any
}