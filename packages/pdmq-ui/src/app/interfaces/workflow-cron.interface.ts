import { PDMQWorkflow } from 'pdmq/dist/interfaces/workflow.interface';

export interface WorkflowCronInterface {
  workflow: PDMQWorkflow,
  nextRunTime: string,
  isRunning: boolean
}