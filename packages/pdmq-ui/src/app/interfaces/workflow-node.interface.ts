import { WorkflowNodeCategories } from "pdmq/dist/enums/workflow-node-categories.enum";
import { WorkflowNodeTypes } from "pdmq/dist/enums/workflow-node-types.enum";

export interface WorkflowNodeInterface {
  title      : string;
  input      : number;
  output     : number;
  type       : WorkflowNodeTypes;
  category   : WorkflowNodeCategories;
  fields    ?: {
    displaySystemVariableExamples?: boolean,
    hint    ?: string;
    value   ?: string;
    text    ?: string;
    type    ?: string;
    multiple?: boolean,
    required?: boolean,
    options ?: {
      text    : string,
      value   : string
    }[];
  }[];
}