import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { RoleInterface } from 'src/app/interfaces/schema/role.interface';
import { CommunicatorService } from 'src/app/services/common/communicator.service';
import { cloneDeep } from 'lodash';
import { RoleActions } from 'src/app/store/role/role.actions';
import { ResourceSelectors } from 'src/app/store/resource/resource.selectors';

@Component({
  selector: 'app-edit-role-detail',
  templateUrl: './edit-role-detail.component.html',
  styleUrls: ['./edit-role-detail.component.css']
})
export class EditRoleDetailComponent implements OnInit {

  public editingItem: RoleInterface = {};

  public resources$ = this._store.select(ResourceSelectors.listAll);

  constructor(
    @Inject(MAT_DIALOG_DATA)
    private _data: { item: RoleInterface },
    private _dialogRef: MatDialogRef<EditRoleDetailComponent>,
    private _store: Store,
    private _comm: CommunicatorService
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    setTimeout(() => {
      if (this._data.item) this.editingItem = cloneDeep(this._data.item);
    }, 0);
  }

  /**
   * Dismiss
   */
  dismiss() {
    this._dialogRef.close();
  }

  get canSave() {
    return !!this.editingItem.role_name
  }

  /**
   * Toggle resource
   * 
   * @param resourceId 
   * @param type 
   */
  togglePermission(resourceId, type) {
    if (!this.editingItem.resources)
      this.editingItem.resources = [];

    const index = this.editingItem.resources.findIndex(resource => resource.resource == resourceId);
    if (index == -1) {
      this.editingItem.resources.push({
        resource: resourceId,
        can_create: false,
        can_delete: false,
        can_edit  : false,
        can_view  : true,
        ...{
          [type]  : true
        }
      })
    }
    else {
      if (type == 'can_view' && this.editingItem.resources[index][type])
        this.editingItem.resources.splice(index, 1);
      else
        this.editingItem.resources[index][type] = !this.editingItem.resources[index][type]
    }
  }

  /**
   * Is Resource Permitted
   */
  isPermitted(resourceId, type) {
    return this.editingItem.resources?.some(resource => resource.resource == resourceId && resource[type])
  }

  /**
   * Save
   */
  save() {
    this._store.dispatch(RoleActions.SAVE({
      payload: this.editingItem,
      cb: () => {
        this._comm.showToast("Saved");
        this.dismiss();
      }
    }))
  }

}
