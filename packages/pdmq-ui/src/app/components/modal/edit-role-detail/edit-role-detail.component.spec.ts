import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRoleDetailComponent } from './edit-role-detail.component';

describe('EditRoleDetailComponent', () => {
  let component: EditRoleDetailComponent;
  let fixture: ComponentFixture<EditRoleDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditRoleDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRoleDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
