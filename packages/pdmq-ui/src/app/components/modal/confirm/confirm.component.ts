import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnInit {

  constructor(
    private _dialogRef: MatDialogRef<ConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: {message: string, confirmText?: string, cancelText?: string, title?: string}
  ) { }

  ngOnInit(): void {
  }

  get title() {
    return this._data.title;
  }

  get message() {
    return this._data.message
  }

  get confirmText() {
    return this._data.confirmText || "Confirm";
  }

  get cancelText() {
    return this._data.cancelText || "Cancel";
  }

  dismiss() {
    return this._dialogRef.close()
  }

  confirm() {
    return this._dialogRef.close(true);
  }

}
