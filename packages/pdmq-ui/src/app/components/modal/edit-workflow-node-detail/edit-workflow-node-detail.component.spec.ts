import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditWorkflowNodeDetailComponent } from './edit-workflow-node-detail.component';

describe('EditWorkflowNodeDetailComponent', () => {
  let component: EditWorkflowNodeDetailComponent;
  let fixture: ComponentFixture<EditWorkflowNodeDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditWorkflowNodeDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditWorkflowNodeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
