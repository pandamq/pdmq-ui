import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { WorkflowNodeInterface } from 'src/app/interfaces/workflow-node.interface';
import { cloneDeep } from 'lodash';

import 'brace/mode/json';
import 'brace/theme/github';
import { SystemVariablesComponent } from '../system-variables/system-variables.component';

@Component({
  selector: 'app-edit-workflow-node-detail',
  templateUrl: './edit-workflow-node-detail.component.html',
  styleUrls: ['./edit-workflow-node-detail.component.css']
})
export class EditWorkflowNodeDetailComponent implements OnInit {

  public editingItem: any = {};

  constructor(
    @Inject(MAT_DIALOG_DATA)
    private _data: {
      node: WorkflowNodeInterface,
      data: any
    },
    private _dialog: MatDialog,
    private _dialogRef: MatDialogRef<EditWorkflowNodeDetailComponent>
  ) {
    
  }

  ngOnInit(): void {
    if (this._data.data) this.editingItem = cloneDeep(this._data.data);
    this.fields.forEach(field => {
      if (!this.editingItem[field.value]) {
        this.editingItem[field.value] = {
          value: '',
          type: field.type,
          key: field.value,
          title: field.text
        }
      }
    })
  }

  ngAfterViewInit(): void {
    
  }

  /**
   * Show Variable Dialog
   */
  showVariableExample() {
    this._dialog.open(SystemVariablesComponent, {
      panelClass: ["dialog", "dialog-md"]
    })
  }

  get fields() {
    return this._data.node.fields || [];
  }

  get canSave() {
    return true
  }

  /**
   * Save Data
   */
  save() {
    this._dialogRef.close(this.editingItem);
  }

  /**
   * Close Dialog
   */
  dismiss() {
    this._dialogRef.close();
  }
}
