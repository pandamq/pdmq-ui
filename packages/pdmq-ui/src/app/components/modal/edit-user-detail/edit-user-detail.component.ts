import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { LoginInterface } from 'src/app/interfaces/schema/login.interface';
import { CommunicatorService } from 'src/app/services/common/communicator.service';
import { UserActions } from 'src/app/store/user/user.actions';
import { cloneDeep } from 'lodash';
import { RoleSelectors } from 'src/app/store/role/role.selectors';

@Component({
  selector: 'app-edit-user-detail',
  templateUrl: './edit-user-detail.component.html',
  styleUrls: ['./edit-user-detail.component.css']
})
export class EditUserDetailComponent implements OnInit {

  public editingItem: LoginInterface = {};
  public roles$ = this._store.select(RoleSelectors.listAll);

  constructor(
    @Inject(MAT_DIALOG_DATA)
    private _data: { item: LoginInterface },
    private _dialogRef: MatDialogRef<EditUserDetailComponent>,
    private _store: Store,
    private _comm: CommunicatorService
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    setTimeout(() => {
      if (this._data.item) this.editingItem = cloneDeep(this._data.item);
    }, 0);
  }

  /**
   * Dismiss
   */
  dismiss() {
    this._dialogRef.close();
  }

  get canSave() {
    return !!this.editingItem.email
  }

  /**
   * Save
   */
  save() {
    this._store.dispatch(UserActions.SAVE({
      payload: this.editingItem,
      cb: () => {
        this._comm.showToast("Saved");
        this.dismiss();
      }
    }))
  }

}
