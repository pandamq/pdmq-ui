import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './loader/loader.component';
import { AppMaterialModule } from 'src/app/plugins/material/material.module';
import { EditTaskDetailComponent } from './edit-task-detail/edit-task-detail.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { EditWorkflowNodeDetailComponent } from './edit-workflow-node-detail/edit-workflow-node-detail.component';
import { EditVariableDetailComponent } from './edit-variable-detail/edit-variable-detail.component';
import { NodeExecutionLogsComponent } from './node-execution-logs/node-execution-logs.component';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { EditUserDetailComponent } from './edit-user-detail/edit-user-detail.component';
import { EditRoleDetailComponent } from './edit-role-detail/edit-role-detail.component';
import { EditResourceDetailComponent } from './edit-resource-detail/edit-resource-detail.component';
import { ChangePasswordDialogComponent } from './change-password/change-password-dialog.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { AppFormModule } from '../form/form.module';
import { SystemVariablesComponent } from './system-variables/system-variables.component';

@NgModule({
  declarations: [
    LoaderComponent,
    EditTaskDetailComponent,
    ConfirmComponent,
    EditWorkflowNodeDetailComponent,
    EditVariableDetailComponent,
    NodeExecutionLogsComponent,
    EditUserDetailComponent,
    EditRoleDetailComponent,
    EditResourceDetailComponent,
    ChangePasswordDialogComponent,
    ForgotPasswordComponent,
    SystemVariablesComponent
  ],
  imports: [
    AppFormModule,
    PipesModule,
    AppMaterialModule,
    CommonModule
  ]
})
export class ModalModule { }
