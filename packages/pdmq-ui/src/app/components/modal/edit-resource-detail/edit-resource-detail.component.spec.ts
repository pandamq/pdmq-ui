import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditResourceDetailComponent } from './edit-resource-detail.component';

describe('EditResourceDetailComponent', () => {
  let component: EditResourceDetailComponent;
  let fixture: ComponentFixture<EditResourceDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditResourceDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditResourceDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
