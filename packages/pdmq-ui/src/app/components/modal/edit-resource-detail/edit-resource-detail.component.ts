import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ResourceInterface } from 'src/app/interfaces/schema/resource.interface';
import { cloneDeep } from 'lodash';
import { Store } from '@ngrx/store';
import { ResourceActions } from 'src/app/store/resource/resource.actions';
import { CommunicatorService } from 'src/app/services/common/communicator.service';

@Component({
  selector: 'app-edit-resource-detail',
  templateUrl: './edit-resource-detail.component.html',
  styleUrls: ['./edit-resource-detail.component.css']
})
export class EditResourceDetailComponent implements OnInit {

  public editingItem: ResourceInterface = {};

  constructor(
    @Inject(MAT_DIALOG_DATA)
    private _data: { item: ResourceInterface },
    private _dialogRef: MatDialogRef<EditResourceDetailComponent>,
    private _store: Store,
    private _comm: CommunicatorService
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    setTimeout(() => {
      if (this._data.item) this.editingItem = cloneDeep(this._data.item);
    }, 0);
  }

  /**
   * Dismiss
   */
  dismiss() {
    this._dialogRef.close();
  }

  get canSave() {
    return !!this.editingItem.resource_name
  }

  /**
   * Save
   */
  save() {
    this._store.dispatch(ResourceActions.SAVE({
      payload: this.editingItem,
      cb: () => {
        this._comm.showToast("Saved");
        this.dismiss();
      }
    }))
  }

}
