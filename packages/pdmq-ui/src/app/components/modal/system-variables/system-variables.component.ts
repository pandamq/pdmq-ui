import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { CommunicatorService } from 'src/app/services/common/communicator.service';
import { PDMQVariableActions } from 'src/app/store/pdmq-variable/pdmq-variable.actions';
import { PDMQVariableSelectors } from 'src/app/store/pdmq-variable/pdmq-variable.selectors';

@Component({
  selector: 'app-system-variables',
  templateUrl: './system-variables.component.html',
  styleUrls: ['./system-variables.component.css']
})
export class SystemVariablesComponent implements OnInit {

  public examples$ = this._store.select(PDMQVariableSelectors.examples);

  constructor(
    private _store: Store,
    private _comm: CommunicatorService,
    private _dialogRef: MatDialogRef<SystemVariablesComponent>
  ) { }

  ngOnInit(): void {
    this._store.dispatch(PDMQVariableActions.FETCH_VARIABLES_EXAMPLE())
  }

  /**
   * Close Dialog
   * 
   * @returns {void} 
   */
  dismiss() {
    return this._dialogRef.close()
  }

  /**
   * Format variable name
   * 
   * @param text 
   * @returns {string}
   */
  formatVariableName(text: string) {
    return `{{${text}}}`
  }

  /**
   * Copy String
   * 
   * @param str 
   */
  copy(str: string) {
    this._comm.copy(str);
  }

}
