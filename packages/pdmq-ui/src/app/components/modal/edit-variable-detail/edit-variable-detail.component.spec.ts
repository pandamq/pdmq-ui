import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditVariableDetailComponent } from './edit-variable-detail.component';

describe('EditVariableDetailComponent', () => {
  let component: EditVariableDetailComponent;
  let fixture: ComponentFixture<EditVariableDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditVariableDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditVariableDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
