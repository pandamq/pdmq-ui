import 'brace/mode/json';
import 'brace/theme/github';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PDMQVariable } from 'pdmq/dist/interfaces/variable.interface';
import { FieldTypeOptions } from 'pdmq/dist/constants/field-type-options.const';
import { Store } from '@ngrx/store';
import { CommunicatorService } from 'src/app/services/common/communicator.service';
import { PDMQVariableActions } from 'src/app/store/pdmq-variable/pdmq-variable.actions';
import { cloneDeep } from 'lodash';

@Component({
  selector: 'app-edit-variable-detail',
  templateUrl: './edit-variable-detail.component.html',
  styleUrls: ['./edit-variable-detail.component.css']
})
export class EditVariableDetailComponent implements OnInit {

  public editingItem: PDMQVariable = {};
  public readonly fieldTypes = FieldTypeOptions

  constructor(
    @Inject(MAT_DIALOG_DATA)
    private _data: {item: PDMQVariable},
    private _dialogRef: MatDialogRef<EditVariableDetailComponent>,
    private _store: Store,
    private _comm: CommunicatorService,
  ) { }

  ngOnInit(): void {
    if (this._data.item?._id)
      this.editingItem = cloneDeep(this._data.item);
  }

  get canSave() {
    return this.editingItem.variable_name;
  }

  /**
   * Save Variable
   */
  save() {
    this._store.dispatch(PDMQVariableActions.SAVE_VARIABLE({
      payload: this.editingItem,
      cb: () => {
        this._comm.showToast("Saved");
        this.dismiss();
      }
    }))
  }

  dismiss() {
    this._dialogRef.close();
  }
}
