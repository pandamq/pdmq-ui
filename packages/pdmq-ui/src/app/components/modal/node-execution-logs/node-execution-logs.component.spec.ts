import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeExecutionLogsComponent } from './node-execution-logs.component';

describe('NodeExecutionLogsComponent', () => {
  let component: NodeExecutionLogsComponent;
  let fixture: ComponentFixture<NodeExecutionLogsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NodeExecutionLogsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeExecutionLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
