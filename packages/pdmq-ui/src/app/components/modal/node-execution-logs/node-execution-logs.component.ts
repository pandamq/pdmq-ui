import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { PageSizeOptions } from 'src/app/constants/page-size-options.const';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';
import { PDMQWorkflowExecutionLogActions, PDMQWorkflowExecutionLogActionTypes } from 'src/app/store/pdmq-workflow-execution-log/pdmq-workflow-execution-log.actions';
import { PDMQWorkflowExecutionLogSelectors } from 'src/app/store/pdmq-workflow-execution-log/pdmq-workflow-execution-log.selectors';
import { cloneDeep } from 'lodash';
import { PageEvent } from '@angular/material/paginator';
import { CommunicatorService } from 'src/app/services/common/communicator.service';

@Component({
  selector: 'app-node-execution-logs',
  templateUrl: './node-execution-logs.component.html',
  styleUrls: ['./node-execution-logs.component.css']
})
export class NodeExecutionLogsComponent implements OnInit {

  public logs$  = this._store.select(PDMQWorkflowExecutionLogSelectors.listAll);
  public count$ = this._store.select(PDMQWorkflowExecutionLogSelectors.count);

  public readonly pageSizeOptions = PageSizeOptions;
  public pagination: PaginationInterface = {
    index: 0,
    size: 20
  }

  constructor(
    @Inject(MAT_DIALOG_DATA)
    private _data: {nodeId: number, workflowId: string},
    private _store: Store,
    private _comm: CommunicatorService,
    private _dialogRef: MatDialogRef<NodeExecutionLogsComponent>
  ) { }

  ngOnInit(): void {
    this.init();
  }

  /**
   * Fetch logs
   */
  init() {
    this._store.dispatch(PDMQWorkflowExecutionLogActions.FETCH_WORKFLOW_EXECUTION_LOG({
      workflowId: this._data.workflowId,
      nodeId: this._data.nodeId,
      pagination: cloneDeep(this.pagination)
    }))
  }

  /**
   * Copy Data
   * 
   * @param results 
   */
  copy(results?: any) {
    if (results && typeof results == 'object') {
      this._comm.copy(JSON.stringify(results, null, 2));
    }
  }

  /**
   * Update pagiantion
   */
  fetch(evt: PageEvent) {
    this.pagination.index = evt.pageIndex;
    this.pagination.size = evt.pageSize;
    this.init();
  }

  /**
   * Close Dialog
   */
  dismiss() {
    this._dialogRef.close();
  }
}
