import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LoginInterface } from 'src/app/interfaces/schema/login.interface';

@Component({
  selector: 'app-change-password-dialog',
  templateUrl: './change-password-dialog.component.html',
  styleUrls: ['./change-password-dialog.component.css']
})
export class ChangePasswordDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA)
    private _data: { item: LoginInterface },
    private _dialogRef: MatDialogRef<ChangePasswordDialogComponent>
  ) { }

  ngOnInit(): void {
  }

  get login() {
    return this._data.item;
  }

  dismiss() {
    this._dialogRef.close()
  }

  completed() {
    this._dialogRef.close();
  }
}
