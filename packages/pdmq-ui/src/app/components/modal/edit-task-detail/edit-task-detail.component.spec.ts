import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTaskDetailComponent } from './edit-task-detail.component';

describe('EditTaskDetailComponent', () => {
  let component: EditTaskDetailComponent;
  let fixture: ComponentFixture<EditTaskDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditTaskDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTaskDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
