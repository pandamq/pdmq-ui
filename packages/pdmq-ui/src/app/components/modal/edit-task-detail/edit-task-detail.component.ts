import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { Store } from '@ngrx/store';
import { PDMQTasksActions } from 'src/app/store/pdmq-task/pdmq-task.actions';
import { Subscription } from 'rxjs';
import { Actions, ofType } from '@ngrx/effects';
import { CommunicatorService } from 'src/app/services/common/communicator.service';
import { PDMQTask } from 'pdmq/dist/interfaces/task.interface';
import { PDMQTaskSelectors } from 'src/app/store/pdmq-task/pdmq-task.selectors';
import * as ace from 'brace';
import 'brace/mode/json';
import 'brace/mode/plain_text';
import 'brace/theme/monokai';
import { PDMQStatusSelectors } from 'src/app/store/status/pdmq-status.selectors';
import { StatusActions } from 'src/app/store/status/pdmq-status.actions';
import { TaskTriggerTypes } from 'pdmq/dist/enums/task-trigger-types.enum';
import { cloneDeep } from 'lodash';
import { WorkflowActions } from 'src/app/store/pdmq-workflow/pdmq-workflow.actions';
import { PDMQWorkflowSelectors } from 'src/app/store/pdmq-workflow/pdmq-workflow.selectors';

@Component({
  selector: 'app-edit-task-detail',
  templateUrl: './edit-task-detail.component.html',
  styleUrls: ['./edit-task-detail.component.css']
})
export class EditTaskDetailComponent implements OnInit {

  public readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  public editingItem: PDMQTask = {
    task_name: "",
    task_trigger_type: TaskTriggerTypes.INSTANT
  };
  public isJSON = true;
  private editor: ace.Editor;
  private subs: Subscription[] = [];
  public storedTasks$ = this._store.select(PDMQTaskSelectors.storedTasks);
  public consumers$ = this._store.select(PDMQStatusSelectors.consumers);

  constructor(
    private _dialogRef: MatDialogRef<EditTaskDetailComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: {item?: PDMQTask},
    private _store: Store,
    private _comm: CommunicatorService,
    private _actions: Actions
  ) {
    this._store.dispatch(StatusActions.FETCH_CONSUMERS());

    this.subs.push(
      this._actions.pipe(ofType(
        PDMQTasksActions.SAVE_TASK_SUCCESS,
      ))?.subscribe(() => {
        this.dismiss();
        this._comm.showToast("Saved");
      })
    )
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subs.forEach(sub => sub.unsubscribe());
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    setTimeout(() => {
      this._store.dispatch(PDMQTasksActions.FETCH_STORED_TASK());
      if (this._data.item) this.editingItem = JSON.parse(JSON.stringify(this._data.item));

      this.editor = ace.edit('json-editor');
      this.editor.setTheme('ace/theme/monokai');
      this.editor.setValue(this.editingItem.task_message || "");
      try {
        JSON.parse(this.editingItem.task_message);
      } catch (error) {
        this.isJSON = false;
      } finally {
        this.toggleJsonValidator();
      }
    }, 0);
  }

  addYear(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if (!this.editingItem.task_trigger_years) this.editingItem.task_trigger_years = [];
    if ((value || '').trim()) this.editingItem.task_trigger_years.push(value.trim());
    if (input) input.value = '';
  }

  toggleJsonValidator() {
    if (this.isJSON) {
      this.editor.getSession().setMode('ace/mode/json');
    } else {
      this.editor.getSession().setMode('ace/mode/plain_text')
    }
  }

  save() {
    this.editingItem.task_message = this.editor.getValue();
    this._store.dispatch(PDMQTasksActions.SAVE_TASK({
      task: this.editingItem
    }));
  }

  get canSave() {
    return !!this.editingItem.task_name && !!this.editingItem.task_trigger_type;
  }

  dismiss() {
    this._dialogRef.close();
  }
}
