import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListVariablesComponent } from './list-variables.component';

describe('ListVariablesComponent', () => {
  let component: ListVariablesComponent;
  let fixture: ComponentFixture<ListVariablesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListVariablesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListVariablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
