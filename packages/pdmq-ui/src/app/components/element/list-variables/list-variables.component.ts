import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { PDMQVariableActions } from 'src/app/store/pdmq-variable/pdmq-variable.actions';

@Component({
  selector: 'app-list-variables',
  templateUrl: './list-variables.component.html',
  styleUrls: ['./list-variables.component.css']
})
export class ListVariablesComponent implements OnInit {

  constructor(
    private _store: Store
  ) { }

  ngOnInit(): void {
    this._store.dispatch(PDMQVariableActions.FETCH_NAMES())
  }

}
