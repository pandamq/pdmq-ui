import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { LoginInterface } from 'src/app/interfaces/schema/login.interface';
import { UserActions } from 'src/app/store/user/user.actions';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  @Input() isDialog: boolean        = false;
  @Input() login   : LoginInterface = {}

  @Output() dismiss: EventEmitter<void> = new EventEmitter();
  @Output() completed: EventEmitter<void> = new EventEmitter();

  public editingItem = {
    password: "",
    confirmPassword: ""
  }

  constructor(
    private _store: Store
  ) { }

  get canConfirm() {
    return this.editingItem.confirmPassword == this.editingItem.password && this.editingItem.password;
  }

  ngOnInit(): void {
  }

  /**
   * update password
   */
  confirm() {
    this._store.dispatch(UserActions.CHANGE_PASSWORD({
      password: this.editingItem.password
    }))
  }

  /**
   * cancel process
   */
  cancel() {
    this.dismiss.emit();
  }
}
