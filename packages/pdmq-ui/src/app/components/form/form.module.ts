import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AppMaterialModule } from 'src/app/plugins/material/material.module';

@NgModule({
  declarations: [
    ChangePasswordComponent,
  ],
  imports: [
    CommonModule,
    AppMaterialModule
  ],
  exports: [
    ChangePasswordComponent
  ]
})
export class AppFormModule { }
