import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from './modal/modal.module';
import { AppMaterialModule } from '../plugins/material/material.module';
import { PipesModule } from '../pipes/pipes.module';
import { AppFormModule } from './form/form.module';
import { ListVariablesComponent } from './element/list-variables/list-variables.component';

@NgModule({
  declarations: [ListVariablesComponent],
  imports: [
    PipesModule,
    CommonModule,
    ModalModule,
    AppFormModule,
    AppMaterialModule
  ],
  exports: [
    AppFormModule,
    ModalModule
  ]
})
export class ComponentsModule { }
