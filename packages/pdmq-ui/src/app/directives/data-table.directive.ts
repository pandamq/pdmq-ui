import { Directive, ElementRef, Input } from '@angular/core';
import * as $ from "jquery";
import * as shortid from 'shortid';

@Directive({
  selector: '[appDataTable]'
})
export class DataTableDirective {

  @Input() loading = false;
  @Input() size: string;

  private tableId = shortid();

  constructor(
    private el: ElementRef<HTMLTableElement>
  ) {

  }

  get footerClass() {
  return `appTableFooter-${this.tableId}`;
  }

  ngAfterViewInit(): void {
    $(this.el.nativeElement).addClass("table w-100 border");
    $(this.el.nativeElement).find('th').addClass("bg-primary text-white");
  }

  ngAfterContentChecked(): void {
    const tbody = this.el.nativeElement.getElementsByTagName("tbody");
    const footers = this.el.nativeElement.getElementsByClassName(this.footerClass);

    if (tbody.length && !this.loading) {
      if (footers.length) {
        $(this.el.nativeElement).find(`.${this.footerClass}`).remove();
      }
      $(this.el.nativeElement).find('tbody').show();
    }
    else if (!footers.length) {
      const footer = document.createElement("tfoot");
      footer.classList.add(this.footerClass);
      this.el.nativeElement.appendChild(footer);
    }

    if (this.loading && !tbody.length) {
      $(this.el.nativeElement).find('tbody').hide();
      this.el.nativeElement.getElementsByClassName(this.footerClass).item(0).innerHTML = `
        <tr><td class="text-center" colspan="999">
          <img src="/assets/svg-loaders/cow.gif" style="width: 30px;" class="mr-1">
          Loading...
        </td></tr>`;
    } else if (!tbody.length) {
      this.el.nativeElement.getElementsByClassName(this.footerClass).item(0).innerHTML = `<tr><td class="text-center" colspan="999">No Data Available</td></tr>`;
    }
    if (this.size === 'sm') {
      $(this.el.nativeElement).find('td').removeClass();
      $(this.el.nativeElement).find('th').removeClass();
      $(this.el.nativeElement).find('td').addClass("pl-1 pr-0 py-0 font-sm");
      $(this.el.nativeElement).find('th').addClass("pl-1 pr-0 py-0 font-sm");
    }
    $(this.el.nativeElement).find('td').addClass("bg-white vertical-inherit");
    $(this.el.nativeElement).find('th').addClass("bg-primary text-white vertical-inherit");
  }
}
