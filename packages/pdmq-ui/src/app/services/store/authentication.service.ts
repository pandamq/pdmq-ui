import { Injectable } from '@angular/core';
import { StorageMap } from '@ngx-pwa/local-storage';
import { mergeMap } from 'rxjs/operators';
import { RequestService } from '../common/request.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private _http: RequestService,
    private _storage: StorageMap
  ) { }

  /**
   * Login
   * 
   * @param payload 
   * @returns 
   */
  login(payload: {
    email: string,
    password: string
  }) {
    return this._http.post('/authentication/login', payload);
  }

  /**
   * Verify Token
   * 
   * @param token 
   * @param refreshToken 
   * @returns 
   */
  verifyToken(token: string, refreshToken: string) {
    return this._http.post('/authentication/token/verify', {token, refreshToken});
  }

  /**
   * Generate access key
   */
  generateAccessKey() {
    return this._http.post('/authentication/access-key');
  }

  /**
   * Fetch access key summary
   */
  fetchAccessKey() {
    return this._http.get('/authentication/access-key/summary')
  }
}
