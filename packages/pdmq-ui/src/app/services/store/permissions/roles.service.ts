import { Injectable } from '@angular/core';
import { stringify } from 'qs';
import { Observable } from 'rxjs';
import { FetchResponseInterface } from 'src/app/interfaces/common/fetch-response.interface';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';
import { RoleInterface } from 'src/app/interfaces/schema/role.interface';
import { RequestService } from '../../common/request.service';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  constructor(
    private _http: RequestService
  ) { }

  /**
   * Fetch role list
   */
  fetchSummary() {
    return this._http.get('/permissions/roles/summary')
  }

  /**
   * Fetch role list
   * 
   * @param pagination 
   */
  fetch(pagination: PaginationInterface): Observable<FetchResponseInterface<RoleInterface>> {
    return this._http.get('/permissions/roles?' + stringify(pagination))
  }

  /**
   * Delete role
   * 
   * @param id 
   */
  delete(id: string) {
    return this._http.delete('/permissions/roles/' + id);
  }

  /**
   * Update individual login
   * 
   * @param payload 
   */
  save(payload: RoleInterface): Observable<{result: RoleInterface}> {
    return this._http.post('/permissions/roles', payload);
  }
}
