import { Injectable } from '@angular/core';
import { stringify } from 'qs';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';
import { ResourceInterface } from 'src/app/interfaces/schema/resource.interface';
import { RequestService } from '../../common/request.service';

@Injectable({
  providedIn: 'root'
})
export class ResourcesService {

  constructor(
    private _http: RequestService
  ) { }

  /**
   * Fetch resource list
   * 
   * @param pagination 
   */
  fetch(pagination: PaginationInterface) {
    return this._http.get('/permissions/resources?' + stringify(pagination))
  }

  /**
   * Fetch resource list
   */
  fetchSummary() {
    return this._http.get('/permissions/resources/summary')
  }

  /**
   * Delete resources
   * 
   * @param id 
   */
  delete(id: string) {
    return this._http.delete('/permissions/resources/' + id);
  }

  /**
   * Update individual login
   * 
   * @param payload 
   */
  save(payload: ResourceInterface) {
    return this._http.post('/permissions/resources', payload);
  }
}
