import { Injectable } from '@angular/core';
import { stringify } from 'qs';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';
import { LoginInterface } from 'src/app/interfaces/schema/login.interface';
import { RequestService } from '../../common/request.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private _http: RequestService
  ) { }

  /**
   * Change user password
   * 
   * @param password 
   */
  changePassword(password: string) {
    return this._http.patch('/permissions/users/change-password', {
      password
    })
  }

  /**
   * Fetch user list
   * 
   * @param pagination 
   */
  fetch(pagination: PaginationInterface) {
    return this._http.get('/permissions/users?' + stringify(pagination))
  }

  /**
   * Update individual login
   * 
   * @param payload 
   */
  save(payload: LoginInterface) {
    return this._http.post('/permission/users', payload);
  }
}
