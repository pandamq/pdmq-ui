import { Injectable } from '@angular/core';
import { SystemSettings } from 'src/app/enums/system-settings.enum';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';
import { SystemSettingInterface } from 'src/app/interfaces/schema/system-setting.interface';
import { RequestService } from '../common/request.service';

@Injectable({
  providedIn: 'root'
})
export class SystemSettingsService {

  constructor(
    private _http: RequestService
  ) { }

  /**
   * Fetch settings
   * 
   * @param pagination 
   */
  fetch() {
    return this._http.get('/system/settings/' + name);
  }

  /**
   * Fetch Item by Name
   * 
   * @param name 
   */
  fetchByName(name: SystemSettings) {
    return this._http.get('/system/settings/' + name);
  }

  /**
   * Save Setting
   * 
   * @param payload 
   */
  save(payload: SystemSettingInterface) {
    return this._http.post('/system/settings', payload);
  }

  
}
