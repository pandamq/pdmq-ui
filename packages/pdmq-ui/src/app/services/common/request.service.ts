import { catchError, map, mergeAll, mergeMap, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from "@auth0/angular-jwt";
import { LocalStorage } from '@ngx-pwa/local-storage';
import { of, Observable } from 'rxjs';
import { CommunicatorService } from './communicator.service';
import { environment } from 'src/environments/environment';

interface HttpOptions {
  header?: any,
  baseURL?: string
}

interface ResponseInterface {
  statusCode: number,
  message: any
}

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  private baseURL     : string = environment.API_URL;
  private token       : string;
  private refreshToken: string;

  private refreshTokenObservable: Observable<any>;

  private _jwt: JwtHelperService = new JwtHelperService();

  public roleId     : string;
  public locationId : string;
  public operationId: string;

  constructor(
    private _http: HttpClient,
    private _store: LocalStorage,
    private _comm: CommunicatorService
  ) {

  }

  /**
   * Set Tokens
   * 
   * @param tokens 
   */
  async setTokens(tokens: {
    token: string,
    refreshToken: string
  }) {
    this.token = tokens.token;
    this.refreshToken = tokens.refreshToken;
    
    await Promise.all([
      this._store.setItem('token', tokens.token).toPromise(),
      this._store.setItem('refreshToken', tokens.refreshToken).toPromise()
    ])
  }

  /**
   * Refresh Token
   * 
   * @param token 
   * @returns 
   */
  renewToken(token: string) {
    return this._store.getItem('refreshToken').pipe(
      mergeMap((refreshToken: string) => {
        if (!this.refreshTokenObservable) this.refreshTokenObservable = this._http.post(
          this.baseURL + '/authentication/token/refresh',
          {token: token, refreshToken: refreshToken},
          {
            headers: new HttpHeaders({
              authorization: 'Bearer ' + refreshToken
            })
          }
        )
        return this.refreshTokenObservable;
      }),
      mergeMap((response: {message: any}) => {
        this.refreshTokenObservable = null;
        this.token = response.message['token'];
        this.refreshToken = response.message['refreshToken'];
        return this._store.setItem('token', this.token);
      }),
      mergeMap(() => {
        return this._store.setItem('refreshToken', this.refreshToken);
      }),
      map(() => this.token)
    );
  }

  private getHeader(options?: HttpOptions) {
    return this._store.getItem('token').pipe(
      mergeMap((token: string) => {
        if (!token) return of("");
        else if (this._jwt.isTokenExpired(token)) return this.renewToken(token);
        return of(token)
      }),
      map((token: string) => {
        const header: any = {
          timeout: (10 * 60 * 1000).toString(),
          'x-role-id'     : this.roleId      || '',
          'x-client-type' : `WEB-${environment.production ? 'PRODUCTION' : 'DEVELOP'}`
        };
        const headerKeys = Object.keys(options?.header || {});
        headerKeys.forEach((key) => header[key] = options.header[key]);
        if (token) header.authorization = `Bearer ${token}`;
        
        return new HttpHeaders(header);
      })
    )
  }

  handleError(error, type: string) {
    this._comm.showErrors(error.error?.message || error.message || error.statusText);
  }

  /**
   * Ger Request Wrapper
   * 
   * @param url 
   * @param options 
   * @returns 
   */
  get(url, options: HttpOptions = {header: {}, baseURL: this.baseURL}) {
    let ref;
    return this.getHeader(options).pipe(
      tap(() => ref = this._comm.showLoading()),
      mergeMap((headers: HttpHeaders) => {
        return this._http.get(this.baseURL + url, {headers})
      }),
      map((res: ResponseInterface) => {
        return res.message;
      }),
      tap(() => this._comm.hideLoading(ref)),
      catchError((error, caught) => {
        this._comm.hideLoading(ref);
        this.handleError(error, `[GET] ${URL}`);
        throw error;
      })
    );
  }

  /**
   * Delete Request Wrapper
   * 
   * @param url 
   * @param options 
   * @returns 
   */
  delete(url, options: HttpOptions = {header: {}, baseURL: this.baseURL}) {
    let ref;
    return this.getHeader(options).pipe(
      tap(() => ref = this._comm.showLoading()),
      mergeMap((headers: HttpHeaders) => {
        return this._http.delete(this.baseURL + url, {headers})
      }),
      map((res: ResponseInterface) => {
        return res.message;
      }),
      tap(() => this._comm.hideLoading(ref)),
      catchError((error, caught) => {
        this._comm.hideLoading(ref);
        this.handleError(error, `[GET] ${URL}`);
        throw error;
      })
    );
  }

  post(url, data: any = {}, options: HttpOptions = {header: {}, baseURL: this.baseURL}) {
    let ref;
    return this.getHeader(options).pipe(
      tap(() => ref = this._comm.showLoading()),
      mergeMap((headers: HttpHeaders) => {
        return this._http.post(this.baseURL + url, data, {headers})
      }),
      map((res: ResponseInterface) => {
        return res.message;
      }),
      tap(() => this._comm.hideLoading(ref)),
      catchError((error, caught) => {
        this._comm.hideLoading(ref);
        this.handleError(error, `[POST] ${URL}`);
        throw error;
      })
    );
  }

  patch(url, data: any = {}, options: HttpOptions = {header: {}, baseURL: this.baseURL}) {
    let ref;
    return this.getHeader(options).pipe(
      tap(() => ref = this._comm.showLoading()),
      mergeMap((headers: HttpHeaders) => {
        return this._http.patch(this.baseURL + url, data, {headers})
      }),
      map((res: ResponseInterface) => {
        return res.message;
      }),
      tap(() => this._comm.hideLoading(ref)),
      catchError((error, caught) => {
        this._comm.hideLoading(ref);
        this.handleError(error, `[PATCH] ${URL}`);
        throw error;
      })
    );
  }

}
