import { Injectable, NgZone } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { MatDialog, MatDialogRef, MatDialogState } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LoaderComponent } from 'src/app/components/modal/loader/loader.component';
import { ConfirmComponent } from 'src/app/components/modal/confirm/confirm.component';

@Injectable({
  providedIn: 'root'
})
export class CommunicatorService {

  private communicator: Subject<{action: string, data?: any}> = new Subject();
  private loader: MatDialogRef<LoaderComponent, any>;

  constructor(
    private _dialog: MatDialog,
    private _toast: MatSnackBar,
    private _ngZone: NgZone
  ) { }

  communicate(action ,data:any = []) {
    this.communicator.next({
      action: action,
      data: data
    });
  }

  getCommunicate(): Observable<{action: string, data?: any}> {
    return this.communicator.asObservable();
  }

  showToast(message, panelClass = "bg-success") {
    this._ngZone.run(() => {
      this._toast.open(message, 'X', {
        duration: 4 * 1000,
        panelClass
      })
    });
  }

  showErrors(message) {
    this._toast.open('😥 ' + message, 'X', {
      duration: 30 * 1000,
      panelClass: "bg-danger"
    });
  }

  showLoading() {
    if (this.loader) return this.loader;
    const ref = this._dialog.open(LoaderComponent, {
      width: "150px",
      panelClass: ["loadingDialog"]
    })

    setTimeout(() => {
      const state = ref.getState();
      if (state != MatDialogState.CLOSED) {
        ref.close();
        this.showErrors("Connection timeout, please refresh the page and try again later.")
      }
    }, 1000 * 120);

    this.loader = ref;
    return ref;
  }

  hideLoading(ref: MatDialogRef<LoaderComponent, any> = this.loader) {
    if (!ref) return;
    ref.close();
    this.loader = null;
  }

  confirm(message, title?: string): Promise<boolean> {
    const ref = this._dialog.open(ConfirmComponent, {
      panelClass: ["dialog", "dialog-sm"],
      data: {
        message,
        title
      }
    })
    return ref.afterClosed().toPromise();
  }

  copy(str) {
    const el = document.createElement('textarea');
    el.value = str;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);

    this.showToast("Copied");
  };
}
