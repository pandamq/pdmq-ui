import { Injectable } from '@angular/core';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';
import { RequestService } from '../common/request.service';
import { stringify } from 'qs';

@Injectable({
  providedIn: 'root'
})
export class WorkflowExecutionLogsService {

  constructor(
    private _http: RequestService
  ) { }

  /**
   * Fetch logs
   */
  fetch(options?: {
    workflowId: string,
    nodeId: number,
    pagination?: PaginationInterface
  }) {
    return this._http.get(`/pdmq/workflows/${options.workflowId}/nodes/${options.nodeId}/logs?` + stringify(options.pagination))
  }

}
