import { TestBed } from '@angular/core/testing';

import { WorkflowExecutionLogsService } from './workflow-execution-logs.service';

describe('WorkflowExecutionLogsService', () => {
  let service: WorkflowExecutionLogsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WorkflowExecutionLogsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
