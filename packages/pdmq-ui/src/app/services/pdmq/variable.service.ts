import { Injectable } from '@angular/core';
import { PDMQVariable } from 'pdmq/dist/interfaces/variable.interface';
import { stringify } from 'qs';
import { Observable } from 'rxjs';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';
import { RequestService } from '../common/request.service';

@Injectable({
  providedIn: 'root'
})
export class VariableService {

  constructor(
    private _http: RequestService
  ) { }

  /**
   * Fetch Names
   */
  fetchNames(): Observable<{
    results: string[]
  }> {
    return this._http.get('/pdmq/variables/names');
  }

  /**
   * Fetch Examples
   * 
   * @returns 
   */
  fetchExamples(): Observable<{
    results: {text: string, value: string}[]
  }> {
    return this._http.get('/pdmq/variables/examples');
  }

  /**
   * Fetch stored variables
   */
  fetch(pagination?: PaginationInterface) {
    return this._http.get('/pdmq/variables?' + stringify(pagination));
  }

  /**
   * Create Variable
   * 
   * @param payload 
   */
   save(payload: PDMQVariable) {
    return this._http.post('/pdmq/variables', payload);
  }

  /**
   * Delete Variable
   * 
   * @param id
   */
  delete(id: string) {
    return this._http.delete('/pdmq/variables/' + id)
  }
}
