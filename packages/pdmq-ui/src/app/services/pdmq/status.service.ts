import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { StatusActionsTypes } from 'src/app/store/actions-types/status.action-types';
import { StatusActions } from 'src/app/store/status/pdmq-status.actions';


@Injectable({
  providedIn: 'root'
})
export class StatusService {

  constructor(
    private _store: Store
  ) {

  }

  fetchProcessingTask() {
    
  }

  fetchConsumerCount() {

  }

  fetchConsumers() {

  }
}
