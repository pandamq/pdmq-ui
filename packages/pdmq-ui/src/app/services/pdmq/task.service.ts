import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PDMQTask } from 'pdmq/dist/interfaces/task.interface';
import { TaskTriggerTypes } from 'src/app/enums/task-trigger-types.enum';
import { RequestService } from '../common/request.service';
import { stringify } from 'querystring';
import { FetchResponseInterface } from 'src/app/interfaces/common/fetch-response.interface';


@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(
    private _http: RequestService,
  ) {

  }

  /**
   * Save Task
   * 
   * @param payload 
   */
  saveTask(payload: PDMQTask) {
    return this._http.post('/pdmq/tasks', payload);
  }

  /**
   * Delete Task
   * 
   * @param task_id 
   * @returns 
   */
  deleteTask(task_id: string): Observable<string> {
    return this._http.delete('/pdmq/tasks/' + task_id)
  }

  /**
   * Fetch List of tasks
   * 
   * @param from 
   * @param to 
   * @param type 
   * @returns {Observable} Observable
   */
  fetchTasks(options: {
    from?: string, 
    to?: string, 
    type?: TaskTriggerTypes
  })
    : Observable<FetchResponseInterface<PDMQTask>> {
    return this._http.get('/pdmq/tasks?' + stringify(options))
  }

  /**
   * Fetch All Stored Tasks
   * 
   * @returns {Observable} Observable
   */
  fetchStoredTasks() 
  : Observable<FetchResponseInterface<PDMQTask>> {
    return this._http.get('/pdmq/tasks?' + stringify({
      type: TaskTriggerTypes.NEVER
    }))
  }
}
