import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FetchResponseInterface } from 'src/app/interfaces/common/fetch-response.interface';
import { PDMQWorkflow } from 'pdmq/dist/interfaces/workflow.interface';
import { RequestService } from '../common/request.service';
import { PaginationInterface } from 'src/app/interfaces/common/pagination.interface';
import { stringify } from 'qs';
import { WorkflowCronInterface } from 'src/app/interfaces/workflow-cron.interface';

@Injectable({
  providedIn: 'root'
})
export class WorkflowService {

  constructor(
    private _http: RequestService
  ) { }

  /**
   * Trigger workflow
   * 
   * @param workflowId 
   * @returns 
   */
  run(workflowId: string) {
    return this._http.post('/pdmq/workflows/' + workflowId + '/run');
  }

  /**
   * Fetch Cron List
   */
  fetchWorkflowCronList() {
    return this._http.get('/pdmq/workflows/cron/summary')
  }

  /**
   * Stop workflow execution
   * 
   * @param workflowId 
   * @returns 
   */
  stop(workflowId: string) {
    return this._http.post('/pdmq/workflows/' + workflowId + '/stop');
  }
  
  /**
   * Fetch list of workflows
   * 
   * @returns {Observable} Observable
   */
  fetchWorkflows(pagination: PaginationInterface = {}) : Observable<FetchResponseInterface<PDMQWorkflow>> {
    return this._http.get('/pdmq/workflows?' + stringify(pagination))
  }

  /**
   * Fetch single workflow
   */
  fetchItem(id: string): Observable<{result: PDMQWorkflow}> {
    return this._http.get('/pdmq/workflows/' + id);
  }

  /**
   * Save Workflow
   * 
   * @param workflow
   */
  saveWorkflow(workflow): Observable<{result: PDMQWorkflow}> {
    return this._http.post('/pdmq/workflows', workflow)
  }

  /**
   * Delete Workflow
   * 
   * @param workflowId 
   * @returns 
   */
  deleteWorkflow(workflowId: string): Observable<string> {
    return this._http.delete('/pdmq/workflows/' + workflowId)
  }

  /**
   * Toggle Workflow Cron Status
   * 
   * @param workflowId 
   */
  toggleWorkflowCronStatus(workflowId: string): Observable<{ result: WorkflowCronInterface }> {
    return this._http.post('/pdmq/workflows/cron/' + workflowId + '/toggle-status')
  }
}
