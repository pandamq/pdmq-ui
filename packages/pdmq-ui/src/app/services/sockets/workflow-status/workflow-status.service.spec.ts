import { TestBed } from '@angular/core/testing';

import { WorkflowStatusService } from './workflow-status.service';

describe('WorkflowStatusService', () => {
  let service: WorkflowStatusService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WorkflowStatusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
