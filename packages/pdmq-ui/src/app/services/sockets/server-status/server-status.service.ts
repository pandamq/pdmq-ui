import { Injectable } from '@angular/core';
import { StorageMap } from '@ngx-pwa/local-storage';
import { Socket } from 'ngx-socket-io';
import { Observable } from 'rxjs';
import { finalize, map, mergeMap, tap } from 'rxjs/operators';
import { SocketGateways } from 'src/app/enums/socket-gateways.enum';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServerStatusService {

  constructor(
    private _socket: Socket,
    private _storage: StorageMap
  ) {}

  /**
   * Subscribe to workflow status change
   * 
   * @param workflowId 
   * @returns 
   */
  getStatus() {
    this._socket.disconnect();
    return this._storage.get('token').pipe(
      mergeMap(token => {
        return new Observable((res) => {
          this._socket = new Socket({
            url: environment.WSS_URL,
            options: {
              transportOptions: {
                polling: {
                  extraHeaders: {
                    Authorization: `Bearer ${token}`
                  }
                }     
              }
            }
          })
          
          this._socket.on('connect', () => {
            return res.next();
          });
          this._socket.connect();
        })
      }),
      mergeMap(() => {
        this._socket.emit(SocketGateways.GET_SERVER_STATUS);
        return this._socket.fromEvent(SocketGateways.GET_SERVER_STATUS).pipe(
          map((data: string) => {
            try {
              return JSON.parse(data);
            } catch (error) {
              return data
            }
          })
        );
      })
    )
    
  }
}
