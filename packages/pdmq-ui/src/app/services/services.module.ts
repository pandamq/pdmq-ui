import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestService } from './common/request.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [

  ],
  providers: [
    RequestService
  ],
  imports: [
    HttpClientModule,
    CommonModule
  ]
})
export class ServicesModule { }
