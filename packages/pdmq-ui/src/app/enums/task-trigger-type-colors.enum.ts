export enum TaskTriggerTypeColors {
  DAILY = "#CECFB0",
  WEEKLY = "#FD7F76",
  ONCE = "#448E67",
  MONTHLY = "#5EA740",
  ANNUALLY = "#FDBC47"
}
