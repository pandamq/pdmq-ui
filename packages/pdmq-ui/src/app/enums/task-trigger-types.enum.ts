export enum TaskTriggerTypes {
  TIME    = "time",
  NEVER   = "never",
  INSTANT = "instant",
  ALL     = "all"
}
