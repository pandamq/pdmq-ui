export enum SocketGateways {
  GET_WORKFLOW_STATUS = 'GetWorkflowStatus',
  GET_SERVER_STATUS   = 'GetServerStatus'
}