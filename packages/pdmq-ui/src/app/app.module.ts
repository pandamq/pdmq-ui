import { PDMQStatusEffects } from './store/status/pdmq-status.effects';
import { DashboardModule } from './dashboard/dashboard.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { InitialConnectionModule } from './initial-connection/initial-connection.module';
import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { StoreModule } from '@ngrx/store';
import { PDMQTaskReducer } from './store/pdmq-task/pdmq-task.reducer';
import { EffectsModule } from '@ngrx/effects';
import { TaskEffects } from './store/pdmq-task/pdmq-task.effects';
import { PDMQStatusReducer } from './store/status/pdmq-status.reducer';
import { PDMQWorkflowReducer } from './store/pdmq-workflow/pdmq-workflow.reducer';
import { PDMQWorkflowEffects } from './store/pdmq-workflow/pdmq-workflow.effects';
import { AuthenticationReducer } from './store/authentication/authentication.reducer';
import { AuthenticationEffects } from './store/authentication/authentication.effects';
import { ServicesModule } from './services/services.module';
import { StorageModule } from '@ngx-pwa/local-storage';
import { PDMQVariableReducer } from './store/pdmq-variable/pdmq-variable.reducer';
import { PDMQVariableEffects } from './store/pdmq-variable/pdmq-variable.effects';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { PDMQWorkflowExecutionLogReducer } from './store/pdmq-workflow-execution-log/pdmq-workflow-execution-log.reducer';
import { PDMQWorkflowExecutionLogEffects } from './store/pdmq-workflow-execution-log/pdmq-workflow-execution-log.effects';
import { SystemSettingEffects } from './store/system-settings/system-settings.effects';
import { SystemSettingReducer } from './store/system-settings/system-settings.reducer';
import { UserReducer } from './store/user/user.reducer';
import { RoleReducer } from './store/role/role.reducer';
import { ResourceReducer } from './store/resource/resource.reducer';
import { UserEffects } from './store/user/user.effects';
import { RoleEffects } from './store/role/role.effects';
import { ResourceEffects } from './store/resource/resource.effects';
import { environment } from 'src/environments/environment';
import { PipesModule } from './pipes/pipes.module';

const config: SocketIoConfig = {
  url: environment.WSS_URL,
  options: {
    autoConnect: false
  }
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    PipesModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ComponentsModule,
    ServicesModule,
    DashboardModule,
    InitialConnectionModule,
    SocketIoModule.forRoot(config),
    StorageModule.forRoot({
      IDBDBName: environment.RELEASE_NAME,
      LSPrefix: environment.RELEASE_VERSION + "_"
    }),
    StoreModule.forRoot({
      User          : UserReducer,
      Role          : RoleReducer,
      Resource      : ResourceReducer,
      Authentication: AuthenticationReducer,
      SystemSetting : SystemSettingReducer,
      PDMQTask      : PDMQTaskReducer,
      PDMQStatus    : PDMQStatusReducer,
      PDMQVariable  : PDMQVariableReducer,
      PDMQWorkflow  : PDMQWorkflowReducer,
      PDMQWorkflowExecutionLog  : PDMQWorkflowExecutionLogReducer,
    }),
    EffectsModule.forRoot([
      UserEffects,
      RoleEffects,
      ResourceEffects,
      TaskEffects,
      PDMQStatusEffects,
      PDMQVariableEffects,
      PDMQWorkflowEffects,
      PDMQWorkflowExecutionLogEffects,
      AuthenticationEffects,
      SystemSettingEffects
    ])
  ],
  providers: [],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
