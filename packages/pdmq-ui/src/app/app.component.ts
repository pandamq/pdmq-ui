import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthenticationActions } from './store/authentication/authentication.actions';
import { Subscription } from 'rxjs';
import { AuthenticationSelector } from './store/authentication/authentication.selectors';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public subs: Subscription[] = [];
  public loaded = false;

  constructor(
    private _router: Router,
    private _store: Store
  ) {
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this._store.dispatch(AuthenticationActions.DO_APP_LOAD({
        cb: () => {
          /** Subscribe User Login Status */
          this.loaded = true;
          this.subs.push(
            this._store.select(AuthenticationSelector.isLoggedIn).subscribe((loginStatus: boolean) => {
              if (loginStatus && location.pathname == '/')
                this._router.navigateByUrl('/dashboard')
              else if (!loginStatus && location.pathname != '/')
                this._router.navigateByUrl('/');
            })
          )
        }
      }))
    }, 0);
  }
}
