#!/bin/bash
find /applications -name "main.*.js" -type f -exec sed -i -e "s_http://localhost:3000_${API_URL}_g" {} + &&
find /applications -name "main.*.js" -type f -exec sed -i -e "s_ws://localhost:3000_${WSS_URL}_g" {} + &&
service nginx restart
watch service nginx status