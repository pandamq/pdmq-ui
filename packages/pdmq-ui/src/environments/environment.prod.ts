export const environment = {
  production: true,
  RELEASE_NAME: 'DARLENE',
  RELEASE_VERSION: '1',
  API_URL: "http://localhost:3000",
  WSS_URL: "ws://localhost:3000"
};
