import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  const config = new DocumentBuilder()
    .setTitle('PDMQ API')
    .setVersion('1.1.6.1')
    .addBearerAuth()
    .addSecurity('x-api-key', {
      type        : 'apiKey',
      description : 'x-api-key',
      name        : 'x-api-key',
      in          : 'header'
    })
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);

  await app.listen(3000);
}
bootstrap();
