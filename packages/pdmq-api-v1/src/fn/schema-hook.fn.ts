import { HttpStatus } from "@nestjs/common";

export const MongooseMiddlewarePostInit = (schema) => {
  return () => {
    schema.pre(/(save)|(update)|(insert)/i, async function (this: any, next: any) {
      const self: any = this;
      if (self._update) {
        self._update.updated_at = new Date();
        self._update.revision   = self._update.revision ? (self._update.revision + 1) : 1
      }

      next()
    });

    schema.pre(/update/i, function (this: any, next: any) {
      const self: any = this;
      if (self.is_system && !self.is_active) {
        throw {
          statusCode: HttpStatus.BAD_REQUEST,
          message   : "You can NOT remove system entity."
        }
      }
      next();
    })

    return schema;
  }
}
