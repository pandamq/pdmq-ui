import { Test, TestingModule } from '@nestjs/testing';
import { SystemVariablesService } from './system-variables.service';

describe('SystemVariablesService', () => {
  let service: SystemVariablesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SystemVariablesService],
    }).compile();

    service = module.get<SystemVariablesService>(SystemVariablesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
