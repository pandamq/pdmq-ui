import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as moment from 'moment';
import { Model } from 'mongoose';
import { SystemVariablesList } from 'src/constants/common/system-variables.const';
import { SystemVariables } from 'src/enum/system/system-variables.enum';
import { Variable, VariableDocument } from 'src/schema/tbl_variables.schema';

@Injectable()
export class SystemVariablesService {

  constructor(
    @InjectModel(Variable.name)
    private _variableModel: Model<VariableDocument>
  ) {}

  /**
   * Replace variables in the message
   * 
   * @param message 
   */
  async replaceVariables(message: string, context?: any) {
    /** Fetch Custom Variables */
    const variableDocs = await this._variableModel.find({
      is_active: true
    }).select([
      "variable_name",
      "variable_data"
    ]);

    /** Replace Custom Variables */
    variableDocs.forEach(variable => {
      if (message.includes(`{{${variable.variable_name}}}`))
        message = message.split(`{{${variable.variable_name}}}`).join(variable.variable_data);
    });

    /** Replace System Variables */
    SystemVariablesList.forEach(variable => {
      if (message.includes(`{{${variable.text}}}`)) {
        let value;
        if (variable.text == SystemVariables.DATE) {
          value = moment().format("DD MMM YYYY")
        }
        if (variable.text == SystemVariables.TIME) {
          value = moment().format("HH:mm A")
        }
        if (variable.text == SystemVariables.DATETIME) {
          value = moment().format("DD MMM YYYY HH:mm A")
        }
        if (variable.text == SystemVariables.ISO_DATETIME) {
          value = moment().toISOString();
        }
        if (variable.text == SystemVariables.CONTEXT && context) {
          value = context.toString();
        }
        message = message.split(`{{${variable.text}}}`).join(value);
      }
    })

    return message;
  }

  /**
   * Fetch a list of all variables
   */
  async listAllVariables() {
    /** Fetch Custom Variables */
    const variableDocs = await this._variableModel.find({
      is_active: true
    }).select([
      "variable_name",
      "variable_data"
    ]);
    return SystemVariablesList.map(variable => {
      let value;
      if (variable.text == SystemVariables.DATE) {
        value = moment().format("DD MMM YYYY")
      }
      if (variable.text == SystemVariables.TIME) {
        value = moment().format("HH:mm A")
      }
      if (variable.text == SystemVariables.DATETIME) {
        value = moment().format("DD MMM YYYY HH:mm A")
      }
      if (variable.text == SystemVariables.ISO_DATETIME) {
        value = moment().toISOString()
      }
      return {
        text: variable.text,
        value
      }
    }).concat(variableDocs.map(variable => ({
      text: variable.variable_name,
      value: variable.variable_data
    })) as any)
  }

}
