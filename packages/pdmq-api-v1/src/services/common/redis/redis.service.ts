import { Logger } from "@nestjs/common";
import * as redis from "redis";

export class RedisService {

  private logger: Logger = new Logger(RedisService.name);
  private client: redis.RedisClient;

  constructor() {
    this.init(process.env.REDIS_CONNECTION_STRING).then(() => {
      if (process.env.DEBUG_MODE)
        this.logger.debug("Redis Connected.");
    }).catch((error) => {
      this.logger.error(error);
    })
  }

  /**
   * Initial Redis Connection
   * 
   * @param connectionString 
   * @returns 
   */
  init(connectionString: string): Promise<redis.RedisClient> {
    return new Promise((res, rej) => {
      if (this.client?.connected) return res(this.client);
      this.client = redis.createClient({
        url: connectionString,
        retry_strategy: () => 0
      });

      this.client.on('ready', () => {
        return res(this.client);
      })

      this.client.on('error', (error) => {
        return rej(error);
      })

      setTimeout(() => {
        rej("Connection Timeout.")
      }, 30 * 600);
    })
  }

  /**
   * Promisify SET
   * 
   * @param key 
   * @param value 
   * @returns 
   */
  public set(key, value: string):Promise<'OK'> {
    return new Promise((res, rej) => {
      this.client?.set(key, value, (error, reply) => {
        if (error) rej(error);
        res(reply);
      })
    })
  }

  /**
   * Promisify Set With Index
   * 
   * @param key 
   * @param value 
   * @param indexName 
   * @param index 
   * @returns 
   */
  public setWithIndex(key, value: string, indexName: string, index: number = 0):Promise<any[]> {
    return new Promise((res, rej) => {
      this.client.multi().set(key, value).zadd(indexName, index, key).exec((error, reply) => {
        if (error) rej(error);
        res(reply);
      })
    })
  }

  /**
   * Rearrange index
   * 
   * @param indexName 
   * @param index 
   * @param end 
   * @returns 
   */
  public async zrange(indexName: string, index: number, end:number): Promise<string[]> {
    return new Promise((res, rej) => {
      this.client.zrange(indexName, index, end, (error, reply) => {
        if (error) return rej(error);
        res(reply);
      })
    })
  }

  /**
   * Scan through keys
   * 
   * @param matchWith 
   * @param options 
   * @returns 
   */
  public async searchKeys(matchWith: string, options?: {
    count: number
  }): Promise<string[]> {
    const scan = (searchKey, cursor): Promise<[string, string[]]> => {
      return new Promise((res, rej) => {
        this.client.scan(cursor, "MATCH", searchKey, (error, reply) => {
          if (error) return rej(error);
          else if (!reply) return res(['0', []]);
          else return res(reply)
        })
      })
    }

    let cursor = null, reply = [], maxLooping = 1000;
    while (cursor != '0' && maxLooping > 0) {
      const [tempCursor, tempReply] = await scan(matchWith, cursor === null ? '0' : cursor);
      cursor = tempCursor;
      reply = reply.concat(tempReply);
      if (options?.count && reply.length >= options.count) {
        return reply.slice(0, options.count);
      }
      maxLooping--;
    }

    return reply;
  }
  
  /**
   * Get multi values
   * 
   * @param keys 
   * @returns 
   */
  public getMulti(keys: string[]): Promise<string[]> {
    return new Promise((res, rej) => {
      this.client?.mget(keys, (error, reply) => {
        if (error) rej(error);
        res(reply);
      })
    })
  }

  /**
   * Clone value
   * 
   * @param source 
   * @param destination 
   * @returns 
   */
  public copy(source: string, destination: string) {
    return new Promise((res, rej) => {
      this.client?.send_command(`COPY ${source} ${destination}`, (error, reply) => {
        if (error) rej(error);
        res(reply);
      })
    })
  }

  /**
   * Delete with index
   * 
   * @param key 
   * @param indexName 
   * @returns 
   */
  public deleteWithIndex(key: string, indexName: string): Promise<any> {
    return new Promise((res, rej) => {
      this.client
        .multi()
        .del(key)
        .zrem(indexName, key)
        .exec((error, reply) => {
          if (error) rej(error);
          res(reply);
        })
    })
  }

  /**
   * Delete key in index
   * 
   * @param key 
   * @param indexName 
   * @returns 
   */
  public deleteKeyInIndex(key: string, indexName: string): Promise<number> {
    return new Promise((res, rej) => {
      this.client
        .zrem(indexName, key, (error, reply) => {
          if (error) rej(error);
          res(reply);
        })
    })
  }

  /**
   * Promisify Delete
   * 
   * @param keys 
   * @returns 
   */
  public delete(keys: string[]) {
    return new Promise((res, rej) => {
      if (!keys?.length) res(1);
      this.client.del(...keys, (error, reply) => {
        if (error) rej(error);
        res(reply);
      })
    })
  }
}
