import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SystemSetting, SystemSettingDocument } from 'src/schema/tbl_system_settings.schema';
import * as nodemailer from 'nodemailer';
import { SystemSettings } from 'src/enum/system/system-settings.enum';

@Injectable()
export class EmailService {

  private client: nodemailer.Transporter;
  private setting: {
    host?: string,
    port?: number,
    username?: string,
    password?: string,
  } = {}

  constructor(
    @InjectModel(SystemSetting.name)
    private _systemSettingsModel: Model<SystemSettingDocument>
  ) {}

  /**
   * Reset Client
   */
  resetClient() {
    this.client = null
  }

  /** 
   * Initial Client
   */
  async init() {
    if (this.client) return this.client;
    const setting = await this._systemSettingsModel.findOne({
      system_setting_name: SystemSettings.SMTP,
      is_active: true
    })
    if (!setting)
      return;

    this.setting = setting.system_setting_data;
    this.client = nodemailer.createTransport({
      host: setting.system_setting_data.host,
      port: setting.system_setting_data.port,
      secure: setting.system_setting_data.port == 465,
      auth: {
        user: setting.system_setting_data.username,
        pass: setting.system_setting_data.password
      }
    })
  }

  /**
   * Send plain text email
   * 
   * @param payload 
   */
  async sendPlain(payload: {
    recipients: [],
    subject: string,
    message: string
  }) {
    await this.init();
    if (!this.client) throw {
      statusCode: HttpStatus.BAD_REQUEST,
      message: "SMTP setting not found"
    }

    const result = await this.client.sendMail({
      to: payload.recipients,
      html: payload.message,
      text: payload.message,
      subject: payload.subject,
      from: this.setting.username
    });

    this.client.transporter.close();
    this.client = null;
    return result;
  }

}
