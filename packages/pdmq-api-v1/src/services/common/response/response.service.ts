import { HttpStatus, Inject, Logger } from '@nestjs/common';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Request, Response } from 'express';
import * as moment from 'moment';
import { Model } from 'mongoose';
import { RequestLog, RequestLogDocument } from 'src/schema/tbl_request_logs.schema';

@Injectable()
export class ResponseService {

  private logger: Logger = new Logger(ResponseService.name);

  constructor(
    @InjectModel(RequestLog.name)
    private _requestLogModel: Model<RequestLogDocument>
  ) {}

  /**
   * Respond Request
   * 
   * @param param0 
   */
  send(options: {
    req: Request,
    res: Response,
    statusCode?: HttpStatus,
    message: any
  }) {
    let {
      req,
      res,
      statusCode,
      message
    } = options;

    if (message.statusCode && message.message) {
      statusCode = message.statusCode;
      message    = message.message;
    }

    if (!statusCode)
      statusCode = HttpStatus.OK
      
    if (statusCode != HttpStatus.OK)
      this.logger.error(message, `[${req.method}] ${req['fullPath']}`);

    if (!statusCode)
      statusCode = HttpStatus.INTERNAL_SERVER_ERROR;

    const result = statusCode == HttpStatus.OK ? 'success' : 'fail';
    const environment = process.env.ENVIRONMENT
    /** Send Response */
    res.status(statusCode).send({
      result,
      statusCode,
      message,
      environment
    })

    /** Log Request */
    const respondat  = moment().toDate();
    const timespend  = moment().diff(req['receivedAt'], 'milliseconds');
    const path       = req.originalUrl                 || "";
    const receivedat = req['receivedAt'].toDate()      || null;
    const useragent  = req.headers['user-agent']       || "";
    const origin     = req.headers.origin              || "";
    const method     = req.method                      || "";
    const ip         = req.header['x-forwarded-for']   || req.ip || ""
    const host       = req.headers.host                || "";
    const role       = req.headers['x-role-id']        || "";
    const client     = req.headers['x-client-type']    || "";

    this._requestLogModel.create({
      login       : req['user']?._id,
      key         : req.headers['x-api-key'] || req.headers['authorization'],
      path        : path,
      method      : method,
      status_code : statusCode,
      ip          : ip,
      client      : client,
      useragent   : useragent,
      received_at : receivedat,
      responded_at: respondat,
      time_spend  : timespend,
      error       : statusCode == HttpStatus.OK ? null : message
    })
  };

}
