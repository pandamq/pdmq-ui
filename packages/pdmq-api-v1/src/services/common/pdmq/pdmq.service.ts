import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as pdmq from 'pdmq';
import { PDMQClient } from 'pdmq/dist/client';
import { PDMQConsumer } from 'pdmq/dist/consumer';
import { PDMQTask } from 'pdmq/dist/interfaces/task.interface';

@Injectable()
export class PdmqService {

  public client  : PDMQClient;
  public consumer: PDMQConsumer;
  private logger : Logger = new Logger(PdmqService.name);

  constructor(
    private _env: ConfigService
  ) {
    this.init().then((res) => {
      /** Initial Client */
      this.client   = res.client;

      /** Listen To Task Queue */
      if (!process.env.DISABLE_CONSUMER) {
        this.consumer = res.consumer;

        setTimeout(() => {        
          this.consumer.taskQueue.subscribe((task: PDMQTask) => {
  
            if (process.env.DEBUG_MODE) 
              this.logger.debug(`Catch Task ${task.task_name}...`)
  
            setTimeout(() => {
              task.success({"Test": "Test"});
            }, 10 * 1000);
  
          })
          
          this.consumer.lookupExpiredTasks()
        }, 500);
      }
  
      /** Log Connection Status */
      if (process.env.DEBUG_MODE)
        this.logger.debug("PDMQ Connected.");
    }).catch((error) => {
      this.logger.error(error);
    })
  }

  /**
   * Initial Producer and Consumer
   */
  init() {
    return new pdmq.pandaMQ({
      redis_url: process.env.REDIS_CONNECTION_STRING,
      debug: !!process.env.DEBUG_MODE,
      client_only: !!process.env.DISABLE_CONSUMER
    }).init();
  }

}
