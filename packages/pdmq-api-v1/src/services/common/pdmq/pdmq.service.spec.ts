import { Test, TestingModule } from '@nestjs/testing';
import { PdmqService } from './pdmq.service';

describe('PdmqService', () => {
  let service: PdmqService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PdmqService],
    }).compile();

    service = module.get<PdmqService>(PdmqService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
