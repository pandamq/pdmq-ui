import { Module } from '@nestjs/common';
import { PdmqService } from './pdmq/pdmq.service';
import { RedisService } from './redis/redis.service';
import { ResponseService } from './response/response.service';
import { EmailService } from './email/email.service';
import { MongooseModule } from '@nestjs/mongoose';
import { MongooseMiddlewarePostInit } from 'src/fn/schema-hook.fn';
import { SystemSetting, SystemSettingSchema } from 'src/schema/tbl_system_settings.schema';
import { RequestLog, RequestLogSchema } from 'src/schema/tbl_request_logs.schema';
import { SystemVariablesService } from './system-variables/system-variables.service';
import { Variable, VariableSchema } from 'src/schema/tbl_variables.schema';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: SystemSetting.name,
        useFactory: MongooseMiddlewarePostInit(SystemSettingSchema)
      },
      {
        name: RequestLog.name,
        useFactory: MongooseMiddlewarePostInit(RequestLogSchema)
      },
      {
        name: Variable.name,
        useFactory: MongooseMiddlewarePostInit(VariableSchema)
      },
    ])
  ],
  providers: [
    PdmqService,
    RedisService,
    ResponseService,
    EmailService,
    SystemVariablesService
  ],
  exports: [
    PdmqService,
    RedisService,
    ResponseService,
    EmailService,
    SystemVariablesService
  ]
})
export class CommonServicesModule {}
