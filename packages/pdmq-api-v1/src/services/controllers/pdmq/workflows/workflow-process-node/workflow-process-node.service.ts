import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { LeanDocument, Model, Types } from 'mongoose';
import { WorkflowNode } from 'pdmq/dist/interfaces/workflow-node.interface';
import { Workflow, WorkflowDocument } from 'src/schema/tbl_workflows.schema';
import { WorkflowNodeTypes } from 'pdmq/dist/enums/workflow-node-types.enum';
import { VariablesService } from '../../variables/variables.service';
import { isEqual } from 'lodash';
import { PdmqService } from 'src/services/common/pdmq/pdmq.service';
import { TaskTriggerTypes } from 'src/enum/task-trigger-types.enum';
import { v1 } from 'uuid';
import { InjectModel } from '@nestjs/mongoose';
import { TaskExecution, TaskExecutionDocument } from 'src/schema/tbl_task_executions.schema';
import { FieldTypes } from 'pdmq/dist/enums/field-types.enum';
import { EmailService } from 'src/services/common/email/email.service';
import { PDMQTask } from 'pdmq/dist/interfaces/task.interface';
import { WorkflowExecutionLog, WorkflowExecutionLogDocument } from 'src/schema/tbl_workflow_execution_logs.schema';
import { WorkflowNodeCategories } from 'pdmq/dist/enums/workflow-node-categories.enum';
import * as jp from "jsonpath";
import { Variable, VariableDocument } from 'src/schema/tbl_variables.schema';
import { SystemVariablesService } from 'src/services/common/system-variables/system-variables.service';
import { WorkflowExecution, WorkflowExecutionDocument } from 'src/schema/tbl_workflow_executions.schema';
import { WorkflowExecutionStatus } from 'pdmq/dist/enums/workflow-execution-status.enum';

@Injectable()
export class WorkflowProcessNodeService {

  private logger = new Logger(WorkflowProcessNodeService.name);

  constructor(
    private _http: HttpService,
    private _pdmq: PdmqService,
    private _variable: VariablesService,
    private _email: EmailService,
    private _systemVariables: SystemVariablesService,

    @InjectModel(TaskExecution.name)
    private _execModel: Model<TaskExecutionDocument>,

    @InjectModel(WorkflowExecution.name)
    private _workflowExecModel: Model<WorkflowExecutionDocument>,

    @InjectModel(Variable.name)
    private _variableModel: Model<VariableDocument>,

    @InjectModel(WorkflowExecutionLog.name)
    private _workflowExecLogModel: Model<WorkflowExecutionLogDocument>,

    @InjectModel(Workflow.name)
    private _workflowModel: Model<WorkflowDocument>
  ) {
    /** Mark running tasks as stopped */
    this.markRunningTasksAsStopped()
  }

  /**
   * Get Data By Path
   * 
   * @param path 
   */
  getDataByPath(body: any, path: string) {
    const data = jp.query(body, path);
    if (data.length > 1) return data;
    return data[0];
  }

  /**
   * Mark running task as stopped
   */
  async markRunningTasksAsStopped() {
    
    const logDocs = await this._workflowExecLogModel.find({
      is_active: true,
      process_completed_at: {
        $eq: null
      }
    }).then(docs => docs.map(doc => doc.toJSON()));
    if (!logDocs.length) return;
    await this._workflowExecLogModel.updateMany({
      _id: {
        $in: logDocs.map(doc => doc._id)
      }
    }, {
      process_completed_at: new Date(),
      node_execution_result: {
        success: false,
        result: "Execution failed due to application restart"
      }
    });

    for (const logDoc of logDocs) {
      if (logDoc.node_category == WorkflowNodeCategories.TASK) {
        /** Check Execute Result */
        await this._pdmq.client.deleteTask(logDoc.node_execution_id);

        /** Check Execution Result */
        const execResult = await this._pdmq.client.getExecutionResult(logDoc.node_execution_id, {
          remove: true
        });

        if (execResult) {
          await this._execModel.create({
            task_execution_id      : logDoc.node_execution_id,
            task_execution_success : execResult.success,
            task_execution_result  : execResult.error || execResult.res,
            task_execution_task_id : execResult.task.task_id
          });

          await this._workflowExecLogModel.updateOne({
            _id: logDoc._id
          }, {
            process_completed_at: new Date(),
            node_execution_result: {
              success: execResult.success,
              result: execResult.error || execResult.res
            }
          });
        }
      }
    }
  }

  /**
   * Track task output 
   */
  trackTaskResult(execId): Promise<{
    res    ?: any,
    error  ?: any,
    task   ?: PDMQTask,
    success : boolean
  }> {
    return new Promise((res, rej) => {
      const interval = setInterval(async () => {
        try {
          const execResult = await this._pdmq.client.getExecutionResult(execId, {
            remove: true
          });

          if (execResult) {
            await this._execModel.create({
              task_execution_id: execId,
              task_execution_success: execResult.success,
              task_execution_result: execResult.error || execResult.res,
              task_execution_task_id: execResult.task.task_id
            });

            clearInterval(interval);

            return res(execResult)
          }

        } catch (error) {
          return rej(error);
        }
      }, 10 * 1000)
    })
  }

  /**
   * Put task to queue and track result
   * 
   * @param payload 
   */
  async putTaskToQueue(payload: {
    processingNode: WorkflowNode 
  }) {
    const {
      processingNode
    } = payload;
    try {
      /** Load stored task detail */
      const storedTask = await this._pdmq.client.findStoredTaskById(processingNode.data.type);
      if (!storedTask) throw "Stored Task No Logger Valid"

      const taskExecId = v1();
      await this._pdmq.client.addTask({
        ...storedTask,
        task_trigger_type: TaskTriggerTypes.INSTANT,
        task_exec_id: taskExecId
      })

      if (process.env.DEBUG_MODE)
        this.logger.debug(`Processing Task ${processingNode.data.type}`)

      return this.trackTaskResult(taskExecId);
    } catch (error) {
      this.logger.error(error);
      throw error;
    }
  }

  /**
   * Verify context meet condition
   * 
   * @param context 
   */
  async verifyCondition(payload: {
    processingNode: WorkflowNode, 
    context: any
  }) {
    const {
      processingNode,
      context
    } = payload;
    
    try {
      const nodeType = processingNode.data.type;

      /** If Succeed */
      if (nodeType == WorkflowNodeTypes.IF_SUCCEED && !!context) {
        return true;
      }
      /** If Failed */
      else if (nodeType == WorkflowNodeTypes.IF_FAILED && !context) {
        return true;
      }
      /** Compare Variable */
      else if (nodeType == WorkflowNodeTypes.IF_NOT_EQUALS) {
        let value = processingNode.data.fields.value?.value;

        if (!value) {
          /** Fetch Variable */
          const { result: variableDoc } = await this._variable.fetchItemByName(processingNode.data.fields?.variable_name?.value);
          value = variableDoc?.variable_data;
          if (variableDoc?.variable_type == FieldTypes.JSON)
            value = JSON.parse(value);
          else if (variableDoc?.variable_type == FieldTypes.NUMBER)
            value = Number(value);
        }

        /** If both undefined */
        if (!context && !value) {
          return false;
        }
        else if (typeof value == 'number' || typeof context == 'number') {
          return Number(context) != Number(value);
        }
        else {
          return !isEqual(context, value);
        }
      }
      /** Compare Variable */
      else if (nodeType == WorkflowNodeTypes.IF_VARIABLE_EQUALS) {
        let value = processingNode.data.fields.value?.value;

        if (!value) {
          /** Fetch Variable */
          const { result: variableDoc } = await this._variable.fetchItemByName(processingNode.data.fields?.variable_name?.value);
          value = variableDoc?.variable_data;
          if (variableDoc?.variable_type == FieldTypes.JSON)
            value = JSON.parse(value);
          else if (variableDoc?.variable_type == FieldTypes.NUMBER)
            value = Number(value);
        }


        /** If both undefined */
        if (!context && !value) {
          return true;
        }
        else if (typeof value == 'number' || typeof context == 'number') {
          return Number(context) == Number(value);
        }
        else {
          return isEqual(context, value);
        }
      }

      return false;
    } catch (error) {
      this.logger.error(error);
      return false;
    }
  }

  /**
   * Process Node v1.0
   * 
   * @param payload 
   */
  async processNode(payload: {
    processingNode : WorkflowNode,
    workflowNodes  : WorkflowNode[],
    workflow       : LeanDocument<WorkflowDocument>,
    apiUrl         : string,
    context       ?: any
  }) {
    /** Initial Constants */
    const {
      processingNode,
      workflowNodes,
      workflow,
      apiUrl,
      context
    } = payload;
    if (process.env.DEBUG_MODE)
      this.logger.debug(`Processing Action ${processingNode.data.type}`)

    const nodeData = processingNode.data.fields;

    try {

      /** HTTP Request Action */
      if (processingNode.data.type == WorkflowNodeTypes.SEND_HTTP_REQUEST) {
        /** Initial Variable */
        let data;

        /** Try sending request */
        try {
          const method   = await this._systemVariables.replaceVariables(nodeData.method?.value   || "get" , context) as any;
          const url      = await this._systemVariables.replaceVariables(nodeData.endpoint?.value || "/"   , context);
          const headers  = await this._systemVariables.replaceVariables(nodeData.headers?.value  || "{}"  , context);
          const body     = await this._systemVariables.replaceVariables(nodeData.body?.value     || "{}"  , context);

          const response = await this._http.request({
            method,
            url,
            headers : JSON.parse(headers),
            data    : method.toLowerCase() != 'get' ? JSON.parse(body) : null
          }).toPromise().catch(error => {
            throw error.response?.data;
          })
          data = response.data;
        } catch (error) {
          /** Retry if required */
          if (nodeData.retry?.value && !isNaN(Number(nodeData.retry?.value)) && Number(nodeData.retry?.value) >= 1) {
            let retry = Number(nodeData.retry?.value);
            while(retry) {
              if (process.env.DEBUG_MODE)
                this.logger.debug(`${nodeData.endpoint.value} ${nodeData.method.value} failed, awaiting retry ${nodeData.retry.value - retry + 1}/${nodeData.retry.value}...`);
              await new Promise((res, rej) => {
                setTimeout(() => {
                  res(1);
                }, Number(nodeData.retry_delay?.value || 0) * 1000);
              });
              if (process.env.DEBUG_MODE)
                this.logger.debug(`Retrying ${nodeData.endpoint.value} ${nodeData.method.value} ${nodeData.retry.value - retry + 1}/${nodeData.retry.value}...`);
              try {
                const { data: response } = await this._http.request({
                  method  : nodeData.method?.value || "get",
                  url     : nodeData.endpoint?.value || '/',
                  headers : JSON.parse(nodeData.headers?.value || "{}"),
                  data    : JSON.parse(nodeData.body?.value || "{}")
                }).toPromise().catch(error => {
                  throw error.response.data;
                })
                data  = response;
                retry = 0;
              } catch (error) {
                retry--;
                if (!retry) throw error;
              }
            }
          }
          /** Throw error if no retry */
          else throw error
        }

        if (nodeData.result_mapping?.value) {
          try {
            return this.getDataByPath(data, nodeData.result_mapping?.value);
          } catch (error) {
            this.logger.error(error);
            return data;
          }
        }
        return data;
      }

      /** Send Email Action */
      else if (processingNode.data.type == WorkflowNodeTypes.SEND_EMAIL) {
        const recipients = processingNode.data.fields?.recipients?.value?.split(",");
        if (!recipients?.length)
          throw {
            statusCode: HttpStatus.BAD_REQUEST,
            message: "Recipient not found"
          }

        let message: string = processingNode.data.fields?.message?.value
        let subject: string = processingNode.data.fields?.subject?.value || "Message Queue Workflow Message"
        if (!message)
          throw {
            statusCode: HttpStatus.BAD_REQUEST,
            message: "Messaging can not be empty"
          }

        /** Replace Variables */
        message = await this._systemVariables.replaceVariables(message, context);
        subject = await this._systemVariables.replaceVariables(subject, context);
        await Promise.all(recipients.map(email => this._systemVariables.replaceVariables(email, context)));

        /** Send Email */
        const result = await this._email.sendPlain({
          recipients,
          subject,
          message
        })
        return result
      }

      /** Sleep */
      else if (processingNode.data.type == WorkflowNodeTypes.DELAY_PROGRESS) {
        await new Promise(r => setTimeout(r, 1000 * Number(nodeData.seconds?.value || 0)))
        return context;
      }

      /** Set Context to Variable */
      else if (processingNode.data.type == WorkflowNodeTypes.SET_VARIABLE) {
        let variable_data = context;
        if (!processingNode.data.fields?.key?.value)
          throw {
            statusCode: HttpStatus.BAD_REQUEST,
            message: "Variable name not set"
          }

        /** Determine Variable Type */
        let variable_type: FieldTypes = processingNode.data.fields?.type?.value;
        
        if (typeof context == 'object') {
          !variable_type && (variable_type = FieldTypes.JSON);

          /** Clean Data by Given Path */
          if (processingNode.data.fields?.path?.value) {
            try {
              variable_data = this.getDataByPath(context, processingNode.data.fields?.path?.value);
              if (typeof variable_data == 'object')
                variable_data = JSON.stringify(variable_data);
            } catch (error) {
              this.logger.error(error, "[Clean Data by Given Path]");
            }
          }
          else {
            variable_data = JSON.stringify(context);
          }
        }
        else if (typeof context == 'number') {
          !variable_type && (variable_type = FieldTypes.NUMBER);
          variable_data = context?.toString();
        }

        /** Upsert variable by name */
        await this._variable.save({
          variable_name: processingNode.data.fields?.key.value,
          variable_type: variable_type || FieldTypes.STRING,
          variable_data
        })

        return context;
      }

      /** Start new workflow */
      else if (processingNode.data.type == WorkflowNodeTypes.START_WORKFLOW) {
        if (!processingNode.data.fields?.workflow?.value)
          throw {
            statusCode: HttpStatus.BAD_REQUEST,
            message: "Workflow name not set"
          }

        /** Get workflow id by name */
        const workflowDoc = await this._workflowModel.findOne({
          workflow_name: processingNode.data.fields?.workflow.value,
          is_active: true
        });

        if (!workflowDoc)
          throw {
            statusCode: HttpStatus.NOT_FOUND,
            message: `${processingNode.data.fields.workflow.value} workflow not found`
          }

        /** Trigger run workflow endpoint */
        console.info(workflow.workflow_bind_api_key);
        const { data } = await this._http.post(apiUrl + `/pdmq/workflows/${workflowDoc._id}/run`, {}, {
          headers: {
            'x-api-key': workflow.workflow_bind_api_key
          }
        }).toPromise();
        const executionDoc:LeanDocument<WorkflowExecutionDocument> = data.message.execution;

        /** Monitor Execution, Frequency = 5 second, Max Timeout = 1 hour */
        const lookupExecution = () => {
          return new Promise((res, rej) => {
            const executionLookupInterval = setInterval(() => {
              this.getWorkflowStatus(executionDoc._id).then(status => {
                if (status != WorkflowExecutionStatus.RUNNING) {
                  clearInterval(executionLookupInterval)
                  res(status);
                }
              })
              .catch(error => {
                clearInterval(executionLookupInterval)
                rej({
                  statusCode: HttpStatus.BAD_REQUEST,
                  message: error
                })
              })
            }, 5 * 1000);
          })
        }

        await lookupExecution();

        return context
      }
    } catch (error) {
      this.logger.error(error);
      if (nodeData?.result_mapping?.value) {
        try {
          throw this.getDataByPath(error, nodeData.result_mapping?.value);
        } catch (failed) {
          throw failed || error;
        }
      }
      else throw error;
    }
  }

  /**
   * Get Workflow Status
   * 
   * @param workflowId 
   */
  async getWorkflowStatus(executionId: string) {
    const processRunning = await this._workflowExecLogModel.exists({
      is_active: true,
      workflow_execution: new Types.ObjectId(executionId),
      process_completed_at: {
        $eq: null
      }
    })

    if (processRunning) return WorkflowExecutionStatus.RUNNING;
    else {
      const workflowDoc = await this._workflowExecModel.findById(executionId);
      return workflowDoc?.workflow_execution_terminated_at ? WorkflowExecutionStatus.TERMINATED : WorkflowExecutionStatus.COMPLETED;
    }
  }
}
