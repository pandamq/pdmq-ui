import { Test, TestingModule } from '@nestjs/testing';
import { WorkflowProcessNodeService } from './workflow-process-node.service';

describe('WorkflowProcessNodeService', () => {
  let service: WorkflowProcessNodeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WorkflowProcessNodeService],
    }).compile();

    service = module.get<WorkflowProcessNodeService>(WorkflowProcessNodeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
