import { Test, TestingModule } from '@nestjs/testing';
import { WorkflowCronService } from './workflow-cron.service';

describe('WorkflowCronService', () => {
  let service: WorkflowCronService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WorkflowCronService],
    }).compile();

    service = module.get<WorkflowCronService>(WorkflowCronService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
