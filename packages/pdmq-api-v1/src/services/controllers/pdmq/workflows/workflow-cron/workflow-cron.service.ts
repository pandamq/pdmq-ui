import { InjectModel } from '@nestjs/mongoose';
import { HttpService } from '@nestjs/axios';
import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { WorkflowCronJobInterface } from 'src/interface/workfow-cron-job.interface';
import { Workflow, WorkflowDocument } from 'src/schema/tbl_workflows.schema';
import { Model, Types } from 'mongoose';
import { CronJob, CronTime } from 'cron';


@Injectable()
export class WorkflowCronService {
  
  private workflowCornJobs: WorkflowCronJobInterface[] = [];
  private logger = new Logger(WorkflowCronService.name);

  constructor(
    @InjectModel(Workflow.name)
    private _workflowModel: Model<WorkflowDocument>,

    private _http: HttpService
  ) {
    this.initial();
  }

  /**
   * Initial Cron Jobs
   */
  async initial() {
    if (process.env.DEBUG_MODE)
      this.logger.debug("Initializing Workflow Cron Entry...");

    /** Get All Workflows with Cron Entry Point */
    const workflowDocs = await this._workflowModel.find({
      is_active: true,
      workflow_entry_point_cron: {
        $exists: true
      }
    });

    if (process.env.DEBUG_MODE)
      this.logger.debug(`Found ${workflowDocs.length} Workflows with Cron Expression`);

    workflowDocs.forEach(workflowDoc => {
      const job = new CronJob(workflowDoc.workflow_entry_point_cron, async () => {
        try {
          if (!workflowDoc?.workflow_bind_api_key)
            return;
          await this._http.request({
            headers: {
              'x-api-key': workflowDoc.workflow_bind_api_key
            },
            url: `${process.env.API_BASE_URL}/pdmq/workflows/${workflowDoc._id}/run`,
            method: 'post'
          }).toPromise();
        } catch (error) {
          this.logger.error(error.response?.data || error);
        }
      });

      job.start();

      this.workflowCornJobs.push({
        workflow_id: workflowDoc._id.toString(),
        workflow_cron_expression: workflowDoc.workflow_entry_point_cron,
        job
      })
    })
  }

  /**
   * Job List
   */
  fetch() {
    if (!this.workflowCornJobs.length) return [];
    return this._workflowModel.find({
      is_active: true,
      _id: {
        $in: this.workflowCornJobs.map(job => new Types.ObjectId(job.workflow_id))
      }
    }).then((workflows) => {
      return workflows.map(workflow => {
        const job = this.workflowCornJobs.find(job => job.workflow_id == workflow._id.toString());
        return {
          workflow: workflow.toJSON(),
          isRunning: job.job.running,
          nextRunTime: job.job.nextDate().format("DD MMM YYYY HH:mm:ss")
        }
      })
    })
  }

  /**
   * Remove job
   */
  remove(workflowId: string) {
    const jobIndex = this.workflowCornJobs.findIndex(job => job.workflow_id == workflowId);
    if (jobIndex != -1) {
      this.workflowCornJobs[jobIndex].job.stop();
      this.workflowCornJobs.splice(jobIndex, 1);
    }
  }

  /**
   * Update or insert job
   */
  upsert(workflow: WorkflowDocument) {
    /** Find existing job */
    const existingJob = this.workflowCornJobs.find(job => job.workflow_id == workflow._id?.toString());
    const cronTime = new CronTime(workflow.workflow_entry_point_cron);

    /** Update job */
    if (existingJob) {
      const isRunning = existingJob.job.running;
      isRunning && existingJob.job.stop();
      existingJob.job.setTime(cronTime);
      isRunning && existingJob.job.start();
      existingJob.workflow_cron_expression = workflow.workflow_entry_point_cron;
    }
    /** Put job into list */
    else {
      const job = new CronJob(workflow.workflow_entry_point_cron, async () => {
        try {
          const workflowDoc = await this._workflowModel.findOne({
            is_active: true,
            _id: workflow._id
          });

          if (!workflowDoc) {
            job.stop();
            this.workflowCornJobs = this.workflowCornJobs.filter(job => job.workflow_id != job.workflow_id);
          }
          if (!workflowDoc?.workflow_bind_api_key)
            return;

          await this._http.request({
            headers: {
              'x-api-key': workflowDoc.workflow_bind_api_key
            },
            url: `${process.env.API_BASE_URL}/pdmq/workflows/${workflowDoc._id}/run`,
            method: 'post'
          }).toPromise()
        } catch (error) {
          this.logger.error(error);
        }
      });

      job.start();
      this.workflowCornJobs.push({
        workflow_id: workflow._id?.toString(),
        workflow_cron_expression: workflow.workflow_entry_point_cron,
        job
      })
    }
  }

  /**
   * Toggle job status
   */
  toggleStatus(workflowId: string) {
    const job = this.workflowCornJobs.find(cron => cron.workflow_id == workflowId);
    if (job) {
      job.job.running ? job.job.stop() :job.job.start();
      return this._workflowModel.findOne({
        is_active: true,
        _id: new Types.ObjectId(job.workflow_id)
      }).then((workflow) => {
        return {
          workflow: workflow.toJSON(),
          isRunning: job.job.running,
          nextRunTime: job.job.nextDate().format("DD MMM YYYY HH:mm:ss")
        }
      })
    }
    else {
      throw {
        statusCode: HttpStatus.NOT_FOUND,
        message: "Cron job not found"
      }
    }

  }

}
