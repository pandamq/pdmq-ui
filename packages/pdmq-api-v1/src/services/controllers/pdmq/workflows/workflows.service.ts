import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, LeanDocument, Model, Types } from 'mongoose';
import { PaginationDto } from 'src/dto/common/pagination.dto';
import { SaveWorkflowDto } from 'src/dto/pdmq/save-workflow.dto';
import { Workflow, WorkflowDocument } from 'src/schema/tbl_workflows.schema';
import { WorkflowNode } from 'pdmq/dist/interfaces/workflow-node.interface';
import { WorkflowProcessNodeService } from './workflow-process-node/workflow-process-node.service';
import { WorkflowNodeCategories } from 'pdmq/dist/enums/workflow-node-categories.enum';
import { WorkflowExecution, WorkflowExecutionDocument } from 'src/schema/tbl_workflow_executions.schema';
import { WorkflowExecutionLog, WorkflowExecutionLogDocument } from 'src/schema/tbl_workflow_execution_logs.schema';
import { v1 } from 'uuid';
import { WorkflowCronService } from './workflow-cron/workflow-cron.service';
import { WorkflowStatusService } from 'src/services/gateways/workflow-status/workflow-status.service';
import { WorkflowExecutionStatus } from 'pdmq/dist/enums/workflow-execution-status.enum';

interface NodeConnection {
  node: string,
  output: string
}

@Injectable()
export class WorkflowsService {

  constructor(
    @InjectModel(Workflow.name)
    private _workflowModel: Model<WorkflowDocument>,

    @InjectModel(WorkflowExecution.name)
    private _workflowExecModel: Model<WorkflowExecutionDocument>,

    @InjectModel(WorkflowExecutionLog.name)
    private _workflowExecLogModel: Model<WorkflowExecutionLogDocument>,

    private _processing: WorkflowProcessNodeService,
    private _cron: WorkflowCronService,
    private _workflowStatus: WorkflowStatusService
  ) {}

  /**
   * Fetch single workflow
   * 
   * @param workflowId 
   */
  async fetchItem(workflowId: string) {
    const result = await this._workflowModel.findById(workflowId);
    return {
      result
    }
  }

  /**
   * Fetch All Workflows
   */
  async fetch(pagination: PaginationDto) {
    const query: FilterQuery<WorkflowDocument> = {
      is_active: true
    }
    if (pagination.searchKey) {
      query["$or"] = [
        {
          workflow_description: new RegExp(pagination.searchKey, 'i')
        },
        {
          workflow_name: new RegExp(pagination.searchKey, 'i')
        }
      ]
    }
    const results = [];
    if (pagination.index && pagination.size) {
      results.push(
        ...(
          await this._workflowModel
            .find(query)
            .skip(Number(pagination.index) * Number(pagination.size))
            .limit(Number(pagination.size))
            .then(res => res.map(doc => doc.toJSON()))
        )
      )
    } else {
      results.push(
        ...(
          await this._workflowModel
            .find(query)
            .then(res => res.map(doc => doc.toJSON()))
        )
      )
    }

    /**
     * Get Last Execution
     */
    if (results.length) {
      const execDocs = await this._workflowExecModel.find({
        is_active: true,
        workflow: {
          $in: results.map(result => result._id)
        }
      }).then(res => res.map(doc => doc.toJSON()));
  
      for (const result of results) {
        const execDoc = execDocs.find(doc => doc.workflow.toString() == result._id.toString());
        if (execDoc)
          result.last_execution = execDoc
      }
    }

    return {
      results,
      count: (await this._workflowModel.countDocuments(query))
    }
    
  }

  /**
   * Upsert Workflow
   * 
   * @param payload 
   */
  async save(payload: SaveWorkflowDto) {
    let result: WorkflowDocument;
    /** Update */
    if (payload._id) {
      await this._workflowModel.updateOne({
        _id: new Types.ObjectId(payload._id)
      }, payload);

      result = await this._workflowModel.findById(payload._id)
    }
    /** Create */
    else {
      result = await this._workflowModel.create(payload);
    }

    /** If cron enabled */
    if (result.workflow_entry_point_cron && result.workflow_bind_api_key) {
      this._cron.upsert(result)
    }

    return {
      result
    };
  }

  /**
   * Delete Workflow
   * 
   * @param workflowId 
   */
  async delete(workflowId: string) {
    await this._workflowModel.updateOne({
      _id: new Types.ObjectId(workflowId)
    }, {
      is_active: false
    })

    /** Unregister cron */
    this._cron.remove(workflowId);

    return workflowId;
  }

  /**
   * Stop workflow
   * 
   * @param workflowId 
   */
  async stop(workflowId: string) {
    /** Mark Execution as Stopped */
    await this._workflowExecModel.updateMany({
      workflow: new Types.ObjectId(workflowId),
      workflow_execution_terminated_at: {
        $eq: null
      }
    }, {
      workflow_execution_terminated_at: new Date()
    })
  }

  /**
   * Run workflow
   * 
   * @param workflowId
   */
  async run(workflowId: string, options: {
    apiUrl: string
  }) {
    const { apiUrl } = options;

    /** Check workflow is not running */
    const workflowStatus = await this._workflowStatus.getExecutionStatus(new Types.ObjectId(workflowId))
    if (workflowStatus == WorkflowExecutionStatus.RUNNING)
      throw {
        statusCode: HttpStatus.BAD_REQUEST,
        message: "Workflow is running."
      }

    /** Fetch Workflow Nodes */
    const workflowDoc = await this._workflowModel.findOne({
      is_active: true,
      _id: new Types.ObjectId(workflowId)
    }).then(doc => doc.toJSON());

    if (!workflowDoc)
      throw {
        statusCode: HttpStatus.NOT_FOUND,
        message: "Workflow not found"
      }
    
    /** Get Entry Point */
    const nodes: WorkflowNode[] = Object.values(workflowDoc.workflow_body);
    const entryPoint = nodes.find(node => node.name == "ENTRY_POINT");
    if (!entryPoint)
      throw {
        statusCode: HttpStatus.NOT_FOUND,
        message: "Entry point is missing."
      }
    
    /** Workflow Start... */
    const pendingProcessConnections: NodeConnection[] = (Object.values(entryPoint.outputs)[0] as { connections: NodeConnection[] })?.connections;

    /**
     * Create Execution Entry
     */
    const executionDoc = await this._workflowExecModel.create({
      workflow: workflowDoc._id,
      workflow_execution_started_at: new Date(),
      workflow_execution_logs: []
    })

    this.runParallel({
      connections: pendingProcessConnections,
      workflowNodes: nodes,
      workflow: workflowDoc,
      execId: executionDoc._id,
      apiUrl
    })

    return executionDoc.toJSON();
  }

  /**
   * Run multiple nodes
   */
  runParallel(payload: {
    connections   : NodeConnection[],
    workflowNodes : WorkflowNode[],
    workflow      : LeanDocument<WorkflowDocument>,
    execId        : Types.ObjectId,
    apiUrl        : string,
    context      ?: any,
  }) {
    const {
      connections,
      workflowNodes,
      workflow,
      execId,
      apiUrl,
      context
    } = payload;

    /** Find all nodes */
    const nodesPendingProcess = connections.map(connection => {
      return workflowNodes.find(node => node.id.toString() == connection.node.toString());
    }).filter(node => node);

    nodesPendingProcess.filter(node => { // Filter Disabled Node
      return !node.data?.fields?.disabled?.value || node.data?.fields?.disabled?.value == 'false' || node.data?.fields?.disabled?.value == '0'
    }).forEach(node => {
      this.processNode({
        processingNode: node,
        workflowNodes,
        workflow,
        execId,
        context,
        apiUrl: apiUrl
      })
    })
  }

  /**
   * Run Node
   * 
   * @param payload
   */ 
  async processNode(payload: {
    processingNode : WorkflowNode,
    workflowNodes  : WorkflowNode[],
    workflow       : LeanDocument<WorkflowDocument>,
    execId         : Types.ObjectId,
    apiUrl         : string,
    context       ?: any
  }) {
    const {
      processingNode,
      workflowNodes,
      workflow,
      context,
      apiUrl,
      execId
    } = payload;
      
    let res; // context for next layer
    let success;
    let pass = true;
    let logDoc: WorkflowExecutionLogDocument;

    try {
      /** Check execution has terminated or not */
      const terminated = await this._workflowExecModel.exists({
        _id: execId,
        workflow_execution_terminated_at: {
          $ne: null
        }
      });

      if (terminated) return;


      /** If node has two inputs, check it didn't ran already */
      if (processingNode.inputs.input_1?.connections?.length && processingNode.inputs.input_1?.connections?.length > 1) {
        const alreadyRan = await this._workflowExecLogModel.exists({
          is_active: true,
          node_id: processingNode.id,
          workflow_execution: execId
        })

        if (alreadyRan) return;
      }
        
      const individualExecutionLogId = v1();
      logDoc = await this._workflowExecLogModel.create({
        workflow               : workflow._id,
        process_started_at     : new Date(),
        workflow_execution     : execId,
        node_execution_context : context,
        node_execution_id      : individualExecutionLogId,
        node_id                : processingNode.id,
        node_type              : processingNode.data.type,
        node_category          : processingNode.data.category
      })

      await this._workflowExecModel.findByIdAndUpdate(execId, {
        $push: {
          workflow_execution_logs: {
            $each: [{
              log_id: logDoc._id
            }]
          }
        }
      })

      /** Process Action */
      if (processingNode.data.category == WorkflowNodeCategories.ACTION)
        res = await this._processing.processNode(payload);

      /** Process Condition */
      else if (processingNode.data.category == WorkflowNodeCategories.CONDITION) {
        pass = await this._processing.verifyCondition({
          processingNode, 
          context
        });
        res  = context;
      }

      /** Process Stored Task */
      else if (processingNode.data.category == WorkflowNodeCategories.TASK) {
        /** Put task to instant task queue */
        const taskExecutionResult = await this._processing.putTaskToQueue({
          processingNode
        })
        res = taskExecutionResult.res || taskExecutionResult.error;
        success = taskExecutionResult.success;
      }
      
      /**
       * Trigger Next Level
       */
      if (pass && processingNode.outputs) {
        const pendingProcessConnections: NodeConnection[] = (Object.values(processingNode.outputs)[0] as { connections: NodeConnection[] })?.connections;
        this.runParallel({
          connections: pendingProcessConnections,
          workflowNodes,
          workflow,
          execId,
          apiUrl,
          context: res
        })
      }
      success = pass;
    } catch (error) {
      /** Mark individual execution as failure */
      res = error;
      success = false;
      /** If ignore previous failure is true */
      if (workflow.workflow_ignore_failure && pass) {
        const pendingProcessConnections: NodeConnection[] = (Object.values(processingNode.outputs)[0] as { connections: NodeConnection[] })?.connections;
        this.runParallel({
          connections: pendingProcessConnections,
          workflowNodes,
          workflow,
          execId,
          apiUrl,
          context: res
        })
      }
    } finally {
      if (!logDoc) return;
      /** Log Result */
        await this._workflowExecLogModel.updateOne({
          _id: logDoc._id
        }, {
          process_completed_at: new Date(),
          node_execution_result: {
            success,
            result: res
          }
        });
      /** Update last completed date */
      const logIndex: number = await this._workflowExecModel.findById(execId).select(["workflow_execution_logs"]).then(doc => {
        return doc.workflow_execution_logs.findIndex(log => log.log_id.toString() == logDoc._id.toString());
      });
      await this._workflowExecModel.updateOne({
        _id: execId,
      }, {
        workflow_execution_completed_at: new Date(),
        $set: {
          [`workflow_execution_logs.${logIndex}.success`]: success
        }
      })
    }

  }
}
