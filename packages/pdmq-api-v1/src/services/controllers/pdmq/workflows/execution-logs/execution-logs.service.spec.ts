import { Test, TestingModule } from '@nestjs/testing';
import { ExecutionLogsService } from './execution-logs.service';

describe('ExecutionLogsService', () => {
  let service: ExecutionLogsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ExecutionLogsService],
    }).compile();

    service = module.get<ExecutionLogsService>(ExecutionLogsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
