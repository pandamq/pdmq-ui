import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model, Types } from 'mongoose';
import { PaginationDto } from 'src/dto/common/pagination.dto';
import { WorkflowExecutionLog, WorkflowExecutionLogDocument } from 'src/schema/tbl_workflow_execution_logs.schema';

@Injectable()
export class ExecutionLogsService {

  constructor(
    @InjectModel(WorkflowExecutionLog.name)
    private _workflowExecLogModel: Model<WorkflowExecutionLogDocument>,
  ) {}


  /**
   * Fetch All Workflows
   */
  async fetch(options: {
    pagination: PaginationDto,
    nodeId: string,
    workflowId: string
  }) {
    const nodeId     = Number(options.nodeId);
    const workflowId = new Types.ObjectId(options.workflowId);
    const pagination = options.pagination || {};

    const query: FilterQuery<WorkflowExecutionLogDocument> = {
      is_active : true,
      workflow  : workflowId,
      node_id   : nodeId
    }
    const results = [];
    if (pagination.index && pagination.size) {
      results.push(
        ...(
          await this._workflowExecLogModel
            .find(query, [], {

            })
            .skip(Number(pagination.index) * Number(pagination.size))
            .limit(Number(pagination.size))
            .sort({
              process_started_at: 'desc'
            })
        )
      )
    } else {
      results.push(
        ...(await this._workflowExecLogModel
          .find(query)
          .sort({
            process_started_at: 'desc'
          }))
      )
    }

    return {
      results,
      count: (await this._workflowExecLogModel.countDocuments(query))
    }
    
  }
}
