import { Injectable } from '@nestjs/common';
import { PDMQTask } from 'pdmq/dist/interfaces/task.interface';
import { SaveTaskDto } from 'src/dto/pdmq/save-task.dto';
import { TaskTriggerTypes } from 'src/enum/task-trigger-types.enum';
import { PdmqService } from 'src/services/common/pdmq/pdmq.service';

@Injectable()
export class TasksService {

  constructor(
    private _pdmq: PdmqService
  ) {}

  /**
   * Fetch All Tasks
   */
  async fetch(options: {
    from?: string,
    to  ?: string,
    type : TaskTriggerTypes
  }) {
    const tasks = [];
    if (options.type == TaskTriggerTypes.INSTANT) {
      tasks.push(...(await this._pdmq.client.findInstantTask()));
    }
    else if (options.type == TaskTriggerTypes.NEVER) {
      tasks.push(...(await this._pdmq.client.findStoredTask()));
    }
    else if (options.type == TaskTriggerTypes.ALL)
      tasks.push(...(await this._pdmq.client.findAllTasks()))
    else {
      tasks.push(...(await this._pdmq.client.findTask(
        options.from,
        options.to
      )));
    }

    return {
      results: tasks,
      count: tasks.length
    }
  }

  /**
   * Upsert Task
   * 
   * @param payload 
   */
  async save(payload: SaveTaskDto) {
    if (payload.task_id)
      await this.delete(payload.task_id);
    const result = await this._pdmq.client.addTask(payload as PDMQTask);
    return {
      result
    };
  }

  /**
   * Delete Task
   * 
   * @param taskId 
   */
  delete(taskId: string) {
    return this._pdmq.client.deleteTask(taskId);
  }

}
