import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MongooseMiddlewarePostInit } from 'src/fn/schema-hook.fn';
import { TaskExecution, TaskExecutionSchema } from 'src/schema/tbl_task_executions.schema';
import { Variable, VariableSchema } from 'src/schema/tbl_variables.schema';
import { Workflow, WorkflowSchema } from 'src/schema/tbl_workflows.schema';
import { WorkflowExecution, WorkflowExecutionSchema } from 'src/schema/tbl_workflow_executions.schema';
import { WorkflowExecutionLog, WorkflowExecutionLogSchema } from 'src/schema/tbl_workflow_execution_logs.schema';
import { CommonServicesModule } from 'src/services/common/common-services.module';
import { TasksService } from './tasks/tasks.service';
import { VariablesService } from './variables/variables.service';
import { WorkflowProcessNodeService } from './workflows/workflow-process-node/workflow-process-node.service';
import { WorkflowsService } from './workflows/workflows.service';
import { ExecutionLogsService } from './workflows/execution-logs/execution-logs.service';
import { AuthenticationServicesModule } from '../authentication/authentication-services.module';
import { WorkflowCronService } from './workflows/workflow-cron/workflow-cron.service';
import { WorkflowStatusService } from 'src/services/gateways/workflow-status/workflow-status.service';

@Module({
  providers: [
    TasksService,
    VariablesService,
    WorkflowsService,
    WorkflowProcessNodeService,
    ExecutionLogsService,
    WorkflowCronService,
    WorkflowStatusService
  ],
  imports: [
    HttpModule,
    AuthenticationServicesModule,
    CommonServicesModule,
    MongooseModule.forFeatureAsync([
      {
        name: Workflow.name,
        useFactory: MongooseMiddlewarePostInit(WorkflowSchema)
      },
      {
        name: Variable.name,
        useFactory: MongooseMiddlewarePostInit(VariableSchema)
      },
      {
        name: TaskExecution.name,
        useFactory: MongooseMiddlewarePostInit(TaskExecutionSchema)
      },
      {
        name: WorkflowExecution.name,
        useFactory: MongooseMiddlewarePostInit(WorkflowExecutionSchema)
      },
      {
        name: WorkflowExecutionLog.name,
        useFactory: MongooseMiddlewarePostInit(WorkflowExecutionLogSchema)
      }
    ])
  ],
  exports: [
    AuthenticationServicesModule,
    CommonServicesModule,
    TasksService,
    MongooseModule,
    WorkflowCronService,
    VariablesService,
    WorkflowsService,
    WorkflowProcessNodeService,
    ExecutionLogsService,
    WorkflowStatusService
  ]
})
export class PdmqServiceModule {}
