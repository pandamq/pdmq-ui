import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, LeanDocument, Model, Types } from 'mongoose';
import { FieldTypes } from 'pdmq/dist/enums/field-types.enum';
import { PaginationDto } from 'src/dto/common/pagination.dto';
import { SaveVariableDto } from 'src/dto/pdmq/save-variable.dto';
import { Variable, VariableDocument } from 'src/schema/tbl_variables.schema';

@Injectable()
export class VariablesService {

  constructor(
    @InjectModel(Variable.name)
    private _variableModel: Model<VariableDocument>
  ) {}

  /**
   * Fetch variable names
   */
  fetchNames() {
    return this._variableModel.find({
      is_active: true
    }).select([
      "variable_name"
    ]).then(docs => {
      return docs.map(doc => doc.variable_name);
    })
  }

  /** 
   * Fetch Variable Value
   */
  async fetchVariableValue(variableName: string): Promise<any> {
    const variableDoc = await this.fetchItemByName(variableName);
    if (!variableDoc.result) {
      throw {
        statusCode: HttpStatus.NOT_FOUND,
        message: "Variable not found, please double check the variable name."
      }
    }

    if (variableDoc.result.variable_type == FieldTypes.JSON) {
      try {
        return JSON.parse(variableDoc.result.variable_data);
      } catch (error) {
        return variableDoc.result.variable_data;
      }
    }
    
    if (variableDoc.result.variable_type == FieldTypes.NUMBER) {
      return Number(variableDoc.result.variable_data);
    }

    return variableDoc;
  }

  /**
   * Fetch variable by name
   * 
   * @param variableName 
   */
  fetchItemByName(variableName: string): Promise<{
    result: LeanDocument<VariableDocument>
  }> {
    return this._variableModel.findOne({
      variable_name: variableName,
      is_active: true
    }).then(doc => {
      return {
        result: doc?.toJSON()
      }
    });
  }

  /**
   * Fetch single variable
   * 
   * @param variableId 
   */
  async fetchItem(variableId: string) {
    const result = await this._variableModel.findById(variableId);
    return {
      result
    }
  }

  /**
   * Fetch All Variables
   */
  async fetch(pagination: PaginationDto) {
    const query: FilterQuery<VariableDocument> = {
      is_active: true
    }
    if (pagination.searchKey) {
      query["$or"] = [
        {
          variable_name: new RegExp(pagination.searchKey, 'i')
        },
        {
          variable_data: new RegExp(pagination.searchKey, 'i')
        }
      ]
    }
    const results = [];
    if (pagination.index && pagination.size) {
      results.push(
        ...(
          await this._variableModel.find(query).skip(Number(pagination.index) * Number(pagination.size)).limit(Number(pagination.size))
        )
      )
    } else {
      results.push(
        ...(await this._variableModel.find(query))
      )
    }

    return {
      results,
      count: (await this._variableModel.countDocuments(query))
    }
    
  }

  /**
   * Upsert Variable
   * 
   * @param payload 
   */
  async save(payload: SaveVariableDto) {
    const exitedDoc = await this._variableModel.findOne({
      is_active: true,
      variable_name: payload.variable_name
    });

    /** Update */
    if (exitedDoc?._id || payload._id) {
      await this._variableModel.updateOne({
        _id: new Types.ObjectId(exitedDoc._id || payload._id)
      }, {
        ...payload,
        revision: exitedDoc.revision
      });

      return {
        result: (await this._variableModel.findOne({
          variable_name: payload.variable_name
        }))
      }
    }
    /** Create */
    else {
      const result = await this._variableModel.create(payload);
      return {
        result
      }
    }
  }

  /**
   * Delete Variable
   * 
   * @param variableId 
   */
  async delete(variableId) {
    await this._variableModel.updateOne({
      _id: new Types.ObjectId(variableId)
    }, {
      is_active: false
    })
    return variableId;
  }
}
