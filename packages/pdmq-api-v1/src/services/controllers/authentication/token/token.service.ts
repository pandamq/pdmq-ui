import { HttpStatus } from '@nestjs/common';
import { Injectable } from '@nestjs/common';

import * as path    from 'path';
import * as jwt     from 'jsonwebtoken';
import * as md5     from 'md5';
import * as fs      from 'fs';
import * as shortid from 'shortid';
import { cloneDeep, flatMap, uniqBy } from 'lodash';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, LeanDocument, Model, Types } from 'mongoose';
import { Login, LoginDocument } from 'src/schema/tbl_logins.schema';
import { Resource, ResourceDocument } from 'src/schema/tbl_resources.schema';
import { VerifyTokenDto } from 'src/dto/authentication/verify-token.dto';

@Injectable()
export class TokenService {

  constructor(
    @InjectModel(Login.name)
    private _loginModel: Model<LoginDocument>,

    @InjectModel(Resource.name)
    private _resourceModel: Model<ResourceDocument>
  ) {}

  /**
   * Sign token
   * 
   * @param loginDoc 
   * @returns 
   */
  async sign(loginDoc: LeanDocument<LoginDocument>) {
    const user = loginDoc;
    const nonce_str = shortid();
    const sign = md5(`id=${user._id}&nonce_str=${nonce_str}&email=${user.email}`);

    const key = fs.readFileSync(
      path.join(__dirname, '../../../../', 'keystore', 'private.key'), 
      'utf-8'
    );
    const token = jwt.sign({
      _id: user._id
    }, key, {algorithm: "RS512", expiresIn: '1h'});
    const refreshToken = jwt.sign({
      nonce_str,
      sign
    }, key, {algorithm: "RS512", expiresIn: '120d'});

    return {token, refreshToken};
  }

  /**
   * Verify Token
   * 
   * @param token 
   * @param refreshToken 
   */
  async loadUserFromToken(
    payload: VerifyTokenDto
  ) {
    let tokenPayload: {_id: string}, 
        refreshTokenPayload: {sign: string, nonce_str: string};
    try {    
      /** Verify Token Provided */
      tokenPayload = this.verifyToken(payload.token);
      refreshTokenPayload = this.verifyToken(payload.refreshToken);
    } catch (error) {
      /** Try refresh token */
      refreshTokenPayload = this.verifyToken(payload.refreshToken);
      tokenPayload = jwt.decode(payload.token) as any;
    }

    if (!tokenPayload || !refreshTokenPayload)
      throw {
        statusCode: HttpStatus.FORBIDDEN,
        message: "Token Expired"
      }
    

    /** Find Login */
    const query: FilterQuery<LoginDocument> = {
      is_active: true,
      _id: new Types.ObjectId(tokenPayload._id)
    };

    const loginDoc = await this._loginModel.findOne(query).populate("roles").then(doc => doc?.toJSON());

    if (!loginDoc)
      throw {
        statusCode: HttpStatus.NOT_FOUND,
        message   : "Invalid username or password."
      }

    else if (!loginDoc.roles?.length)
      throw {
        statusCode: HttpStatus.NOT_FOUND,
        message   : "This login has no role attached."
      }
    
    /** Verify Key Pair */
    const sign = md5(`id=${tokenPayload._id}&nonce_str=${refreshTokenPayload.nonce_str}&email=${loginDoc.email}`);
    if (sign != refreshTokenPayload.sign)
      throw {
        statusCode: HttpStatus.FORBIDDEN,
        message: "Key pair mismatch"
      }

    /** Populate resources */
    const resourceIds = uniqBy(
      flatMap(loginDoc.roles, (role: any) => {
        return role?.resources.filter(item => item.can_view)
      }),
      (item) => item.resource
    );

    if (!loginDoc.roles.length) {
      throw {
        statusCode: HttpStatus.FORBIDDEN,
        message   : "This login has no resource attached, please contact HR for login permission."
      }
    }

    if (!resourceIds.length) {
      throw {
        statusCode: HttpStatus.FORBIDDEN,
        message   : "This login has no resource attached, please contact HR for login permission."
      }
    }

    const resources = await this._resourceModel.find({
      is_active: true,
      _id: {
        $in: resourceIds.map(resource => resource.resource)
      }
    }).then();

    if (!resources.length) {
      throw {
        statusCode: HttpStatus.FORBIDDEN,
        message   : "This login has no resource attached, please contact HR for login permission."
      }
    }

    const login = cloneDeep(loginDoc) as LeanDocument<LoginDocument>;
    delete login.password;
    const tokens = await this.sign(loginDoc);
    return {
      tokens,
      login,
      roles: login.roles,
      resources: resources.map(resource => {
        const resourceId = resource._id.toString();
        const permissions = resourceIds.find(r => r.resource.toString() == resourceId);
        return {
          ...resource.toJSON(),
          can_edit    : !!permissions?.can_edit,
          can_view    : !!permissions?.can_view,
          can_create  : !!permissions?.can_create,
          can_delete  : !!permissions?.can_delete
        }
      })
    }
  }

  /**
   * Refresh Token
   * 
   * @param token 
   * @param refreshToken 
   */
  async refreshToken(
    token       : string, 
    refreshToken: string
  ) {
    /** Verify Token Pair */
    try {
      /** Decode tokens */
      const tokenPayload       : any = jwt.decode(token);
      const refreshTokenPayload: any = this.verifyToken(refreshToken);

      const loginDoc = await this._loginModel.findOne({
        is_active: true,
        _id: new Types.ObjectId(tokenPayload._id)
      }).then();

      if (!loginDoc)
        throw {
          statusCode: HttpStatus.FORBIDDEN,
          message: "Login not found"
        }

      /** Check signature */
      const sign = md5(`id=${tokenPayload._id}&nonce_str=${refreshTokenPayload.nonce_str}&email=${loginDoc.email}`);
      if (sign != refreshTokenPayload.sign)
        throw {
          statusCode: HttpStatus.FORBIDDEN,
          message: "Invalid signature"
        }
      
      return this.sign(loginDoc)
    } catch (error) {
      throw {
        statusCode: HttpStatus.FORBIDDEN,
        message   : error.message || "Invalid token pair"
      }
    }
  }

  /** 
   * Verify token
   */
  verifyToken(
    token :string
  ): any {
    const tokenPayload = jwt.verify(
      token, 
      fs.readFileSync(
        path.join(__dirname, '../../../../', 'keystore', 'public.key'),
        "utf8"
      )
    );
    return tokenPayload;
  }
}
