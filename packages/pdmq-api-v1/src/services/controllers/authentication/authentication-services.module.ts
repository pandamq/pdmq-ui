import { Module } from '@nestjs/common';
import { LoginService } from './login/login.service';
import { AccessKeyService } from './access-key/access-key.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Login, LoginSchema } from 'src/schema/tbl_logins.schema';
import { MongooseMiddlewarePostInit } from 'src/fn/schema-hook.fn';
import { Role, RoleSchema } from 'src/schema/tbl_roles.schema';
import { Resource, ResourceSchema } from 'src/schema/tbl_resources.schema';
import { TokenService } from './token/token.service';
import { AccessKey, AccessKeySchema } from 'src/schema/tbl_access_keys.shcema';
import { RequestLog, RequestLogSchema } from 'src/schema/tbl_request_logs.schema';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: Login.name,
        useFactory: MongooseMiddlewarePostInit(LoginSchema)
      },
      {
        name: Role.name,
        useFactory: MongooseMiddlewarePostInit(RoleSchema)
      },
      {
        name: Resource.name,
        useFactory: MongooseMiddlewarePostInit(ResourceSchema)
      },
      {
        name: AccessKey.name,
        useFactory: MongooseMiddlewarePostInit(AccessKeySchema)
      },
      {
        name: RequestLog.name,
        useFactory: MongooseMiddlewarePostInit(RequestLogSchema)
      }
    ])
  ],
  providers: [
    LoginService,
    AccessKeyService,
    TokenService
  ],
  exports: [
    LoginService,
    AccessKeyService,
    TokenService
  ]
})
export class AuthenticationServicesModule {}
