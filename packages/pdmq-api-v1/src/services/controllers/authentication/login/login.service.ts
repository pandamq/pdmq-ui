import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, LeanDocument, Model } from 'mongoose';
import { LoginDto } from 'src/dto/authentication/login.dto';
import { Login, LoginDocument } from 'src/schema/tbl_logins.schema';
import { Role, RoleDocument } from 'src/schema/tbl_roles.schema';
import { uniqBy, flatMap, cloneDeep } from 'lodash';
import { TokenService } from '../token/token.service';
import { Resource, ResourceDocument } from 'src/schema/tbl_resources.schema';
import { ClientTypes } from 'src/enum/client-types.enum';
import { SystemResources } from 'src/enum/system/system-resources.enum';
import { SystemRoles } from 'src/enum/system/system-roles.enum';
import * as md5 from 'md5';

@Injectable()
export class LoginService {

  private logger = new Logger(LoginService.name);
 
  constructor(
    private _token: TokenService,
    
    @InjectModel(Resource.name)
    private _resourceModel: Model<ResourceDocument>,
    
    @InjectModel(Role.name)
    private _roleModel: Model<RoleDocument>,

    @InjectModel(Login.name)
    private _loginModel: Model<LoginDocument>
  ) {
    this.init();
  }

  /**
   * Initial Default Account, Role and Resource
   */
  async init() {
    try {
      const defaultEmail    = process.env.DEFAULT_EMAIL    || 'admin@example.com',
            defaultPassword = process.env.DEFAULT_PASSWORD || 'example';

      /** Verify Default User Exists */
      const userExists = await this._loginModel.exists({
        email: defaultEmail,
        is_active: true
      });
      if (userExists) return;

      /** Create Admin Resource */
      const adminResource = await this._resourceModel.findOne({
        resource_name: SystemResources.ADMINISTRATOR
      }).then(adminResource => {
        return adminResource || this._resourceModel.create({
          resource_name : SystemResources.ADMINISTRATOR,
          has_view      : true,
          has_edit      : true,
          has_create    : true,
          has_delete    : true,
          is_system     : true
        })
      });

      /** Create Admin Role */
      const adminRole = await this._roleModel.findOne({
        resource_name: SystemRoles.ADMINISTRATOR
      }).then(adminRole => {
        return adminRole || this._roleModel.create({
          role_icon: "admin_panel_settings",
          role_name: SystemRoles.ADMINISTRATOR,
          is_system: true,
          resources: [{
            resource  : adminResource._id,
            can_view  : true,
            can_create: true,
            can_edit  : true,
            can_delete: true
          }]
        })
      });

      /** Create User */
      await this._loginModel.create({
        email     : defaultEmail,
        password  : md5(defaultPassword),
        roles     : [adminRole._id],
        is_system : true
      });

      this.logger.verbose(`Super User Created: ${defaultEmail} | ${defaultPassword}`);

    } catch (error) {
      this.logger.error(error);
    }
  }

  /**
   * Dashboard User Login
   * 
   * @param payload 
   */
  async login(payload: LoginDto) {
    if (!payload.email || (!payload.password))
      throw {
        statusCode: HttpStatus.BAD_REQUEST,
        message   : "Missing input."
      }
    /** Find Login */
    const query: FilterQuery<LoginDocument> = {
      is_active: true,
      email: payload.email,
      password: md5(payload.password)
    };

    const loginDoc = await this._loginModel.findOne(query).populate("roles").then(doc => doc?.toJSON());

    if (!loginDoc)
      throw {
        statusCode: HttpStatus.NOT_FOUND,
        message   : "Invalid username or password."
      }

    else if (!loginDoc.roles?.length)
      throw {
        statusCode: HttpStatus.NOT_FOUND,
        message   : "This login has no role attached."
      }

    else if (loginDoc.dashboard_restricted && payload.client_type == ClientTypes.DASHBOARD) {
      throw {
        statusCode: HttpStatus.FORBIDDEN,
        message   : "Dashboard access restricted, please contact your project admin to enable your access."
      }
    }
    
    /** Populate resources */
    const resourceIds = uniqBy(
      flatMap(loginDoc.roles, (role: any) => {
        return role?.resources.filter(item => item.can_view)
      }),
      (item) => item.resource
    );

    if (!loginDoc.roles.length) {
      throw {
        statusCode: HttpStatus.FORBIDDEN,
        message   : "This login has no resource attached, please contact HR for login permission."
      }
    }

    if (!resourceIds.length) {
      throw {
        statusCode: HttpStatus.FORBIDDEN,
        message   : "This login has no resource attached, please contact HR for login permission."
      }
    }

    const resources = await this._resourceModel.find({
      is_active: true,
      _id: {
        $in: resourceIds.map(resource => resource.resource)
      }
    }).then();

    if (!resources.length) {
      throw {
        statusCode: HttpStatus.FORBIDDEN,
        message   : "This login has no resource attached, please contact HR for login permission."
      }
    }

    const login     = cloneDeep(loginDoc) as LeanDocument<LoginDocument>;
    delete login.password;
    const tokens = await this._token.sign(loginDoc);
    return {
      tokens,
      login,
      roles: login.roles,
      resources: resources.map(resource => {
        const resourceId = resource._id.toString();
        const permissions = resourceIds.find(r => r.resource.toString() == resourceId);
        return {
          ...resource.toJSON(),
          can_edit    : !!permissions?.can_edit,
          can_view    : !!permissions?.can_view,
          can_create  : !!permissions?.can_create,
          can_delete  : !!permissions?.can_delete
        }
      })
    }
  }

}
