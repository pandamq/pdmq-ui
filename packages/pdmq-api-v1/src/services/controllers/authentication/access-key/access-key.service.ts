import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { AccessKey, AccessKeyDocument } from 'src/schema/tbl_access_keys.shcema';
import { LoginDocument } from 'src/schema/tbl_logins.schema';
import { RequestLog, RequestLogDocument } from 'src/schema/tbl_request_logs.schema';
import { v1 } from 'uuid';

@Injectable()
export class AccessKeyService {

  constructor(
    @InjectModel(AccessKey.name)
    private _accessKeyModel: Model<AccessKeyDocument>,

    @InjectModel(RequestLog.name)
    private _requestLogModel: Model<RequestLogDocument>
  ) {}

  /**
   * Verify Api Key
   * 
   * @param apiKey 
   */
  async verify(apiKey: string) {
    const keyDoc = await this._accessKeyModel.findOne({
      is_active: true,
      key: apiKey
    }).populate([
      {
        path: 'login'
      }
    ]);

    if (!keyDoc?.login)
      throw {
        statusCode: HttpStatus.FORBIDDEN,
        message: "API Key is Invalid"
      }

    const loginDoc: LoginDocument = keyDoc.login as any;

    if (loginDoc.api_restricted) {
      throw {
        statusCode: HttpStatus.FORBIDDEN,
        message: "API Access is Disabled"
      }
    }
    return loginDoc.toJSON();
  }

  /**
   * Generate / Rotate Key
   * 
   * @param userId
   */
  async generate(userId: string) {
    const login = new Types.ObjectId(userId);
    await this._accessKeyModel.updateMany({
      login,
      is_active: true
    }, {
      is_active: false
    });

    const key = v1();
    await this._accessKeyModel.create({
      login,
      key
    })

    return this.summary(userId);
  }

  /**
   * Get information of access key
   * 
   * @param userId 
   */
  async summary(userId: string) {
    const login = new Types.ObjectId(userId);

    const keyDoc = await this._accessKeyModel.findOne({
      login,
      is_active: true
    }).sort({
      created_at: -1
    })

    if (!keyDoc)
      return {}

    const logDoc = await this._requestLogModel
    .findOne({
      key: keyDoc.key,
      is_active: true
    })
    .sort({
      created_at: -1
    });

    return {
      key: keyDoc.key,
      path: logDoc?.path || "",
      method: logDoc?.method || "",
      lastAccessedAt: logDoc?.created_at || ""
    }
  }

}
