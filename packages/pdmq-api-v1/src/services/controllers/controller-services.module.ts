import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MongooseMiddlewarePostInit } from 'src/fn/schema-hook.fn';
import { SystemSetting, SystemSettingSchema } from 'src/schema/tbl_system_settings.schema';
import { AuthenticationServicesModule } from './authentication/authentication-services.module';
import { PdmqServiceModule } from './pdmq/pdmq-services.module';
import { SettingsService } from './system/settings/settings.service';
import { UsersService } from './system/permissions/users/users.service';
import { ResourcesService } from './system/permissions/resources/resources.service';
import { RolesService } from './system/permissions/roles/roles.service';
import { Login, LoginSchema } from 'src/schema/tbl_logins.schema';
import { Role, RoleSchema } from 'src/schema/tbl_roles.schema';
import { Resource, ResourceSchema } from 'src/schema/tbl_resources.schema';

@Module({
  imports: [
    AuthenticationServicesModule,
    PdmqServiceModule,
    MongooseModule.forFeatureAsync([
      {
        name: SystemSetting.name,
        useFactory: MongooseMiddlewarePostInit(SystemSettingSchema)
      },
      {
        name: Login.name,
        useFactory: MongooseMiddlewarePostInit(LoginSchema)
      },
      {
        name: Role.name,
        useFactory: MongooseMiddlewarePostInit(RoleSchema)
      },
      {
        name: Resource.name,
        useFactory: MongooseMiddlewarePostInit(ResourceSchema)
      },
    ])
  ],
  providers: [
    SettingsService,
    UsersService,
    ResourcesService,
    RolesService
  ],
  exports: [
    SettingsService,
    UsersService,
    ResourcesService,
    RolesService
  ]
})
export class ControllerServicesModule {}
