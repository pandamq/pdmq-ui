import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { SaveSystemSettingDto } from 'src/dto/system/save-system-setting.dto';
import { SystemSettings } from 'src/enum/system/system-settings.enum';
import { SystemSetting, SystemSettingDocument } from 'src/schema/tbl_system_settings.schema';

@Injectable()
export class SettingsService {

  constructor(
    @InjectModel(SystemSetting.name)
    private _systemSettingModel: Model<SystemSettingDocument>
  ) {}
  
  /**
   * Fetch by name
   * 
   * @param name 
   */
  fetchByName(name: SystemSettings) {
    return this._systemSettingModel.findOne({
      is_active: true,
      system_setting_name: name
    })
  }
  
  /**
   * Fetch
   */
  fetch() {
    return this._systemSettingModel.find({
      is_active: true
    }).then(res => ({
      results: res.map(doc => doc.toJSON()),
      count: res.length
    }))
  }

  /**
   * Save Setting
   */
  async save(payload: SaveSystemSettingDto) {
    const existedDoc = await this._systemSettingModel.findOne({
      is_active: true,
      system_setting_name: payload.system_setting_name
    });

    if (existedDoc) {
      await this._systemSettingModel.updateOne({
        _id: existedDoc._id
      }, payload)
    } else{
      await this._systemSettingModel.create({
        is_active: true,
        ...payload
      })
    }

    return this._systemSettingModel.findOne({
      is_active: true,
      system_setting_name: payload.system_setting_name
    })
  }
}
