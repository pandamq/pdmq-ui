import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model, Types } from 'mongoose';
import { PaginationDto } from 'src/dto/common/pagination.dto';
import { SaveUserDto } from 'src/dto/system/save-user.dto';
import { Login, LoginDocument } from 'src/schema/tbl_logins.schema';

@Injectable()
export class UsersService {

  constructor(
    @InjectModel(Login.name)
    private _loginModel: Model<LoginDocument>
  ) {

  }

  /**
   * Fetch user list
   * 
   * @param pagination 
   */
  async fetch(pagination: PaginationDto) {
    const query: FilterQuery<LoginDocument> = {
      is_active: true
    }
    if (pagination.searchKey) 
      query.email = new RegExp(pagination.searchKey, 'i')

    const results = [];
    if (pagination.index && pagination.size) {
      results.push(
        ...(
          await this._loginModel
            .find(query)
            .sort({ email: 1 })
            .skip(Number(pagination.index) * Number(pagination.size))
            .limit(Number(pagination.size))
        )
      )
    } else {
      results.push(
        ...(
          await this._loginModel
            .find(query)
            .sort({ email: -1 })
        )
      )
    }

    return {
      results,
      count: (await this._loginModel.countDocuments(query))
    }
  }

  /**
   * Save user
   * 
   * @param payload 
   */
  async save(payload: SaveUserDto) {
    const exitedDoc = await this._loginModel.findOne({
      is_active: true,
      email: payload.email
    });

    /** Update */
    if (exitedDoc?._id || payload._id) {
      await this._loginModel.updateOne({
        _id: new Types.ObjectId(exitedDoc._id || payload._id)
      }, payload);

      return {
        result: (await this._loginModel.findById(exitedDoc?._id || payload._id))
      }
    }
    /** Create */
    else {
      const result = await this._loginModel.create(payload);
      return {
        result
      }
    }
  }

}
