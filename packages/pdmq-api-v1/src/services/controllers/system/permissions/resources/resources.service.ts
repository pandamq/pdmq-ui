import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model, Types } from 'mongoose';
import { PaginationDto } from 'src/dto/common/pagination.dto';
import { SaveResourceDto } from 'src/dto/system/save-resource.dto';
import { Resource, ResourceDocument } from 'src/schema/tbl_resources.schema';

@Injectable()
export class ResourcesService {

  constructor(
    @InjectModel(Resource.name)
    private _resourceModel: Model<ResourceDocument>
  ) {

  }

  /**
   * Fetch resource list
   */
  async fetchSummary() {
    const resourceDocs = await this._resourceModel
    .find({
      is_active: true
    })
    .sort({ resource_name: 1 });
    return {
      results: resourceDocs,
      count: resourceDocs.length
    }
  }

  /**
   * Fetch resource list
   * 
   * @param pagination 
   */
  async fetch(pagination: PaginationDto) {
    const query: FilterQuery<ResourceDocument> = {
      is_active: true
    }
    if (pagination.searchKey) 
      query.resource_name = new RegExp(pagination.searchKey, 'i')

    const results = [];
    if (pagination.index && pagination.size) {
      results.push(
        ...(
          await this._resourceModel
            .find(query)
            .sort({ resource_name: 1 })
            .skip(Number(pagination.index) * Number(pagination.size))
            .limit(Number(pagination.size))
        )
      )
    } else {
      results.push(
        ...(
          await this._resourceModel
            .find(query)
            .sort({ resource_name: 1 })
        )
      )
    }

    return {
      results,
      count: (await this._resourceModel.countDocuments(query))
    }
  }

  /**
   * Save resource
   * 
   * @param payload 
   */
  async save(payload: SaveResourceDto) {
    const exitedDoc = await this._resourceModel.findOne({
      is_active: true,
      resource_name: payload.resource_name
    });

    /** Update */
    if (exitedDoc?._id || payload._id) {
      await this._resourceModel.updateOne({
        _id: new Types.ObjectId(exitedDoc._id || payload._id)
      }, payload);

      return {
        result: (await this._resourceModel.findById(exitedDoc?._id || payload._id))
      }
    }
    /** Create */
    else {
      const result = await this._resourceModel.create(payload);
      return {
        result
      }
    }
  }
}
