import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model, Types } from 'mongoose';
import { PaginationDto } from 'src/dto/common/pagination.dto';
import { SaveRoleDto } from 'src/dto/system/save-role.dto';
import { RoleDocument } from 'src/schema/tbl_roles.schema';
import { Role } from 'src/schema/tbl_roles.schema';

@Injectable()
export class RolesService {

  constructor(
    @InjectModel(Role.name)
    private _roleModel: Model<RoleDocument>
  ) {

  }

  /**
   * Fetch resource list
   */
  async fetchSummary() {
    const resourceDocs = await this._roleModel
    .find({
      is_active: true
    })
    .sort({ role_name: 1 });
    return {
      results: resourceDocs,
      count: resourceDocs.length
    }
  }

  /**
   * Fetch role list
   * 
   * @param pagination 
   */
  async fetch(pagination: PaginationDto) {
    const query: FilterQuery<RoleDocument> = {
      is_active: true
    }
    if (pagination.searchKey) 
      query.role_name = new RegExp(pagination.searchKey, 'i')

    const results = [];
    if (pagination.index && pagination.size) {
      results.push(
        ...(
          await this._roleModel
            .find(query)
            .sort({ role_name: 1 })
            .skip(Number(pagination.index) * Number(pagination.size))
            .limit(Number(pagination.size))
        )
      )
    } else {
      results.push(
        ...(
          await this._roleModel
            .find(query)
            .sort({ role_name: 1 })
        )
      )
    }

    return {
      results,
      count: (await this._roleModel.countDocuments(query))
    }
  }

  /**
   * Save role
   * 
   * @param payload 
   */
  async save(payload: SaveRoleDto) {
    const exitedDoc = await this._roleModel.findOne({
      is_active: true,
      role_name: payload.role_name
    });

    /** Update */
    if (exitedDoc?._id || payload._id) {
      await this._roleModel.updateOne({
        _id: new Types.ObjectId(exitedDoc._id || payload._id)
      }, payload);

      return {
        result: (await this._roleModel.findById(exitedDoc?._id || payload._id))
      }
    }
    /** Create */
    else {
      const result = await this._roleModel.create(payload);
      return {
        result
      }
    }
  }

}
