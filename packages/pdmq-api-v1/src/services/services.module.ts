import { Module } from '@nestjs/common';
import { ControllerServicesModule } from './controllers/controller-services.module';
import { CommonServicesModule } from './common/common-services.module';
import { GatewayServicesModule } from './gateways/gateways.module';
import { AuthenticationServicesModule } from './controllers/authentication/authentication-services.module';

@Module({
  exports: [
    ControllerServicesModule, 
    CommonServicesModule,
    AuthenticationServicesModule
  ],
  imports: [
    ControllerServicesModule, 
    CommonServicesModule, 
    GatewayServicesModule,
    AuthenticationServicesModule
  ],
  providers: []
})
export class ServicesModule {}
