import { Test, TestingModule } from '@nestjs/testing';
import { WorkflowStatusService } from './workflow-status.service';

describe('WorkflowStatusService', () => {
  let service: WorkflowStatusService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WorkflowStatusService],
    }).compile();

    service = module.get<WorkflowStatusService>(WorkflowStatusService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
