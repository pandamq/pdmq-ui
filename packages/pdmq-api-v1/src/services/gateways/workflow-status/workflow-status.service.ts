import { Injectable } from '@nestjs/common';
import { Socket } from 'dgram';
import { LeanDocument, Model, Types } from 'mongoose';
import { Workflow, WorkflowDocument } from 'src/schema/tbl_workflows.schema';
import { WorkflowExecution, WorkflowExecutionDocument } from 'src/schema/tbl_workflow_executions.schema';
import { SocketGateways } from 'src/enum/sockets/socket-gateways.enum';
import { InjectModel } from '@nestjs/mongoose';
import { WorkflowExecutionLog, WorkflowExecutionLogDocument } from 'src/schema/tbl_workflow_execution_logs.schema';
import { WorkflowExecutionStatus } from "pdmq/dist/enums/workflow-execution-status.enum"

@Injectable()
export class WorkflowStatusService {

  constructor(
    @InjectModel(Workflow.name)
    private _workflowModel: Model<WorkflowDocument>,

    @InjectModel(WorkflowExecution.name)
    private _workflowExecModel: Model<WorkflowExecutionDocument>,

    @InjectModel(WorkflowExecutionLog.name)
    private _workflowExecLogModel: Model<WorkflowExecutionLogDocument>
  ) {

  }

  /**
   * Get workflow status
   * 
   * @param payload 
   */
  async getWorkflowStatus(payload: {
    workflowId: Types.ObjectId,
    client: Socket,
    promise?: Promise<{
      logs?: LeanDocument<WorkflowExecutionLogDocument>[];
      execution?: LeanDocument<WorkflowExecutionDocument>;
    }>
  }) {
    const {
      workflowId,
      client
    } = payload

    /** Get The Latest Execution */
    if (!payload.promise)
      payload.promise = this.getExecutionDetail(workflowId);

    const executionDetail = await payload.promise;

    payload.promise = null;

    if (!executionDetail.execution) {
      client.emit(SocketGateways.GET_WORKFLOW_STATUS, JSON.stringify({
        status: WorkflowExecutionStatus.NEVER_RAN
      }))
    }

    else {
      let status = executionDetail.logs.some(log => !log.process_completed_at) ? WorkflowExecutionStatus.RUNNING : WorkflowExecutionStatus.COMPLETED;

      if (executionDetail.execution.workflow_execution_terminated_at)
        status = WorkflowExecutionStatus.TERMINATED;
        
      client.emit(SocketGateways.GET_WORKFLOW_STATUS, JSON.stringify({
        status    : status,
        execution : executionDetail.execution,
        logs      : executionDetail.logs
      }))
    }
  }

  /**
   * Get execution and logs
   */
  async getExecutionDetail(workflowId: Types.ObjectId): Promise<{
    logs?: LeanDocument<WorkflowExecutionLogDocument>[],
    execution?: LeanDocument<WorkflowExecutionDocument>
  }> {
    const execDoc = await this._workflowExecModel.findOne({
      workflow: workflowId
    })
    .sort({
      workflow_execution_started_at: -1
    })

    if (!execDoc) return {}

    const logDocs = await this._workflowExecLogModel.find({
      is_active: true,
      workflow_execution: execDoc._id
    });

    return {
      execution: execDoc.toJSON(),
      logs: logDocs.map(log => log.toJSON())
    }
  }

  /**
   * Get Workflow Execution Status
   * 
   * @param workflowId 
   */
  async getExecutionStatus(workflowId: Types.ObjectId): Promise<WorkflowExecutionStatus> {
    const { logs, execution } = await this.getExecutionDetail(workflowId);

    if (!execution) {
      return WorkflowExecutionStatus.NEVER_RAN;
    }

    else {
      return logs.some(log => !log.process_completed_at) ? WorkflowExecutionStatus.RUNNING : WorkflowExecutionStatus.COMPLETED;
    }
  }


}
