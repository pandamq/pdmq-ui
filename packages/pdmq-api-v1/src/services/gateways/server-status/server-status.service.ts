import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Socket } from 'dgram';
import { Model } from 'mongoose';
import * as pdmq from 'pdmq';
import { WorkflowDocument } from 'src/schema/tbl_workflows.schema';
import { WorkflowExecution, WorkflowExecutionDocument } from 'src/schema/tbl_workflow_executions.schema';
import { PdmqService } from 'src/services/common/pdmq/pdmq.service';

@Injectable()
export class ServerStatusService {

  constructor(
    private _pdmq: PdmqService,

    @InjectModel(WorkflowExecution.name)
    private _workflowExecModel: Model<WorkflowExecutionDocument>
  ) {}

  /**
   * Get Server Status
   */
  async getServerStatus() {
    
    /** Get Consumer Count */
    const consumerCount = await this._pdmq.client.getCurrentConsumersCount();
    const processingTasks = await this._pdmq.client.getProcessingTask();
    const instantTasks = await this._pdmq.client.findInstantTask();
    const processingWorkflows = await this._workflowExecModel.find({
      is_active: true,
      workflow_execution_terminated_at: {
        $eq: null
      },
      workflow_execution_logs: {
        $elemMatch: {
          success: {
            $exists: false
          }
        }
      }
    })
    .select([
      "workflow",
      "workflow_execution_started_at"
    ])
    .populate([
      {
        path: "workflow"
      }
    ])

    return {
      consumerCount,
      instantTasksCount: instantTasks.length,
      processingTasks: processingTasks?.map(task => ({
        task_id         : task.task_id,
        task_name       : task.task_name,
        task_description: task.task_description,
        task_created_at : task.task_created_at
      })) || [],
      processingWorkflows: processingWorkflows?.map(execution => {
        const workflow: WorkflowDocument = execution.workflow as any;
        return {
          _id       : workflow._id,
          name      : workflow.workflow_name,
          started_at: execution.workflow_execution_started_at
        }
      }) || []
    }

  }

}
