import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MongooseMiddlewarePostInit } from 'src/fn/schema-hook.fn';
import { TaskExecution, TaskExecutionSchema } from 'src/schema/tbl_task_executions.schema';
import { Variable, VariableSchema } from 'src/schema/tbl_variables.schema';
import { Workflow, WorkflowSchema } from 'src/schema/tbl_workflows.schema';
import { WorkflowExecution, WorkflowExecutionSchema } from 'src/schema/tbl_workflow_executions.schema';
import { WorkflowExecutionLog, WorkflowExecutionLogSchema } from 'src/schema/tbl_workflow_execution_logs.schema';
import { WorkflowStatusService } from './workflow-status/workflow-status.service';
import { ServerStatusService } from './server-status/server-status.service';
import { CommonServicesModule } from '../common/common-services.module';

@Module({
  providers: [
    WorkflowStatusService,
    ServerStatusService,
  ],
  exports: [
    WorkflowStatusService,
    ServerStatusService
  ],
  imports: [
    CommonServicesModule,
    MongooseModule.forFeatureAsync([
      {
        name: Workflow.name,
        useFactory: MongooseMiddlewarePostInit(WorkflowSchema)
      },
      {
        name: Variable.name,
        useFactory: MongooseMiddlewarePostInit(VariableSchema)
      },
      {
        name: TaskExecution.name,
        useFactory: MongooseMiddlewarePostInit(TaskExecutionSchema)
      },
      {
        name: WorkflowExecution.name,
        useFactory: MongooseMiddlewarePostInit(WorkflowExecutionSchema)
      },
      {
        name: WorkflowExecutionLog.name,
        useFactory: MongooseMiddlewarePostInit(WorkflowExecutionLogSchema)
      }
    ])
  ],
})
export class GatewayServicesModule {}
