import { Controller, Delete, Param, Post, Get, Req, Res, UseGuards, Query, Body, HttpStatus } from '@nestjs/common';
import { ApiBearerAuth, ApiParam, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { PaginationDto } from 'src/dto/common/pagination.dto';
import { SaveSystemSettingDto } from 'src/dto/system/save-system-setting.dto';
import { SystemSettings } from 'src/enum/system/system-settings.enum';
import { AuthenticationGuard } from 'src/guards/jwt-authentication.guard';
import { ResponseService } from 'src/services/common/response/response.service';
import { SettingsService } from 'src/services/controllers/system/settings/settings.service';

@UseGuards(AuthenticationGuard)
@ApiBearerAuth()
@ApiTags("System Settings")
@Controller('settings')
export class SettingsController {

  constructor(
    private _response: ResponseService,
    private _settings: SettingsService
  ) {}

  @Get()
  async fetch(
    @Req() req: Request,
    @Res() res: Response
  ) {
    try {
      const message = await this._settings.fetch();

      return this._response.send({
        req,
        res,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: error
      })
    }
  }

  @ApiParam({
    name: "name",
    type: String,
    enum: SystemSettings
  })
  @Get('/:name')
  async fetchByName(
    @Req() req: Request,
    @Res() res: Response,
    @Param('name') name: SystemSettings
  ) {
    try {
      const result = await this._settings.fetchByName(name);

      return this._response.send({
        req,
        res,
        message: {
          result
        }
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: error
      })
    }
  }

  @Post()
  async save(
    @Req() req: Request,
    @Res() res: Response,
    @Body() body: SaveSystemSettingDto
  ) {
    try {
      const result = await this._settings.save(body);

      return this._response.send({
        req,
        res,
        message: {
          result
        }
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: error
      })
    }
  }

}
