import { Module } from '@nestjs/common';
import { ServicesModule } from 'src/services/services.module';
import { SettingsController } from './settings/settings.controller';

@Module({
  controllers: [
    SettingsController
  ],
  imports: [
    ServicesModule
  ]
})
export class SystemModule {}
