import { Body, Controller, HttpStatus, Post, Req, Res, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { RefreshTokenDto } from 'src/dto/authentication/refresh-token.dto';
import { VerifyTokenDto } from 'src/dto/authentication/verify-token.dto';
import { AuthenticationGuard } from 'src/guards/jwt-authentication.guard';
import { ResponseService } from 'src/services/common/response/response.service';
import { TokenService } from 'src/services/controllers/authentication/token/token.service';

@ApiTags("Authentication")
@Controller('token')
export class TokenController {

  constructor(
    private _response: ResponseService,
    private _token: TokenService
  ) {}

  @ApiBearerAuth()
  @UseGuards(AuthenticationGuard)
  @Post('/refresh')
  async refreshToken(
    @Req() req: Request,
    @Res() res: Response,
    @Body() body: RefreshTokenDto
  ) {
    try {
      const message = await this._token.refreshToken(body.token, body.refreshToken);

      return this._response.send({
        req,
        res,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }

  @Post('/verify')
  async verifyToken(
    @Req() req: Request,
    @Res() res: Response,
    @Body() body: VerifyTokenDto
  ) {
    try {
      const message = await this._token.loadUserFromToken(body);

      return this._response.send({
        req,
        res,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }
}
