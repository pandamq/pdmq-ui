import { Module } from '@nestjs/common';
import { LoginController } from './login/login.controller';
import { AuthenticationServicesModule } from 'src/services/controllers/authentication/authentication-services.module';
import { AccessKeyController } from './access-key/access-key.controller';
import { CommonServicesModule } from 'src/services/common/common-services.module';
import { TokenController } from './token/token.controller';

@Module({
  controllers: [
    LoginController,
    AccessKeyController,
    TokenController
  ],
  imports: [
    CommonServicesModule,
    AuthenticationServicesModule
  ]
})
export class AuthenticationModule {}
