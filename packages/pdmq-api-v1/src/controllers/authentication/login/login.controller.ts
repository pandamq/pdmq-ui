import { Body, Controller, HttpStatus, Post, Req, Res } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { LoginDto } from 'src/dto/authentication/login.dto';
import { ResponseService } from 'src/services/common/response/response.service';
import { LoginService } from 'src/services/controllers/authentication/login/login.service';

@ApiTags("Authentication")
@Controller('login')
export class LoginController {
  
  constructor(
    private _login: LoginService,
    private _response: ResponseService
  ) {}

  @Post()
  async login(
    @Req() req: Request,
    @Res() res: Response,
    @Body() body: LoginDto
  ) {
    try {
      const message = await this._login.login(body);

      return this._response.send({
        req,
        res,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: error
      })
    }
  }

}
