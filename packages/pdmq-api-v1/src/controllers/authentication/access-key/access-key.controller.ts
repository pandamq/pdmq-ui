import { Controller, Delete, Get, HttpStatus, Param, Post, Put, Req, Res, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiParam, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { AuthenticationGuard } from 'src/guards/jwt-authentication.guard';
import { ResponseService } from 'src/services/common/response/response.service';
import { AccessKeyService } from 'src/services/controllers/authentication/access-key/access-key.service';

@UseGuards(AuthenticationGuard)
@ApiBearerAuth()
@ApiTags("Authentication")
@Controller('access-key')
export class AccessKeyController {

  constructor(
    private _response: ResponseService,
    private _accessKey: AccessKeyService
  ) {}

  @Post()
  async create(
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const message = await this._accessKey.generate(req['user']._id);

      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.OK,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: error
      })
    }
  }

  @Get('/summary')
  async delete(
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const message = await this._accessKey.summary(req['user']._id);

      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.OK,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: error
      })
    }
  }

}
