import { Body, Controller, Get, HttpStatus, Post, Query, Req, Res, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { PaginationDto } from 'src/dto/common/pagination.dto';
import { SaveUserDto } from 'src/dto/system/save-user.dto';
import { AuthenticationGuard } from 'src/guards/jwt-authentication.guard';
import { ResponseService } from 'src/services/common/response/response.service';
import { UsersService } from 'src/services/controllers/system/permissions/users/users.service';

@UseGuards(AuthenticationGuard)
@ApiBearerAuth()
@ApiTags("Users")
@Controller('users')
export class UsersController {

  constructor(
    private _response: ResponseService,
    private _users: UsersService
  ) {}

  @Get()
  async fetch(
    @Req() req: Request,
    @Res() res: Response,
    @Query() query: PaginationDto
  ) {
    try {
      const message = await this._users.fetch(query);

      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.OK,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: error
      })
    }
  }

  @Post()
  async save(
    @Req() req: Request,
    @Res() res: Response,
    @Body() body: SaveUserDto
  ) {
    try {
      const message = await this._users.save(body);

      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.OK,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: error
      })
    }
  }
}
