import { Module } from '@nestjs/common';
import { UsersController } from './users/users.controller';
import { ResourcesController } from './resources/resources.controller';
import { RolesController } from './roles/roles.controller';
import { ControllerServicesModule } from 'src/services/controllers/controller-services.module';
import { CommonServicesModule } from 'src/services/common/common-services.module';
import { AuthenticationServicesModule } from 'src/services/controllers/authentication/authentication-services.module';

@Module({
  controllers: [
    UsersController, 
    ResourcesController, 
    RolesController,
  ],
  imports: [
    AuthenticationServicesModule,
    CommonServicesModule,
    ControllerServicesModule
  ]
})
export class PermissionsModule {}
