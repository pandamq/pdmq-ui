import { Body, Controller, Get, HttpStatus, Post, Query, Req, Res, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { PaginationDto } from 'src/dto/common/pagination.dto';
import { SaveRoleDto } from 'src/dto/system/save-role.dto';
import { AuthenticationGuard } from 'src/guards/jwt-authentication.guard';
import { ResponseService } from 'src/services/common/response/response.service';
import { RolesService } from 'src/services/controllers/system/permissions/roles/roles.service';

@UseGuards(AuthenticationGuard)
@ApiBearerAuth()
@ApiTags("Roles")
@Controller('roles')
export class RolesController {

  constructor(
    private _response: ResponseService,
    private _roles: RolesService
  ) {}

  @Get()
  async fetch(
    @Req() req: Request,
    @Res() res: Response,
    @Query() query: PaginationDto
  ) {
    try {
      const message = await this._roles.fetch(query);

      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.OK,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: error
      })
    }
  }

  @Get('/summary')
  async fetchSummary(
    @Req() req: Request,
    @Res() res: Response
  ) {
    try {
      const message = await this._roles.fetchSummary();

      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.OK,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: error
      })
    }
  }

  @Post()
  async save(
    @Req() req: Request,
    @Res() res: Response,
    @Body() body: SaveRoleDto
  ) {
    try {
      const message = await this._roles.save(body);

      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.OK,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: error
      })
    }
  }
}
