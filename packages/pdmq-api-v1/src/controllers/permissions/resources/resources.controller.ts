import { Body, Controller, Get, HttpStatus, Post, Query, Req, Res, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { PaginationDto } from 'src/dto/common/pagination.dto';
import { SaveResourceDto } from 'src/dto/system/save-resource.dto';
import { AuthenticationGuard } from 'src/guards/jwt-authentication.guard';
import { ResponseService } from 'src/services/common/response/response.service';
import { ResourcesService } from 'src/services/controllers/system/permissions/resources/resources.service';

@UseGuards(AuthenticationGuard)
@ApiBearerAuth()
@ApiTags("Resources")
@Controller('resources')
export class ResourcesController {

  constructor(
    private _response: ResponseService,
    private _resources: ResourcesService
  ) {}

  @Get()
  async fetch(
    @Req() req: Request,
    @Res() res: Response,
    @Query() query: PaginationDto
  ) {
    try {
      const message = await this._resources.fetch(query);

      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.OK,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: error
      })
    }
  }

  @Get('/summary')
  async fetchSummary(
    @Req() req: Request,
    @Res() res: Response
  ) {
    try {
      const message = await this._resources.fetchSummary();

      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.OK,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: error
      })
    }
  }

  @Post()
  async save(
    @Req() req: Request,
    @Res() res: Response,
    @Body() body: SaveResourceDto
  ) {
    try {
      const message = await this._resources.save(body);

      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.OK,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: error
      })
    }
  }
}
