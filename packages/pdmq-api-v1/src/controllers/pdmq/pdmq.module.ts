import { Module } from '@nestjs/common';
import { VariablesController } from './variables/variables.controller';
import { TasksController } from './tasks/tasks.controller';
import { WorkflowsController } from './workflows/workflows.controller';
import { PdmqServiceModule } from 'src/services/controllers/pdmq/pdmq-services.module';
import { ExecutionLogsController } from './workflows/execution-logs/execution-logs.controller';
import { WorkflowCronController } from './workflows/workflow-cron/workflow-cron.controller';

@Module({
  controllers: [
    VariablesController, 
    TasksController, 
    WorkflowsController, ExecutionLogsController, WorkflowCronController
  ],
  imports: [
    PdmqServiceModule
  ]
})
export class PdmqModule {}
