import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Query, Req, Res, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiParam, ApiSecurity, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { PaginationDto } from 'src/dto/common/pagination.dto';
import { SaveWorkflowDto } from 'src/dto/pdmq/save-workflow.dto';
import { AuthenticationGuard } from 'src/guards/jwt-authentication.guard';
import { ResponseService } from 'src/services/common/response/response.service';
import { WorkflowsService } from 'src/services/controllers/pdmq/workflows/workflows.service';

@ApiBearerAuth()
@ApiSecurity('x-api-key')
@UseGuards(AuthenticationGuard)
@ApiTags("Workflows")
@Controller('workflows')
export class WorkflowsController {
    
  constructor(
    private _response: ResponseService,
    private _workflows: WorkflowsService
  ) {}

  @ApiOperation({
    description: "Fetch All Workflows"
  })
  @Get()
  async fetch(
    @Req() req: Request,
    @Res() res: Response,
    @Query() pagination: PaginationDto
  ) {
    try {
      const message = await this._workflows.fetch(pagination);

      return this._response.send({
        req,
        res,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }

  @ApiOperation({
    description: "Fetch Single Workflow"
  })
  @ApiParam({
    name: 'workflowId',
    type: 'string'
  })
  @Get('/:workflowId')
  async fetchItem(
    @Req() req: Request,
    @Res() res: Response,
    @Param('workflowId') workflowId: string
  ) {
    try {
      const message = await this._workflows.fetchItem(workflowId);
      return this._response.send({
        req,
        res,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }
  
  @ApiParam({
    name: 'workflowId',
    type: 'string'
  })
  @ApiOperation({
    description: "Run Workflow"
  })
  @Post('/:workflowId/run')
  async run(
    @Req() req: Request,
    @Res() res: Response,
    @Param('workflowId') workflowId: string
  ) {
    try {
      const executionDoc = await this._workflows.run(workflowId, {
        apiUrl: req.protocol + '://' + req.get('host') 
      });

      return this._response.send({
        req,
        res,
        message: {
          execution: executionDoc
        }
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }

  @ApiParam({
    name: 'workflowId',
    type: 'string'
  })
  @ApiOperation({
    description: "Stop Workflow"
  })
  @Post('/:workflowId/stop')
  async stop(
    @Req() req: Request,
    @Res() res: Response,
    @Param('workflowId') workflowId: string
  ) {
    try {
      await this._workflows.stop(workflowId);

      return this._response.send({
        req,
        res,
        message: {}
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }
  @ApiOperation({
    description: "Upsert Workflow"
  })
  @Post()
  async save(
    @Req() req: Request,
    @Res() res: Response,
    @Body() body: SaveWorkflowDto
  ) {
    try {
      const message = await this._workflows.save(body);

      return this._response.send({
        req,
        res,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }

  @ApiOperation({
    description: "Delete Workflow"
  })
  @ApiParam({
    name: 'workflowId',
    type: 'string'
  })
  @Delete('/:workflowId')
  async delete(
    @Req() req: Request,
    @Res() res: Response,
    @Param() params: {workflowId: string}
  ) {
    try {
      const message = await this._workflows.delete(params.workflowId);

      return this._response.send({
        req,
        res,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }
}
