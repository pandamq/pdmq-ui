import { Controller, Get, HttpStatus, Param, Query, Req, Res, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiParam, ApiSecurity, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { PaginationDto } from 'src/dto/common/pagination.dto';
import { AuthenticationGuard } from 'src/guards/jwt-authentication.guard';
import { ResponseService } from 'src/services/common/response/response.service';
import { ExecutionLogsService } from 'src/services/controllers/pdmq/workflows/execution-logs/execution-logs.service';

@ApiBearerAuth()
@ApiSecurity('x-api-key')
@UseGuards(AuthenticationGuard)
@ApiTags("Workflow Execution Logs")
@Controller('/workflows/:workflowId/nodes')
export class ExecutionLogsController {

  constructor(
    private _response: ResponseService,
    private _executionLogs: ExecutionLogsService
  ) {}

  @ApiParam({
    name: "workflowId",
    type: String
  })
  @ApiParam({
    name: "nodeId",
    type: String
  })
  @ApiOperation({
    description: "Fetch logs for a node"
  })
  @Get('/:nodeId/logs')
  async fetch(
    @Req() req: Request,
    @Res() res: Response,
    @Query() pagination: PaginationDto,
    @Param() params: {
      workflowId: string,
      nodeId: string
    },
  ) {
    try {
      const message = await this._executionLogs.fetch({
        pagination,
        ...params
      });

      return this._response.send({
        req,
        res,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }


}
