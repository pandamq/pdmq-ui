import { Test, TestingModule } from '@nestjs/testing';
import { ExecutionLogsController } from './execution-logs.controller';

describe('ExecutionLogsController', () => {
  let controller: ExecutionLogsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ExecutionLogsController],
    }).compile();

    controller = module.get<ExecutionLogsController>(ExecutionLogsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
