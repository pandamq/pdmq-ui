import { Test, TestingModule } from '@nestjs/testing';
import { WorkflowCronController } from './workflow-cron.controller';

describe('WorkflowCronController', () => {
  let controller: WorkflowCronController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WorkflowCronController],
    }).compile();

    controller = module.get<WorkflowCronController>(WorkflowCronController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
