import { AuthenticationGuard } from 'src/guards/jwt-authentication.guard';
import { Controller, Get, HttpStatus, Param, Post, Req, Res, UseGuards } from '@nestjs/common';
import { ApiTags, ApiSecurity, ApiBearerAuth, ApiParam } from '@nestjs/swagger';
import { ResponseService } from 'src/services/common/response/response.service';
import { WorkflowCronService } from 'src/services/controllers/pdmq/workflows/workflow-cron/workflow-cron.service';
import { Request, Response } from 'express';

@ApiBearerAuth()
@ApiSecurity('x-api-key')
@UseGuards(AuthenticationGuard)
@ApiTags("Workflows")
@Controller('workflows/cron')
export class WorkflowCronController {

  constructor(
    private _response: ResponseService,
    private _cron: WorkflowCronService
  ) {}

  @Get('/summary')
  async summary(
    @Req() req: Request,
    @Res() res: Response
  ) {
    try {
      const results = await this._cron.fetch();

      return this._response.send({
        req,
        res,
        message: {
          results,
          count: results.length
        }
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }

  @ApiParam({
    name: 'workflowId',
    type: String
  })
  @Post('/:workflowId/toggle-status')
  async toggleStatus(
    @Req() req: Request,
    @Res() res: Response,
    @Param('workflowId') workflowId: string
  ) {
    try {
      const result = await this._cron.toggleStatus(workflowId);

      return this._response.send({
        req,
        res,
        message: {
          result
        }
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }

}
