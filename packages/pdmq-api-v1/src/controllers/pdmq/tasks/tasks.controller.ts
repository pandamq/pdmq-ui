import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Query, Req, Res, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiParam, ApiQuery, ApiSecurity, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { SaveTaskDto } from 'src/dto/pdmq/save-task.dto';
import { AuthenticationGuard } from 'src/guards/jwt-authentication.guard';
import { ResponseService } from 'src/services/common/response/response.service';
import { TasksService } from 'src/services/controllers/pdmq/tasks/tasks.service';
import * as moment from 'moment';
import { TaskTriggerTypes } from 'src/enum/task-trigger-types.enum';

@ApiBearerAuth()
@ApiSecurity('x-api-key')
@UseGuards(AuthenticationGuard)
@ApiTags("Tasks")
@Controller('tasks')
export class TasksController {
  
  constructor(
    private _response: ResponseService,
    private _tasks: TasksService
  ) {}

  @ApiOperation({
    description: "Fetch All Tasks"
  })
  @ApiQuery({
    name: 'from',
    type: 'string',
    required: false,
    example: moment().subtract(1, 'week').format("YYYY-MM-DD")
  })
  @ApiQuery({
    name: 'to',
    type: 'string',
    required: false,
    example: moment().format("YYYY-MM-DD")
  })
  @ApiQuery({
    name: 'type',
    type: 'string',
    enum: TaskTriggerTypes
  })
  @Get()
  async fetch(
    @Req() req: Request,
    @Res() res: Response,
    @Query() query: {from?: string, to?: string, type:TaskTriggerTypes }
  ) {
    try {
      const message = await this._tasks.fetch(query);

      return this._response.send({
        req,
        res,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }

  @ApiOperation({
    description: "Upsert Task"
  })
  @Post()
  async save(
    @Req() req: Request,
    @Res() res: Response,
    @Body() body: SaveTaskDto
  ) {
    try {
      const message = await this._tasks.save(body);
      return this._response.send({
        req,
        res,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }

  @ApiOperation({
    description: "Delete Task"
  })
  @ApiParam({
    name: 'taskId',
    type: 'string'
  })
  @Delete('/:taskId')
  async delete(
    @Req() req: Request,
    @Res() res: Response,
    @Param() params: {taskId: string}
  ) {
    try {
      await this._tasks.delete(params.taskId);

      return this._response.send({
        req,
        res,
        message: params.taskId
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }
}
