import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Query, Req, Res, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiParam, ApiSecurity, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { PaginationDto } from 'src/dto/common/pagination.dto';
import { SaveVariableDto } from 'src/dto/pdmq/save-variable.dto';
import { AuthenticationGuard } from 'src/guards/jwt-authentication.guard';
import { ResponseService } from 'src/services/common/response/response.service';
import { SystemVariablesService } from 'src/services/common/system-variables/system-variables.service';
import { VariablesService } from 'src/services/controllers/pdmq/variables/variables.service';

@ApiBearerAuth()
@ApiSecurity('x-api-key')
@UseGuards(AuthenticationGuard)
@ApiTags("Variables")
@Controller('variables')
export class VariablesController {

  constructor(
    private _response: ResponseService,
    private _variables: VariablesService,
    private _systemVariables: SystemVariablesService
  ) {}

  @ApiOperation({
    description: "Fetch All Variables"
  })
  @Get()
  async fetch(
    @Req() req: Request,
    @Res() res: Response,
    @Query() pagination: PaginationDto
  ) {
    try {
      const message = await this._variables.fetch(pagination);

      return this._response.send({
        req,
        res,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }


  @ApiOperation({
    description: "Fetch Examples"
  })
  @Get('examples')
  async fetchExamples(
    @Req() req: Request,
    @Res() res: Response,
    @Query() pagination: PaginationDto
  ) {
    try {
      const variables = await this._systemVariables.listAllVariables();

      return this._response.send({
        req,
        res,
        message: {
          results: variables,
          count: variables.length
        }
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }

  @Get('/names')
  async fetchNames(
    @Req() req: Request,
    @Res() res: Response
  ) {
    try {
      const message = await this._variables.fetchNames();

      return this._response.send({
        req,
        res,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }

  @ApiOperation({
    description: "Fetch Variable Value"
  })
  @Get('/:variableName/value')
  async fetchVariableValue(
    @Req() req: Request,
    @Res() res: Response,
    @Param('variableName') variableName: string
  ) {
    try {
      const message = await this._variables.fetchVariableValue(variableName);

      return this._response.send({
        req,
        res,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }

  @ApiOperation({
    description: "Upsert Variable"
  })
  @Post()
  async save(
    @Req() req: Request,
    @Res() res: Response,
    @Body() body: SaveVariableDto
  ) {
    try {
      const message = await this._variables.save(body);

      return this._response.send({
        req,
        res,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }

  @ApiOperation({
    description: "Delete Variable"
  })
  @ApiParam({
    name: 'variableId',
    type: 'string'
  })
  @Delete('/:variableId')
  async delete(
    @Req() req: Request,
    @Res() res: Response,
    @Param() params: {variableId: string}
  ) {
    try {
      const message = await this._variables.delete(params.variableId);

      return this._response.send({
        req,
        res,
        message
      })
    } catch (error) {
      return this._response.send({
        req,
        res,
        message: error,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR
      })
    }
  }

}
