import { Logger, UseGuards } from '@nestjs/common';
import { SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Socket } from 'dgram';
import { Server } from 'http';
import { Types } from 'mongoose';
import { SocketGateways } from 'src/enum/sockets/socket-gateways.enum';
import { WsAuthenticationGuard } from 'src/guards/ws-authentication.guard';
import { ServerStatusService } from 'src/services/gateways/server-status/server-status.service';

@UseGuards(WsAuthenticationGuard)
@WebSocketGateway({cors: true})
export class ServerStatusGateway {

  @WebSocketServer() 
  private server: Server;

  private logger = new Logger(ServerStatusGateway.name)

  constructor(
    private _serverStatus: ServerStatusService
  ) {

  }

  @SubscribeMessage(SocketGateways.GET_SERVER_STATUS)
  async handleMessage(client: Socket) {
    try {
      let promise: Promise<any>;
      if (!promise)
        promise = this._serverStatus.getServerStatus();
      const res = await promise;
      promise   = null;
      client.emit(SocketGateways.GET_SERVER_STATUS, JSON.stringify(res));


      const interval = setInterval(async () => {
        try {
          if (!promise)
            promise = this._serverStatus.getServerStatus();
          const res = await promise;
          promise   = null;
          client.emit(SocketGateways.GET_SERVER_STATUS, JSON.stringify(res));
        } catch (error) {
          this.logger.error(error);
        }
      }, 5 * 1000);
      
      client.on('disconnect', () => {
        clearInterval(interval);
      })
    } catch (error) {
      this.logger.error(error);
      client.disconnect();
    }
  }

}
