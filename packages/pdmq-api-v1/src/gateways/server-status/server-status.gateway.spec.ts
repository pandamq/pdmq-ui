import { Test, TestingModule } from '@nestjs/testing';
import { ServerStatusGateway } from './server-status.gateway';

describe('ServerStatusGateway', () => {
  let gateway: ServerStatusGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ServerStatusGateway],
    }).compile();

    gateway = module.get<ServerStatusGateway>(ServerStatusGateway);
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });
});
