import { Module } from '@nestjs/common';
import { PdmqServiceModule } from 'src/services/controllers/pdmq/pdmq-services.module';
import { GatewayServicesModule } from 'src/services/gateways/gateways.module';
import { ServicesModule } from 'src/services/services.module';
import { WorkflowStatusGateway } from './workflow-status/workflow-status.gateway';
import { ServerStatusGateway } from './server-status/server-status.gateway';

@Module({
  providers: [
    WorkflowStatusGateway,
    ServerStatusGateway
  ],
  imports: [
    ServicesModule,
    PdmqServiceModule,
    GatewayServicesModule
  ],
  exports: [  
    WorkflowStatusGateway,
    ServerStatusGateway
  ]
})
export class GatewaysModule {}
