import { SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { SocketGateways } from 'src/enum/sockets/socket-gateways.enum';
import { Socket } from 'dgram';
import { Server } from 'net';
import { Logger, UseGuards } from '@nestjs/common';
import { WsAuthenticationGuard } from 'src/guards/ws-authentication.guard';
import { InjectModel } from '@nestjs/mongoose';
import { WorkflowStatusService } from 'src/services/gateways/workflow-status/workflow-status.service';
import { WorkflowExecutionDocument } from 'src/schema/tbl_workflow_executions.schema';
import { LeanDocument, Types } from 'mongoose';


@UseGuards(WsAuthenticationGuard)
@WebSocketGateway({cors: true})
export class WorkflowStatusGateway {
  @WebSocketServer() 
  private server: Server;

  private logger = new Logger(WorkflowStatusGateway.name)

  constructor(
    private _workflowStatus: WorkflowStatusService
  ) {

  }

  @SubscribeMessage(SocketGateways.GET_WORKFLOW_STATUS)
  async getWorkflowStatus(client: Socket, payload: string) {
    try {
      let promise: Promise<any>;
      const workflowId = new Types.ObjectId(payload);

      const func = () => this._workflowStatus.getWorkflowStatus({
        client,
        workflowId,
        promise
      });
      
      func();
      const interval = setInterval(func, 3 * 1000);
      
      client.on('disconnect', () => {
        clearInterval(interval);
      })
    } catch (error) {
      this.logger.error(error);
      client.disconnect();
    }
  }
}
