import { Test, TestingModule } from '@nestjs/testing';
import { WorkflowStatusGateway } from './workflow-status.gateway';

describe('WorkflowStatusGateway', () => {
  let gateway: WorkflowStatusGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WorkflowStatusGateway],
    }).compile();

    gateway = module.get<WorkflowStatusGateway>(WorkflowStatusGateway);
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });
});
