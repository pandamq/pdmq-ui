import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { ApplicationRoutingModule } from './app.routing.module';
import { AppService } from './app.service';
import { AuthenticationModule } from './controllers/authentication/authentication.module';
import { LoggerMiddleware } from './middlewares/logger.middleware';
import { ServicesModule } from './services/services.module';
import { PdmqModule } from './controllers/pdmq/pdmq.module';
import { GatewaysModule } from './gateways/gateways.module';
import { SystemModule } from './controllers/system/system.module';
import { PermissionsModule } from './controllers/permissions/permissions.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true
    }),
    MongooseModule.forRoot(
      process.env.MONGO_CONNECTION_STRING
    ),
    ApplicationRoutingModule,
    AuthenticationModule,
    ServicesModule,
    PdmqModule,
    GatewaysModule,
    SystemModule,
    PermissionsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .forRoutes({
        path   : '*', 
        method : RequestMethod.ALL
      });
  }
}
