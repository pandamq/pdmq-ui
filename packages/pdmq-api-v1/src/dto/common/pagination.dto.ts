import { ApiProperty } from "@nestjs/swagger";

export class PaginationDto {

  @ApiProperty({
    required: false,
    default: 0
  })
  index?: string

  @ApiProperty({
    required: false,
    default: 20
  })
  size?: string

  @ApiProperty({
    required: false
  })
  searchKey?: string

  @ApiProperty({
    required: false
  })
  startAt?: string

  @ApiProperty({
    required: false
  })
  endAt?: string

  @ApiProperty({
    required: false
  })
  sortBy?: string

  @ApiProperty({
    required: false
  })
  direction?: string

  @ApiProperty({
    required: false
  })
  from?: string
  
  @ApiProperty({
    required: false
  })
  to?: string
}