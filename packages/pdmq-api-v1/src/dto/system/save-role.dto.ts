import { ApiProperty } from "@nestjs/swagger";

export class SaveRoleDto {

  @ApiProperty()
  _id?: string

  @ApiProperty()
  role_icon?: string

  @ApiProperty()
  role_name?: string

}