import { ApiProperty } from "@nestjs/swagger";

export class SaveResourceDto {

    @ApiProperty()
    _id?: string

    @ApiProperty()
    resource_name?: string

    @ApiProperty()
    has_view?: boolean

    @ApiProperty()
    has_create?: boolean

    @ApiProperty()
    has_delete?: boolean

    @ApiProperty()
    has_edit?: boolean

}