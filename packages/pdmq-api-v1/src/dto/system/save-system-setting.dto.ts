import { ApiProperty } from "@nestjs/swagger";

export class SaveSystemSettingDto {

  @ApiProperty()
  _id?: string


  @ApiProperty()
  system_setting_name?: string


  @ApiProperty()
  system_setting_data?: any

}