import { ApiProperty } from "@nestjs/swagger";

export class SaveUserDto {

  @ApiProperty()
  _id?: string


  @ApiProperty()
  email?: string


  @ApiProperty()
  api_restricted?: boolean

  @ApiProperty()
  dashboard_restricted?: boolean

}