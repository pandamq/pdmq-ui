import { ApiProperty } from "@nestjs/swagger";
import { ClientTypes } from "src/enum/client-types.enum";

export class LoginDto {

  @ApiProperty()
  email: string

  @ApiProperty()
  password: string

  @ApiProperty()
  client_type?: ClientTypes

}