import { ApiProperty } from "@nestjs/swagger";

export class SaveWorkflowDto {

  @ApiProperty({
    required: false
  })
  _id?: string

  @ApiProperty()
  workflow_name: string

  @ApiProperty()
  workflow_description: string

  @ApiProperty()
  workflow_body: any

}