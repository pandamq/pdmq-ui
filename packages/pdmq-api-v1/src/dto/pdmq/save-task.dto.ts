import { ApiProperty } from "@nestjs/swagger";

export class SaveTaskDto {

  @ApiProperty()
  task_id?: string

  @ApiProperty()
  task_name?: string

  @ApiProperty()
  task_description?: string

  /** TODO */
  @ApiProperty()
  task_cron_expression?: string

  @ApiProperty()
  task_message?: string

  @ApiProperty()
  task_consumer_identity?: string

  @ApiProperty()
  task_trigger_type?: 'once' | 'daily' | 'weekly' | 'monthly' | 'annually' | 'never' | 'instant'

  /** ISO date time string */
  @ApiProperty()
  task_trigger_once_datetime?: string

  /** eg. '00?:00?:00' | '10?:10?:30' | '19?:30?:30' | '23?:59?:59' */
  @ApiProperty()
  task_trigger_time?: string

  /** eg. [1 2 3 4 5 6 7] */
  @ApiProperty()
  task_trigger_days?: number[]

  /** eg. 01 | 15 | 28 */
  @ApiProperty()
  task_trigger_date?: number
  @ApiProperty()

  /** eg. ['01' '12'] */
  @ApiProperty()
  task_trigger_months?: string[]

  /** eg. [2021 /20[0-9]2/] */
  @ApiProperty()
  task_trigger_years?: (string | number)[]

  @ApiProperty()
  task_fallback_retry_limit?: number

  /** In Seconds */
  @ApiProperty()
  task_fallback_retry_duration?: number

  /** In Seconds */
  @ApiProperty()
  task_fallback_task_id?: string

  /** Eg.?: ISO */
  @ApiProperty()
  trigger_next_at?: string

  @ApiProperty()
  task_created_at?: string

  @ApiProperty()
  task_updated_at?: string

  /** In Seconds */
  @ApiProperty()
  task_instant_timeout?: number

}