import { ApiProperty } from "@nestjs/swagger";
import { FieldTypes } from 'pdmq/dist/enums/field-types.enum';

export class SaveVariableDto {

  @ApiProperty({
    required: false
  })
  _id?: string

  @ApiProperty()
  variable_name: string

  @ApiProperty()
  variable_data: string

  @ApiProperty({
    enum: FieldTypes
  })
  variable_type: FieldTypes

}