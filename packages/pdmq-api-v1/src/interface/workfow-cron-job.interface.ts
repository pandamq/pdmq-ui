import { CronJob } from 'cron';

export interface WorkflowCronJobInterface {
  workflow_id: string,
  workflow_cron_expression: string,
  job: CronJob
}