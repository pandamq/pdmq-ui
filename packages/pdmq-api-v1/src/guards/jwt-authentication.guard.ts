import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Request } from 'express';
import * as jwt from 'jsonwebtoken';
import * as fs from 'fs';
import * as path from 'path';
import { AccessKeyService } from 'src/services/controllers/authentication/access-key/access-key.service';

@Injectable()
export class AuthenticationGuard implements CanActivate {

  constructor(
    private _accessKey: AccessKeyService
  ) {}

  async canActivate(
    context: ExecutionContext,
  ): Promise<boolean> {
    try {
      const request: Request = context.switchToHttp().getRequest();
      const token = request.headers?.authorization;
      const apiKey = request.headers['x-api-key'];
      
      if (token) {
        const publicKey = fs.readFileSync(path.join(__dirname, '../keystore', 'public.key'), {encoding: 'utf-8'});
        const user = jwt.verify(token.split(" ")[1], publicKey);
        request['user'] = user;
        return true;
      }
      else if (apiKey) {
        const user = await this._accessKey.verify(apiKey.toString());
        request['user'] = user;
        return true;
      }
      else 
        return false
    } catch (error) {
      return false;
    }
  }
}
