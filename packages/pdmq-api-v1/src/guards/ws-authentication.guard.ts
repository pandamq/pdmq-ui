import { CanActivate, Injectable } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import * as fs from 'fs';
import * as path from 'path';

@Injectable()
export class WsAuthenticationGuard implements CanActivate {
  canActivate(
    context: any,
  ): boolean {
    try {
      const token = context.args[0].handshake.headers.authorization.split(' ')[1];
      if (token) {
        const publicKey = fs.readFileSync(path.join(__dirname, '../keystore', 'public.key'), {encoding: 'utf-8'});
        const user = jwt.verify(token, publicKey);
        return true;
      }
      else 
        return false
    } catch (error) {
      return false;
    }
  }
}
