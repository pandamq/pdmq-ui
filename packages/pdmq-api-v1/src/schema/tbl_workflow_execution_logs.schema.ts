import { Base } from './base.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { WorkflowExecution } from './tbl_workflow_executions.schema';
import { WorkflowNodeCategories } from 'pdmq/dist/enums/workflow-node-categories.enum';
import { Workflow } from './tbl_workflows.schema';

export type WorkflowExecutionLogDocument = WorkflowExecutionLog & Document;

@Schema({
  collection: 'tbl_workflow_execution_logs'
})
export class WorkflowExecutionLog extends Base {

  @Prop()
  process_completed_at: Date

  @Prop()
  process_started_at: Date

  @Prop({
    type: Types.ObjectId,
    ref: Workflow.name
  })
  workflow: Types.ObjectId

  @Prop({
    type: Types.ObjectId,
    ref: WorkflowExecution.name
  })
  workflow_execution: Types.ObjectId

  @Prop()
  node_execution_id: string

  @Prop({
    type: Object
  })
  node_execution_result: any

  @Prop({
    type: Object
  })
  node_execution_context: any

  @Prop({
    type: Number
  })
  node_id: number

  @Prop({
    type: String
  })
  node_type: string

  @Prop({
    enum: WorkflowNodeCategories,
    type: String
  })
  node_category: WorkflowNodeCategories

}

export const WorkflowExecutionLogSchema = SchemaFactory.createForClass(WorkflowExecutionLog);
