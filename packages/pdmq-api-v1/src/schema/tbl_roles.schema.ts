import { Base } from './base.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Resource } from './tbl_resources.schema';

export type RoleDocument = Role & Document;

@Schema({
  collection: 'tbl_roles'
})
export class Role extends Base {

  @Prop({
    required: true
  })
  role_name: string;

  @Prop({
  })
  role_icon: string;

  @Prop({
    default: [],
    type: [{
      resource: {
        type: Types.ObjectId,
        ref: Resource.name,
        required: true
      },
      can_view: {
        type: Boolean,
        default: false
      },
      can_create: {
        type: Boolean,
        default: false
      },
      can_edit: {
        type: Boolean,
        default: false
      },
      can_delete: {
        type: Boolean,
        default: false
      }
    }]
  })
  resources: {
    resource: Types.ObjectId,
    can_view: boolean,
    can_create: boolean,
    can_edit: boolean,
    can_delete: boolean,
  }[]

}

export const RoleSchema = SchemaFactory.createForClass(Role);
