import { Prop, Schema } from '@nestjs/mongoose';
import { Types } from 'mongoose';

@Schema({
})
export class Base {
  @Prop({
    default: Date.now()
  })
  created_at: Date;

  @Prop({
    type: Number,
    default: 1
  })
  revision: number

  @Prop()
  created_by: Types.ObjectId;

  @Prop()
  updated_at: Date;

  @Prop()
  updated_by: Types.ObjectId;

  @Prop({
    default: true
  })
  is_active: boolean;

  @Prop({
    default: false
  })
  is_system: boolean;
}