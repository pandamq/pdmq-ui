import { Base } from './base.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { FieldTypes } from 'pdmq/dist/enums/field-types.enum';
import { Resource } from './tbl_resources.schema';

export type VariableDocument = Variable & Document;

@Schema({
  collection: 'tbl_variables'
})
export class Variable extends Base {
  @Prop()
  variable_name: string; 

  @Prop({
    type: String
  })
  variable_data: string

  @Prop()
  variable_type: FieldTypes; 

  @Prop({
    type: Types.ObjectId,
    ref: Resource.name
  })
  workflow_resource: Types.ObjectId;
}

export const VariableSchema = SchemaFactory.createForClass(Variable);
