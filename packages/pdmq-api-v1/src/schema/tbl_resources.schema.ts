import { Base } from './base.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ResourceDocument = Resource & Document;

@Schema({
  collection: 'tbl_resources'
})
export class Resource extends Base {
  @Prop()
  resource_name: string; 
  @Prop()
  has_view: boolean;
  @Prop()
  has_create: boolean;
  @Prop()
  has_edit: boolean;
  @Prop()
  has_delete: boolean;
}

export const ResourceSchema = SchemaFactory.createForClass(Resource);
