import { Base } from './base.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Resource } from './tbl_resources.schema';

export type WorkflowDocument = Workflow & Document;

@Schema({
  collection: 'tbl_workflows'
})
export class Workflow extends Base {
  @Prop({
    required: true
  })
  workflow_name: string;

  @Prop()
  workflow_description: string;

  @Prop({
    default: false
  })
  workflow_ignore_failure: boolean;

  @Prop({
    type: Object
  })
  workflow_body: any;

  @Prop()
  workflow_entry_point_cron: string;

  @Prop()
  workflow_bind_api_key: string;

  @Prop({
    type: Types.ObjectId,
    ref: Resource.name
  })
  workflow_resource: Types.ObjectId;
}

export const WorkflowSchema = SchemaFactory.createForClass(Workflow);
