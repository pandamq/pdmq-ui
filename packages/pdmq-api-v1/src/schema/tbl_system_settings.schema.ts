import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Base } from "./base.schema";

export type SystemSettingDocument = SystemSetting & Document;

@Schema({
  collection: 'tbl_system_settings'
})
export class SystemSetting extends Base {

  @Prop()
  system_setting_name: string; 

  @Prop({
    type: Object
  })
  system_setting_data: any; 

}

export const SystemSettingSchema = SchemaFactory.createForClass(SystemSetting);