import { Base } from './base.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Login } from './tbl_logins.schema';

export type AccessKeyDocument = AccessKey & Document;

@Schema({
  collection: 'tbl_access_keys'
})
export class AccessKey extends Base {

  @Prop({
    required: true,
    ref: Login.name
  })
  login: Types.ObjectId;

  @Prop({
    required: true
  })
  key: string;
}

export const AccessKeySchema = SchemaFactory.createForClass(AccessKey);
