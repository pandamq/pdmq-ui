import { Base } from './base.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Role } from './tbl_roles.schema';

export type LoginDocument = Login & Document;

@Schema({
  collection: 'tbl_logins'
})
export class Login extends Base {

  @Prop({
    unique: true,
    required: true,
  })
  email: string;

  @Prop({
    required: true
  })
  password: string;

  @Prop({
    default: [],
    ref: Role.name,
    type: [
      Types.ObjectId
    ]
  })
  roles: Types.ObjectId[];

  @Prop()
  agreement_version: string;

  @Prop()
  comment: string;

  @Prop({
    default: true,
    type: Boolean
  })
  dashboard_restricted: boolean;

  @Prop({
    default: false,
    type: Boolean
  })
  api_restricted: boolean;
}

export const LoginSchema = SchemaFactory.createForClass(Login);
