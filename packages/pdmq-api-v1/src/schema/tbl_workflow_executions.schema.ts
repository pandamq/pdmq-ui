import { Base } from './base.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Workflow } from './tbl_workflows.schema';

export type WorkflowExecutionDocument = WorkflowExecution & Document;

@Schema({
  collection: 'tbl_workflow_executions'
})
export class WorkflowExecution extends Base {

  @Prop({
    type: Types.ObjectId,
    ref: Workflow.name
  })
  workflow: Types.ObjectId

  @Prop()
  workflow_execution_completed_at: Date

  @Prop()
  workflow_execution_terminated_at: Date


  @Prop()
  workflow_execution_started_at: Date

  @Prop({
    type: Array,
    default: []
  })
  workflow_execution_logs: {
    log_id: Types.ObjectId,
    success: boolean
  }[]

}

export const WorkflowExecutionSchema = SchemaFactory.createForClass(WorkflowExecution);
