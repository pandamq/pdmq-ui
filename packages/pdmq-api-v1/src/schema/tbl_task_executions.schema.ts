import { Base } from './base.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type TaskExecutionDocument = TaskExecution & Document;

@Schema({
  collection: 'tbl_task_executions'
})
export class TaskExecution extends Base {
  @Prop()
  task_execution_id: string; 

  @Prop()
  task_execution_success: boolean

  @Prop({
    type: Object
  })
  task_execution_result: any

  @Prop()
  task_execution_task_id: string; 
}

export const TaskExecutionSchema = SchemaFactory.createForClass(TaskExecution);
