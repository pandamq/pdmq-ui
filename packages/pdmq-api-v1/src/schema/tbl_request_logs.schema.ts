import { Base } from './base.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Login } from './tbl_logins.schema';
import { HttpStatus } from '@nestjs/common';

export type RequestLogDocument = RequestLog & Document;

@Schema({
  collection: 'tbl_request_logs'
})
export class RequestLog extends Base {

  @Prop({
    type: Types.ObjectId,
    ref: Login.name
  })
  login: Types.ObjectId;

  @Prop()
  key: string;

  @Prop()
  path: string;

  @Prop()
  method: string;

  @Prop()
  status_code: HttpStatus;

  @Prop({
    type: Object
  })
  error: Object;

  @Prop()
  ip: string;

  @Prop()
  client: string;

  @Prop()
  useragent: string;

  @Prop()
  received_at: Date;

  @Prop()
  responded_at: Date;

  @Prop()
  time_spend: number;
}

export const RequestLogSchema = SchemaFactory.createForClass(RequestLog);
