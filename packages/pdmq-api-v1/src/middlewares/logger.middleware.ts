import { Injectable, Logger, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import * as moment from 'moment';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {

  private logger = new Logger(LoggerMiddleware.name);

  constructor(
  ) {}

  use(
    req: Request, 
    res: Response, 
    next: () => void
  ) {
    if (process.env.DEBUG_MODE)
      this.logger.debug(`[${req.method}] ${req.originalUrl}`)
    req['fullPath']   = req.originalUrl;
    req['receivedAt'] = moment();
    req['roleId']     = req.headers['x-role-id'];
    next();
  }
}
