import { Module } from "@nestjs/common";
import { Route, RouterModule } from "nest-router";
import { AuthenticationModule } from "./controllers/authentication/authentication.module";
import { PdmqModule } from "./controllers/pdmq/pdmq.module";
import { PermissionsModule } from "./controllers/permissions/permissions.module";
import { SystemModule } from "./controllers/system/system.module";

const routes: Route[] = [
  {
    path: '/authentication',
    module: AuthenticationModule,
  }, {
    path: '/pdmq',
    module: PdmqModule,
  }, {
    path: '/system',
    module: SystemModule,
  }, {
    path: '/permissions',
    module: PermissionsModule,
  },
];

@Module({
  imports: [
      RouterModule.forRoutes(routes), // setup the routes
  ], // as usual, nothing new
  exports: [
    RouterModule
  ]
})
export class ApplicationRoutingModule {}
