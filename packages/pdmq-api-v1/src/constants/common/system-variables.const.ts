import { SystemVariables } from "src/enum/system/system-variables.enum";

export const SystemVariablesList = [
  {
    text: SystemVariables.DATETIME
  },
  {
    text: SystemVariables.ISO_DATETIME
  },
  {
    text: SystemVariables.TIME
  },
  {
    text: SystemVariables.DATE
  },
  {
    text: SystemVariables.CONTEXT
  }
]