export enum SystemVariables {
  DATETIME     = 'datetime',
  DATE         = 'date',
  TIME         = 'time',
  ISO_DATETIME = 'isodatetime',
  CONTEXT      = 'context'
}